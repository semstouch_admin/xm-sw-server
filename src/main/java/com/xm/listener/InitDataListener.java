package com.xm.listener;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.ServletContextAware;

import javax.servlet.ServletContext;

/**
 * 内容管理平台服务启动监听器
 * 查询字典数据存入缓存
 * Created by Administrator on 2016/9/2.
 */
@Controller
public class InitDataListener  implements InitializingBean,ServletContextAware{

    public void  afterPropertiesSet() throws Exception{}
    public void setServletContext(ServletContext servletContext){

    }
}
