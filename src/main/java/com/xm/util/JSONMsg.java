package com.xm.util;

/**
 * Created by Administrator on 2017/4/28.
 */
public class JSONMsg {
    private boolean status;
    private Object msg;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Object getMsg() {
        return msg;
    }

    public void setMsg(Object msg) {
        this.msg = msg;
    }
}
