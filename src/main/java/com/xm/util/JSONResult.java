package com.xm.util;

import java.io.Serializable;

/**
 * @Description
 * @Author semstouch
 * @Date 2017/4/3
 **/
public class JSONResult<T> implements Serializable {
    private T data;
    private String msg;
    private String code;
    private boolean success;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean isSuccess() {return success;}

    public void setSuccess(boolean success) {this.success = success;}
}
