package com.xm.util;

import java.util.List;

/**
 * @Description
 * @Author semstouch
 * @Date 2017/4/17
 **/
public interface BaseService<E>{
    /**
     * 添加
     *
     * @param e
     * @return
     */
    public int insert(E e);

    /**
     * 删除
     *
     * @param e
     * @return
     */
    public int delete(Integer e);

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    public int deletes(Integer[] ids);

    /**
     * 修改
     *
     * @param e
     * @return
     */
    public int update(E e);

    /**
     * 查询一条记录
     *
     * @param e
     * @return
     */
    public E selectOne(E e);

    /**
     * 根据ID查询一条记录
     *
     * @param id
     * @return
     */
    public E selectById(Integer id);

    /**
     * 分页查询
     *
     * @param e
     * @return
     */
    public List<E> selectPageList(E e);

    /**
     * 根据条件查询所有
     * @return
     */
    public List<E> selectList(E e);

}
