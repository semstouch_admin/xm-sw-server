package com.xm.util;

import java.util.List;

/**
 * @Description
 * @Author semstouch
 * @Date 2017/4/18
 **/
public abstract class BaseServiceImpl<E,DAO extends BaseMapper<E>> implements BaseService<E>{
    protected DAO mapper;

    public DAO getDao() {
        return mapper;
    }

    public abstract void setDao(DAO mapper);

    @Override
    public int insert(E e) {
        return mapper.insertSelective(e);
    }

    @Override
    public int delete(Integer e) {
        return mapper.deleteByPrimaryKey(e);
    }

    @Override
    public int deletes(Integer[] ids) {
        if (ids == null || ids.length == 0) {
            throw new NullPointerException("id不能全为空！");
        }

        for (int i = 0; i < ids.length; i++) {
            mapper.deleteByPrimaryKey(ids[i]);
        }
        return 0;
    }

    @Override
    public int update(E e) {
        return mapper.updateByPrimaryKeySelective(e);
    }

    @Override
    public E selectOne(E e) {
        return mapper.selectOne(e);
    }

    @Override
    public E selectById(Integer id) {
        return mapper.selectByPrimaryKey(id);
    }

    @Override
    public List<E> selectPageList(E e) {
        return mapper.selectPageList(e);
    }

    @Override
    public List<E> selectList(E e) {
        return mapper.selectList(e);
    }

}
