package com.xm.util;


import java.util.List;

/**
 * @Description
 * @Author semstouch
 * @Date 2017/4/18
 **/
public interface BaseMapper <E>{

    int deleteByPrimaryKey(Integer id);

    int insert(E record);

    int insertSelective(E record);

    E selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(E record);

    int delete (Integer id);

    /**
     * 查询一条记录
     *
     * @param e
     * @return
     */
   E selectOne(E e);

    /**
     * 分页查询
     *
     * @param e
     * @return
     */
     List<E> selectPageList(E e);

    /**
     * 根据条件查询所有
     * @return
     */
     List<E> selectList(E e);

}
