package com.xm.service;

import com.xm.pojo.Pipe;
import com.xm.util.BaseService;
import org.springframework.stereotype.Service;

/**
 * Created by Administrator on 2017/5/1.
 */
@Service
public interface PipeService extends BaseService<Pipe> {
}
