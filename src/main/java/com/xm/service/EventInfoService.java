package com.xm.service;

import com.xm.pojo.EventInfo;
import com.xm.util.BaseService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Administrator on 2017/5/1.
 */
@Service
public interface EventInfoService extends BaseService<EventInfo> {
    List<EventInfo> selectAll(EventInfo record);

    List<EventInfo> selectUndo(EventInfo record);

    List<EventInfo> selectDo(EventInfo record);
}
