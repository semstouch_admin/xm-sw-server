package com.xm.service.impl;

import com.xm.mapper.EventInfoMapper;
import com.xm.pojo.EventInfo;
import com.xm.service.EventInfoService;
import com.xm.util.BaseServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Administrator on 2017/5/1.
 */
@Service
public class EventInfoServiceImpl extends BaseServiceImpl<EventInfo,EventInfoMapper> implements EventInfoService {
    @Resource(name="eventInfoMapper")
    @Override
    public void setDao(EventInfoMapper mapper){this.mapper=mapper;}

    @Override
    public List<EventInfo> selectAll(EventInfo record) {
        return super.mapper.selectAll(record);
    }

    @Override
    public List<EventInfo> selectDo(EventInfo record) {
        return super.mapper.selectDo(record);
    }

    @Override
    public List<EventInfo> selectUndo(EventInfo record) {
        return super.mapper.selectUndo(record);
    }
}
