package com.xm.service.impl;

import com.xm.mapper.LocationLogMapper;
import com.xm.pojo.LocationLog;
import com.xm.service.LocationLogService;
import com.xm.util.BaseServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Administrator on 2017/5/1.
 */
@Service
public class LocationLogServiceImpl extends BaseServiceImpl<LocationLog,LocationLogMapper> implements LocationLogService {
    @Resource(name="locationLogMapper")
    @Override
    public void setDao(LocationLogMapper mapper){
        this.mapper=mapper;
    }

    @Override
    public List<LocationLog> selectAll(LocationLog record) {
        return super.mapper.selectAll(record);
    }

    @Override
    public List<LocationLog> selectLocationLog(LocationLog record) {
        return super.mapper.selectLocationLog(record);
    }
}
