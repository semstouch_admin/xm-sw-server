package com.xm.service.impl;

import com.xm.mapper.DangerInfoMapper;
import com.xm.pojo.DangerInfo;
import com.xm.service.DangerInfoService;
import com.xm.util.BaseServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by Administrator on 2017/5/1.
 */
@Service
public class DangerInfoServiceImpl extends BaseServiceImpl<DangerInfo,DangerInfoMapper> implements DangerInfoService {
    @Resource(name="dangerInfoMapper")
    @Override
    public void setDao(DangerInfoMapper mapper){this.mapper=mapper;}
}
