package com.xm.service.impl;

import com.xm.mapper.ArchitectureMapper;
import com.xm.pojo.Architecture;
import com.xm.service.ArchitectureService;
import com.xm.util.BaseServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by Administrator on 2017/5/1.
 */
@Service
public class ArchitectureServiceImpl extends BaseServiceImpl<Architecture,ArchitectureMapper> implements ArchitectureService {
    @Resource(name="architectureMapper")
    @Override
    public void setDao(ArchitectureMapper mapper){
        this.mapper=mapper;
    }
}
