package com.xm.service.impl;

import com.xm.mapper.ManholeMapper;
import com.xm.pojo.Manhole;
import com.xm.service.ManholeService;
import com.xm.util.BaseServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by Administrator on 2017/5/1.
 */
@Service
public class ManholeServiceImpl extends BaseServiceImpl<Manhole,ManholeMapper> implements ManholeService {
    @Resource(name="manholeMapper")
    @Override
    public void setDao(ManholeMapper mapper){
        this.mapper=mapper;
    }
}
