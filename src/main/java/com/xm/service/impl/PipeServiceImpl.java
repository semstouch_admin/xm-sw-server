package com.xm.service.impl;

import com.xm.mapper.PipeMapper;
import com.xm.pojo.Pipe;
import com.xm.service.PipeService;
import com.xm.util.BaseServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by Administrator on 2017/5/1.
 */
@Service
public class PipeServiceImpl extends BaseServiceImpl<Pipe,PipeMapper> implements PipeService {
    @Resource(name="pipeMapper")
    @Override
    public void setDao(PipeMapper mapper){
        this.mapper=mapper;
    }
}
