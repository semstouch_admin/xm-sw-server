package com.xm.service.impl;

import com.xm.mapper.SysuserMapper;
import com.xm.pojo.Sysuser;
import com.xm.service.SysuserService;
import com.xm.util.BaseServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @Description
 * @Author semstouch
 * @Date 2017/4/17
 **/
@Service
public class SysuserServiceImpl extends BaseServiceImpl<Sysuser,SysuserMapper> implements SysuserService{
    @Resource(name = "sysuserMapper")
    @Override
    public void setDao(SysuserMapper mapper) {
        this.mapper=mapper;
    }

    @Override
    public Sysuser selectByLogin(Sysuser sysuser) {
        return super.mapper.selectByLogin(sysuser);
    }

    @Override
    public Sysuser selectByRegister(Sysuser sysuser) {
        return super.mapper.selectByRegister(sysuser);
    }

    @Override
    public int updateSysuserIcon(Sysuser record) {
        return super.mapper.updateSysuserIcon(record);
    }

    @Override
    public int selectAmount(Sysuser record) {
        return super.mapper.selectAmount(record);
    }
}
