package com.xm.service.impl;

import com.xm.mapper.GatesMapper;
import com.xm.pojo.Gates;
import com.xm.service.GatesService;
import com.xm.util.BaseServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by Administrator on 2017/5/1.
 */
@Service
public class GatesServiceImpl extends BaseServiceImpl<Gates,GatesMapper> implements GatesService {
    @Resource(name="gatesMapper")
    @Override
    public void setDao(GatesMapper mapper){this.mapper=mapper;}
}
