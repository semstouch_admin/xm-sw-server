package com.xm.service.impl;

import com.xm.mapper.DangerMapper;
import com.xm.pojo.Danger;
import com.xm.service.DangerService;
import com.xm.util.BaseServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by Administrator on 2017/5/1.
 */
@Service
public class DangerServiceImpl extends BaseServiceImpl<Danger,DangerMapper> implements DangerService {
    @Resource(name="dangerMapper")
    @Override
    public void setDao(DangerMapper mapper){this.mapper=mapper;}
}
