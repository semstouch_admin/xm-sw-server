package com.xm.service.impl;

import com.xm.mapper.SenseMapper;
import com.xm.pojo.Sense;
import com.xm.service.SenseService;
import com.xm.util.BaseServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by Administrator on 2017/5/1.
 */
@Service
public class SenseServiceImpl extends BaseServiceImpl<Sense,SenseMapper> implements SenseService {
    @Resource(name="senseMapper")
    @Override
    public void setDao(SenseMapper mapper){
        this.mapper=mapper;
    }
}
