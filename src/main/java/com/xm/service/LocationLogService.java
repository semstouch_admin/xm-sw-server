package com.xm.service;

import com.xm.pojo.LocationLog;
import com.xm.util.BaseService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Administrator on 2017/5/1.
 */
@Service
public interface LocationLogService extends BaseService<LocationLog> {
    List<LocationLog> selectAll(LocationLog record);
    List<LocationLog> selectLocationLog(LocationLog record);
}
