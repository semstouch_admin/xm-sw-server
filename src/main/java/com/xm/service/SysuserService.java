package com.xm.service;

import com.xm.pojo.Sysuser;
import com.xm.util.BaseService;
import org.springframework.stereotype.Service;

/**
 * @Description
 * @Author semstouch
 * @Date 2017/4/17
 **/
@Service
public interface SysuserService extends BaseService<Sysuser> {
    Sysuser selectByLogin(Sysuser sysuser);

    Sysuser selectByRegister(Sysuser sysuser);

    int updateSysuserIcon(Sysuser record);

    int selectAmount(Sysuser record);


}
