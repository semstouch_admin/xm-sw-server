package com.xm.mapper;

import com.xm.pojo.Manhole;
import com.xm.util.BaseMapper;

public interface ManholeMapper extends BaseMapper<Manhole> {
    int deleteByPrimaryKey(Integer id);

    int insert(Manhole record);

    int insertSelective(Manhole record);

    Manhole selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Manhole record);

    int updateByPrimaryKey(Manhole record);
}