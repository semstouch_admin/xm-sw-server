package com.xm.mapper;

import com.xm.pojo.Sense;
import com.xm.util.BaseMapper;

public interface SenseMapper extends BaseMapper<Sense> {
    int deleteByPrimaryKey(Integer id);

    int insert(Sense record);

    int insertSelective(Sense record);

    Sense selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Sense record);

    int updateByPrimaryKey(Sense record);
}