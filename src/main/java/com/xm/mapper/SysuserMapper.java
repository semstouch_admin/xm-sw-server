package com.xm.mapper;

import com.xm.pojo.Sysuser;
import com.xm.util.BaseMapper;

public interface SysuserMapper extends BaseMapper<Sysuser> {
    int deleteByPrimaryKey(Integer id);

    int insert(Sysuser record);

    int insertSelective(Sysuser record);

    Sysuser selectByPrimaryKey(Integer id);

    Sysuser selectByLogin(Sysuser sysuser);
    Sysuser selectByRegister(Sysuser sysuser);

    int updateByPrimaryKeySelective(Sysuser record);

    int updateByPrimaryKey(Sysuser record);

    int updateSysuserIcon(Sysuser record);

    Integer selectAmount(Sysuser record);
}