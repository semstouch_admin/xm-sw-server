package com.xm.mapper;

import com.xm.pojo.EventInfo;
import com.xm.util.BaseMapper;

import java.util.List;

public interface EventInfoMapper extends BaseMapper<EventInfo> {
    int deleteByPrimaryKey(Integer id);

    int insert(EventInfo record);

    int insertSelective(EventInfo record);

    EventInfo selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(EventInfo record);

    int updateByPrimaryKey(EventInfo record);

    List<EventInfo> selectAll(EventInfo record);

    List<EventInfo> selectUndo(EventInfo record);

    List<EventInfo> selectDo(EventInfo record);
}