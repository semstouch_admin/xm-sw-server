package com.xm.mapper;

import com.xm.pojo.Gates;
import com.xm.util.BaseMapper;

public interface GatesMapper extends BaseMapper<Gates> {
    int deleteByPrimaryKey(Integer id);

    int insert(Gates record);

    int insertSelective(Gates record);

    Gates selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Gates record);

    int updateByPrimaryKey(Gates record);

}