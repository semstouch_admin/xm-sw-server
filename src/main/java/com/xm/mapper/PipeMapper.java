package com.xm.mapper;

import com.xm.pojo.Pipe;
import com.xm.util.BaseMapper;

public interface PipeMapper extends BaseMapper<Pipe> {
    int deleteByPrimaryKey(Integer id);

    int insert(Pipe record);

    int insertSelective(Pipe record);

    Pipe selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Pipe record);

    int updateByPrimaryKey(Pipe record);
}