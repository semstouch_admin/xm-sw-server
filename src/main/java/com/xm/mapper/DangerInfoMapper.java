package com.xm.mapper;

import com.xm.pojo.DangerInfo;
import com.xm.util.BaseMapper;

public interface DangerInfoMapper extends BaseMapper<DangerInfo> {
    int deleteByPrimaryKey(Integer id);

    int insert(DangerInfo record);

    int insertSelective(DangerInfo record);

    DangerInfo selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(DangerInfo record);

    int updateByPrimaryKeyWithBLOBs(DangerInfo record);

    int updateByPrimaryKey(DangerInfo record);
}