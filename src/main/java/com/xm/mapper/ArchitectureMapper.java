package com.xm.mapper;

import com.xm.pojo.Architecture;
import com.xm.util.BaseMapper;

public interface ArchitectureMapper extends BaseMapper<Architecture> {
    int deleteByPrimaryKey(Integer id);

    int insert(Architecture record);

    int insertSelective(Architecture record);

    Architecture selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Architecture record);

    int updateByPrimaryKey(Architecture record);
}