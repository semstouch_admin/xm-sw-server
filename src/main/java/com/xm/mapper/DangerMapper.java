package com.xm.mapper;

import com.xm.pojo.Danger;
import com.xm.util.BaseMapper;

public interface DangerMapper extends BaseMapper<Danger> {
    int deleteByPrimaryKey(Integer id);

    int insert(Danger record);

    int insertSelective(Danger record);

    Danger selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Danger record);

    int updateByPrimaryKey(Danger record);
}