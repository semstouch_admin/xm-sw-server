package com.xm.mapper;

import com.xm.pojo.LocationLog;
import com.xm.util.BaseMapper;

import java.util.List;

public interface LocationLogMapper extends BaseMapper<LocationLog> {
    int deleteByPrimaryKey(Integer id);

    int insert(LocationLog record);

    int insertSelective(LocationLog record);

    LocationLog selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(LocationLog record);

    int updateByPrimaryKey(LocationLog record);

    List<LocationLog> selectAll(LocationLog record);

    List<LocationLog> selectLocationLog(LocationLog record);
}