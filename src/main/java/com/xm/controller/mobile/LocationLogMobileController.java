package com.xm.controller.mobile;

import com.xm.pojo.LocationLog;
import com.xm.pojo.Sysuser;
import com.xm.service.LocationLogService;
import com.xm.service.SysuserService;
import com.xm.util.EhcacheCacheProvider;
import com.xm.util.JSONMsg;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * Created by Administrator on 2017/5/1.
 */
@Controller
@RequestMapping("/m/locationLog")
public class LocationLogMobileController {
    @Resource
    private LocationLogService locationLogService;
    @Resource
    private SysuserService sysuserService;
    @Resource(name = "ehcacheCacheProvider")
    private EhcacheCacheProvider cacheProvider;

    /**
     * 人员上报方法
     * @param locationLog
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/insertLocationLog",method=RequestMethod.POST)
    @ResponseBody
    public JSONMsg insertLocationLog(LocationLog locationLog,String token) throws Exception{
        JSONMsg jsonMsg=new JSONMsg();
        Sysuser sysuser=(Sysuser)cacheProvider.get(token);
        locationLog.setCreateUser(sysuser.getId());
        locationLogService.insert(locationLog);
        jsonMsg.setStatus(true);
        return jsonMsg;
    }

}
