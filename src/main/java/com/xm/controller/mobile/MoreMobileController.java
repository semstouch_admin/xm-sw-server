package com.xm.controller.mobile;

import com.xm.pojo.Sysuser;
import com.xm.service.SysuserService;
import com.xm.util.EhcacheCacheProvider;
import com.xm.util.JSONMsg;
import com.xm.util.MD5;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Administrator on 2017/5/12.
 */
@Controller
@RequestMapping("/m/more")
public class MoreMobileController {
    @Resource
    private SysuserService sysuserService;
    @Resource(name = "ehcacheCacheProvider")
    private EhcacheCacheProvider cacheProvider;


    /**
     * 修改用户密码信息方法
     *
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping(value = "/updateSysuserPwd", method = RequestMethod.POST)
    @ResponseBody
    public JSONMsg updateSysuserPwd(Sysuser sysuser,String token,String oldPwd) throws Exception {
        JSONMsg jsonMsg = new JSONMsg();
        //查询用户
        Sysuser oldSysuser=(Sysuser)cacheProvider.get(token);

        //判断用户数据库的密码跟前端传来的密码是否一致
        if(!oldSysuser.getPwd().equals(MD5.md5(oldPwd))){
            jsonMsg.setStatus(false);
            jsonMsg.setMsg("原密码有误！");
        }
        //对密码进行加密后存入数据库
        sysuser.setPwd(MD5.md5(sysuser.getPwd()));
        sysuserService.update(sysuser);
        jsonMsg.setStatus(true);
        return jsonMsg;
    }

    /**
     * 修改用户头像信息方法
     *
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping(value = "/updateSysuserIcon", method = RequestMethod.POST)
    @ResponseBody
    public JSONMsg updateSysuserIcon(Sysuser sysuser) throws Exception {
        JSONMsg jsonMsg = new JSONMsg();
        sysuserService.updateSysuserIcon(sysuser);
        jsonMsg.setStatus(true);
        return jsonMsg;
    }
}
