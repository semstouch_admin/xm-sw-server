package com.xm.controller.mobile;

import com.xm.pojo.Sysuser;
import com.xm.service.SysuserService;
import com.xm.util.EhcacheCacheProvider;
import com.xm.util.JSONMsg;
import com.xm.util.MD5;
import com.xm.util.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @Description
 * @Author semstouch
 * @Date 2017/4/17
 **/
@Controller
@RequestMapping("/m")
public class LoginMobileController {
    @Resource
    private SysuserService sysuserService;
    @Resource(name = "ehcacheCacheProvider")
    private EhcacheCacheProvider cacheProvider;

    /**
     * 用户登出方法
     * @param token
     * @return
     */
    @RequestMapping(value = "/loginOut", method = RequestMethod.POST)
    @ResponseBody
    public JSONMsg loginOut(String token) {
        JSONMsg jsonMsg = new JSONMsg();

        cacheProvider.remove(token);
        jsonMsg.setStatus(true);
        jsonMsg.setMsg("退出成功！");
        return jsonMsg;
    }

    /**
     * 用户登陆方法
     * @param loginName
     * @param pwd
     * @return
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public JSONMsg login(String loginName, String pwd) {
        JSONMsg jsonMsg = new JSONMsg();

        Sysuser params= new Sysuser();
        params.setLoginName(loginName);
        params.setPwd(MD5.md5(pwd));
        Sysuser sysuser=sysuserService.selectByLogin(params);

        if(loginName==null&&loginName.equals("")&&pwd==null&&pwd.equals("")){
            jsonMsg.setStatus(false);
            jsonMsg.setMsg("帐号密码错误！");
            return jsonMsg;
        }
        String oldToken = (String)cacheProvider.get(sysuser.getId().toString());//剔除老的登陆用户
        if(oldToken!=null) {
            cacheProvider.remove(oldToken);
        }
        String token = StringUtils.getUUID();//使用UUID获取唯一编码
        cacheProvider.put(token,sysuser);//缓存当前用户，使用token作为唯一key
        cacheProvider.put(sysuser.getId().toString(),token);//缓存token，使用id作为唯一key

        Map rs = new HashMap();
        rs.put("token",token);
        rs.put("sysuser",sysuser);
        jsonMsg.setStatus(true);
        jsonMsg.setMsg(rs);
        return jsonMsg;
    }

    /**
     * 用户注册方法
     * @param loginName
     * @param pwd
     * @param activeCode
     * @return
     */
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    @ResponseBody
    public JSONMsg register(String loginName, String pwd,String activeCode) {
        JSONMsg jsonMsg = new JSONMsg();

        Sysuser params= new Sysuser();
        params.setLoginName(loginName);
        params.setActiveCode(activeCode);

        Sysuser sysuser=sysuserService.selectByRegister(params);

        if(sysuser==null||sysuser.getRoleType().equals("1")){
            jsonMsg.setStatus(false);
            jsonMsg.setMsg("手机号和激活码有误！");
            return jsonMsg;
        }
        sysuser.setPwd(MD5.md5(pwd));
        sysuser.setStatus("1");
        sysuserService.update(sysuser);

        jsonMsg.setStatus(true);
        jsonMsg.setMsg("注册成功！");
        return jsonMsg;
    }

}
