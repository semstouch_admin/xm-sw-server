package com.xm.controller.mobile;

import com.xm.pojo.Danger;
import com.xm.pojo.EventInfo;
import com.xm.service.DangerInfoService;
import com.xm.service.DangerService;
import com.xm.service.EventInfoService;
import com.xm.util.EhcacheCacheProvider;
import com.xm.util.JSONMsg;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Administrator on 2017/5/1.
 */
@Controller
@RequestMapping("/m/information")
public class InformationMobileController {
    @Resource
    private EventInfoService eventInfoService;
    @Resource
    private DangerService dangerService;
    @Resource
    private DangerInfoService dangerInfoService;

    @Resource(name = "ehcacheCacheProvider")
    private EhcacheCacheProvider cacheProvider;


    /**
     * 查询全部信息方法
     *
     * @param eventInfo
     * @return
     */
    @RequestMapping(value = "/selectAll", method = RequestMethod.GET)
    @ResponseBody
    public JSONMsg selectAll(EventInfo eventInfo) {
        JSONMsg jsonMsg = new JSONMsg();
        List<EventInfo> list = eventInfoService.selectAll(eventInfo);
        jsonMsg.setMsg(list);
        jsonMsg.setStatus(true);
        return jsonMsg;
    }

    /**
     * 查询全部事件信息方法
     *
     * @param eventInfo
     * @return
     */
    @RequestMapping(value = "/selectEventInfoList", method = RequestMethod.GET)
    @ResponseBody
    public JSONMsg selectEventInfoList(EventInfo eventInfo) {
        JSONMsg jsonMsg = new JSONMsg();
        List<EventInfo> list = eventInfoService.selectList(eventInfo);
        jsonMsg.setMsg(list);
        jsonMsg.setStatus(true);
        return jsonMsg;
    }


    /**
     * 查询全部危险源信息方法
     *
     * @param danger
     * @return
     */
    @RequestMapping(value = "/selectDangerList", method = RequestMethod.GET)
    @ResponseBody
    public JSONMsg selectDangerList(Danger danger) {
        JSONMsg jsonMsg = new JSONMsg();
        List<Danger> list = dangerService.selectList(danger);
        jsonMsg.setMsg(list);
        jsonMsg.setStatus(true);
        return jsonMsg;
    }

    /**
     * 查询全部未处理信息方法
     *
     * @param eventInfo
     * @return
     */
    @RequestMapping(value = "/selectUndo", method = RequestMethod.GET)
    @ResponseBody
    public JSONMsg selectUndo(EventInfo eventInfo) {
        JSONMsg jsonMsg = new JSONMsg();
        List<EventInfo> list = eventInfoService.selectUndo(eventInfo);
        jsonMsg.setMsg(list);
        jsonMsg.setStatus(true);
        return jsonMsg;
    }

    /**
     * 查询全部已完成信息方法
     *
     * @param eventInfo
     * @return
     */
    @RequestMapping(value = "/selectDo", method = RequestMethod.GET)
    @ResponseBody
    public JSONMsg selectDo(EventInfo eventInfo) {
        JSONMsg jsonMsg = new JSONMsg();
        List<EventInfo> list = eventInfoService.selectDo(eventInfo);
        jsonMsg.setMsg(list);
        jsonMsg.setStatus(true);
        return jsonMsg;
    }


}
