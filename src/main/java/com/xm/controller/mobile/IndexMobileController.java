package com.xm.controller.mobile;

import com.xm.pojo.*;
import com.xm.service.*;
import com.xm.util.EhcacheCacheProvider;
import com.xm.util.JSONMsg;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Description
 * @Author semstouch
 * @Date 2017/4/17
 **/
@Controller
@RequestMapping("/m/index")
public class IndexMobileController {
    @Resource
    private ArchitectureService architectureService;
    @Resource
    private SenseService senseService;
    @Resource
    private ManholeService manholeService;
    @Resource
    private GatesService gatesService;
    @Resource
    private PipeService pipeService;
    @Resource
    private DangerService dangerService;
    @Resource
    private DangerInfoService dangerInfoService;
    @Resource
    private LocationLogService locationLogService;
    @Resource
    private SysuserService sysuserService;
    @Resource
    private EventInfoService eventInfoService;
    @Resource(name = "ehcacheCacheProvider")
    private EhcacheCacheProvider cacheProvider;


    /**
     * 事件上报方法
     *
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping(value = "/insertEventInfo", method = RequestMethod.POST)
    @ResponseBody
    public JSONMsg insertEventInfo(EventInfo eventInfo, String token) throws Exception {
        JSONMsg jsonMsg = new JSONMsg();
        Sysuser sysuser = (Sysuser) cacheProvider.get(token);
        eventInfo.setCreateUser(sysuser.getId());
        int rs = eventInfoService.insert(eventInfo);
        if (rs == 1) {
            jsonMsg.setStatus(true);
            return jsonMsg;
        } else {
            jsonMsg.setStatus(false);
            return jsonMsg;
        }

    }


    /**
     * 危险源上报方法
     *
     * @return jsonMsg
     * @throws Exception
     */
    @RequestMapping(value = "/insertDanger", method = RequestMethod.POST)
    @ResponseBody
    public JSONMsg insertDanger(Danger danger, String token) throws Exception {
        JSONMsg jsonMsg = new JSONMsg();
        Sysuser sysuser = (Sysuser) cacheProvider.get(token);
        danger.setCreateUser(sysuser.getId());
        int rs = dangerService.insert(danger);
        if (rs == 1) {
            DangerInfo dangerInfo = new DangerInfo();
            dangerInfo.setDangerID(danger.getId());
            dangerInfo.setPic(danger.getPic());
            dangerInfo.setContent(danger.getContent());
            dangerInfo.setStatus(danger.getStatus());
            dangerInfo.setCreateUser(sysuser.getId());
            dangerInfoService.insert(dangerInfo);
            jsonMsg.setStatus(true);
            return jsonMsg;
        } else {
            jsonMsg.setStatus(false);
            return jsonMsg;
        }

    }


    /**
     * 新增危险进度方法
     *
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping(value = "/insertDangerInfo", method = RequestMethod.POST)
    @ResponseBody
    public JSONMsg insertDangerInfo(DangerInfo dangerInfo, String token) throws Exception {
        JSONMsg jsonMsg = new JSONMsg();
        Sysuser sysuser = (Sysuser) cacheProvider.get(token);
        dangerInfo.setCreateUser(sysuser.getId());
        int rs = dangerInfoService.insert(dangerInfo);
        if (rs == 1) {
            Danger danger=new Danger();
            danger.setId(dangerInfo.getDangerID());
            danger.setStatus(dangerInfo.getStatus());
            dangerService.update(danger);
            jsonMsg.setStatus(true);
            return jsonMsg;
        } else {
            jsonMsg.setStatus(false);
            return jsonMsg;
        }

    }

    /**
     * 修改事件信息方法
     *
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping(value = "/updateEventInfo", method = RequestMethod.POST)
    @ResponseBody
    public JSONMsg updateEventInfo(EventInfo eventInfo) throws Exception {
        JSONMsg jsonMsg = new JSONMsg();
        eventInfoService.update(eventInfo);
        jsonMsg.setStatus(true);
        return jsonMsg;
    }


    /**
     * 查询全部建筑信息方法
     *
     * @param architecture
     * @return
     */
    @RequestMapping(value = "/selectArchitectureList", method = RequestMethod.GET)
    @ResponseBody
    public JSONMsg selectArchitectureList(Architecture architecture) {
        JSONMsg jsonMsg = new JSONMsg();
        List<Architecture> list = architectureService.selectList(architecture);
        jsonMsg.setMsg(list);
        jsonMsg.setStatus(true);
        return jsonMsg;
    }

    /**
     * 查询全部传感信息方法
     *
     * @param sense
     * @return
     */
    @RequestMapping(value = "/selectSenseList", method = RequestMethod.GET)
    @ResponseBody
    public JSONMsg selectSenseList(Sense sense) {
        JSONMsg jsonMsg = new JSONMsg();
        List<Sense> list = senseService.selectList(sense);
        jsonMsg.setMsg(list);
        jsonMsg.setStatus(true);
        return jsonMsg;
    }

    /**
     * 查询全部井盖信息方法
     *
     * @param manhole
     * @return
     */
    @RequestMapping(value = "/selectManholeList", method = RequestMethod.GET)
    @ResponseBody
    public JSONMsg selectManholeList(Manhole manhole) {
        JSONMsg jsonMsg = new JSONMsg();
        List<Manhole> list = manholeService.selectList(manhole);
        jsonMsg.setMsg(list);
        jsonMsg.setStatus(true);
        return jsonMsg;
    }

    /**
     * 查询全部涵闸信息方法
     *
     * @param gates
     * @return
     */
    @RequestMapping(value = "/selectGatesList", method = RequestMethod.GET)
    @ResponseBody
    public JSONMsg selectGatesList(Gates gates) {
        JSONMsg jsonMsg = new JSONMsg();
        List<Gates> list = gatesService.selectList(gates);
        jsonMsg.setMsg(list);
        jsonMsg.setStatus(true);
        return jsonMsg;
    }

    /**
     * 查询全部管网信息方法
     *
     * @param pipe
     * @return
     */
    @RequestMapping(value = "/selectPipeList", method = RequestMethod.GET)
    @ResponseBody
    public JSONMsg selectPipeList(Pipe pipe) {
        JSONMsg jsonMsg = new JSONMsg();
        List<Pipe> list = pipeService.selectList(pipe);
        jsonMsg.setMsg(list);
        jsonMsg.setStatus(true);
        return jsonMsg;
    }

    /**
     * 查询全部危险源信息方法
     *
     * @param danger
     * @return
     */
    @RequestMapping(value = "/selectDangerList", method = RequestMethod.GET)
    @ResponseBody
    public JSONMsg selectDangerList(Danger danger) {
        JSONMsg jsonMsg = new JSONMsg();
        List<Danger> list = dangerService.selectList(danger);
        jsonMsg.setMsg(list);
        jsonMsg.setStatus(true);
        return jsonMsg;
    }

    /**
     * 查询危险源详情方法
     *
     * @param danger
     * @return
     */
    @RequestMapping(value = "/selectDangerDetail", method = RequestMethod.GET)
    @ResponseBody
    public JSONMsg selectDangerDetail(Danger danger) {
        JSONMsg jsonMsg = new JSONMsg();
        Danger rs = dangerService.selectOne(danger);
        jsonMsg.setMsg(rs);
        jsonMsg.setStatus(true);
        return jsonMsg;
    }

    /**
     * 查询危险源全部进度方法
     *
     * @param dangerInfo
     * @return
     */
    @RequestMapping(value = "/selectDangerInfoList", method = RequestMethod.GET)
    @ResponseBody
    public JSONMsg selectDangerInfoList(DangerInfo dangerInfo) {
        JSONMsg jsonMsg = new JSONMsg();
        List<DangerInfo> list = dangerInfoService.selectList(dangerInfo);
        jsonMsg.setMsg(list);
        jsonMsg.setStatus(true);
        return jsonMsg;
    }

    /**
     * 查询进度详情方法
     *
     * @param dangerInfo
     * @return
     */
    @RequestMapping(value = "/selectDangerInfoDetail", method = RequestMethod.GET)
    @ResponseBody
    public JSONMsg selectDangerInfoDetail(DangerInfo dangerInfo) {
        JSONMsg jsonMsg = new JSONMsg();
        DangerInfo rs = dangerInfoService.selectOne(dangerInfo);
        jsonMsg.setMsg(rs);
        jsonMsg.setStatus(true);
        return jsonMsg;
    }

    /**
     * 查询全部事件信息方法
     *
     * @param eventInfo
     * @return
     */
    @RequestMapping(value = "/selectEventInfoList", method = RequestMethod.GET)
    @ResponseBody
    public JSONMsg selectEventInfoList(EventInfo eventInfo) {
        JSONMsg jsonMsg = new JSONMsg();
        List<EventInfo> list = eventInfoService.selectList(eventInfo);
        jsonMsg.setMsg(list);
        jsonMsg.setStatus(true);
        return jsonMsg;
    }

    /**
     * 查询事件详情方法
     *
     * @param eventInfo
     * @return
     */
    @RequestMapping(value = "/selectEventInfoDetail", method = RequestMethod.GET)
    @ResponseBody
    public JSONMsg selectEventInfoDetail(EventInfo eventInfo) {
        JSONMsg jsonMsg = new JSONMsg();
        EventInfo rs = eventInfoService.selectOne(eventInfo);
        jsonMsg.setMsg(rs);
        jsonMsg.setStatus(true);
        return jsonMsg;
    }


}
