package com.xm.controller.web;

import com.xm.pojo.Danger;
import com.xm.pojo.DangerInfo;
import com.xm.pojo.Sysuser;
import com.xm.service.DangerInfoService;
import com.xm.service.DangerService;
import com.xm.util.JSONResult;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Administrator on 2017/5/1.
 */
@Controller
@RequestMapping("/web/danger")
public class DangerController {
    @Resource
    private DangerService dangerService;
    @Resource
    private DangerInfoService dangerInfoService;

    /**
     * 跳转到危险管控页面方法
     * @return
     */
    @RequestMapping(value="/toDangerList")
    public ModelAndView toDangerList(HttpServletRequest request){
        ModelAndView modelAndView=new ModelAndView("dangerList");
        Sysuser loginUser=(Sysuser)request.getSession().getAttribute("sysuser");
        modelAndView.addObject("sysuser",loginUser);
        return modelAndView;
    }


    /**
     * 查询全部危险信息方法
     *
     * @param danger
     * @return
     */
    @RequestMapping(value = "/selectDangerList", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult selectDangerList(Danger danger) {
        JSONResult jsonResult = new JSONResult();
        List<Danger> list = dangerService.selectList(danger);
        jsonResult.setData(list);
        jsonResult.setSuccess(true);
        return jsonResult;
    }


    /**
     * 查询危险详情列表方法（进度）
     *
     * @param dangerInfo
     * @return
     */
    @RequestMapping(value = "/selectDangerInfoList", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult selectDangerInfoList(DangerInfo dangerInfo) {
        JSONResult jsonResult = new JSONResult();
        List<DangerInfo> list = dangerInfoService.selectList(dangerInfo);
        jsonResult.setData(list);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 查询进度详情方法
     *
     * @param dangerInfo
     * @return
     */
    @RequestMapping(value = "/selectDangerInfoDetail", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult selectDangerInfoDetail(DangerInfo dangerInfo) {
        JSONResult jsonResult = new JSONResult();
        DangerInfo rs = dangerInfoService.selectOne(dangerInfo);
        jsonResult.setData(rs);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

}
