package com.xm.controller.web;

import com.xm.pojo.*;
import com.xm.service.*;
import com.xm.util.JSONResult;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Administrator on 2017/5/4.
 */
@Controller
@RequestMapping("/web/information")
public class InformationController {
    @Resource
    private ArchitectureService architectureService;
    @Resource
    private SenseService senseService;
    @Resource
    private ManholeService manholeService;
    @Resource
    private GatesService gatesService;
    @Resource
    private PipeService pipeService;


    /**
     * 跳转到信息管理页面方法
     * @return
     */
    @RequestMapping(value="/toInformationList")
    public ModelAndView toInformationList(HttpServletRequest request){
        ModelAndView modelAndView=new ModelAndView("informationList");
        Sysuser loginUser=(Sysuser)request.getSession().getAttribute("sysuser");
        modelAndView.addObject("sysuser",loginUser);
        return modelAndView;
    }


    /**
     * 查询全部建筑信息方法
     *
     * @param architecture
     * @return
     */
    @RequestMapping(value = "/selectArchitectureList", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult selectArchitectureList(Architecture architecture) {
        JSONResult jsonResult = new JSONResult();
        List<Architecture> list = architectureService.selectList(architecture);
        jsonResult.setData(list);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 查询全部传感信息方法
     * @param sense
     * @return
     */
    @RequestMapping(value = "/selectSenseList", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult selectSenseList(Sense sense) {
        JSONResult jsonResult = new JSONResult();
        List<Sense> list = senseService.selectList(sense);
        jsonResult.setData(list);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 查询全部井盖信息方法
     *
     * @param manhole
     * @return
     */
    @RequestMapping(value = "/selectManholeList", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult selectManholeList(Manhole manhole) {
        JSONResult jsonResult = new JSONResult();
        List<Manhole> list = manholeService.selectList(manhole);
        jsonResult.setData(list);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 查询全部涵闸信息方法
     *
     * @param gates
     * @return
     */
    @RequestMapping(value = "/selectGatesList", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult selectGatesList(Gates gates) {
        JSONResult jsonResult = new JSONResult();
        List<Gates> list = gatesService.selectList(gates);
        jsonResult.setData(list);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 查询全部管网信息方法
     *
     * @param pipe
     * @return
     */
    @RequestMapping(value = "/selectPipeList", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult selectPipeList(Pipe pipe) {
        JSONResult jsonResult = new JSONResult();
        List<Pipe> list = pipeService.selectList(pipe);
        jsonResult.setData(list);
        jsonResult.setSuccess(true);
        return jsonResult;
    }



    /**
     * 新增建筑方法
     *
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping(value = "/insertArchitecture", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult insertArchitecture(Architecture architecture,HttpServletRequest request) throws Exception {
        JSONResult jsonResult = new JSONResult();
        Sysuser loginUser=(Sysuser)request.getSession().getAttribute("sysuser");
        if(loginUser==null){
            jsonResult.setSuccess(false);
            jsonResult.setMsg("用戶未登陸！");
            return jsonResult;
        }
        architecture.setCreateUser(loginUser.getId());
        architectureService.insert(architecture);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 新增传感方法
     *
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping(value = "/insertSense", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult insertSense(Sense sense,HttpServletRequest request) throws Exception {
        JSONResult jsonResult = new JSONResult();
        Sysuser loginUser=(Sysuser)request.getSession().getAttribute("sysuser");
        if(loginUser==null){
            jsonResult.setSuccess(false);
            jsonResult.setMsg("用戶未登陸！");
            return jsonResult;
        }
        sense.setCreateUser(loginUser.getId());
        senseService.insert(sense);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 新增井盖方法
     *
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping(value = "/insertManhole", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult insertManhole(Manhole manhole,HttpServletRequest request) throws Exception {
        JSONResult jsonResult = new JSONResult();
        Sysuser loginUser=(Sysuser)request.getSession().getAttribute("sysuser");
        if(loginUser==null){
            jsonResult.setSuccess(false);
            jsonResult.setMsg("用戶未登陸！");
            return jsonResult;
        }
        manhole.setCreateUser(loginUser.getId());
        manholeService.insert(manhole);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 新增涵闸方法
     *
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping(value = "/insertGates", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult insertGates(Gates gates,HttpServletRequest request) throws Exception {
        JSONResult jsonResult = new JSONResult();
        Sysuser loginUser=(Sysuser)request.getSession().getAttribute("sysuser");
        if(loginUser==null){
            jsonResult.setSuccess(false);
            jsonResult.setMsg("用戶未登陸！");
            return jsonResult;
        }
        gates.setCreateUser(loginUser.getId());
        gatesService.insert(gates);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 新增管网方法
     *
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping(value = "/insertPipe", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult insertPipe(Pipe pipe,HttpServletRequest request) throws Exception {
        JSONResult jsonResult = new JSONResult();
        Sysuser loginUser=(Sysuser)request.getSession().getAttribute("sysuser");
        if(loginUser==null){
            jsonResult.setSuccess(false);
            jsonResult.setMsg("用戶未登陸！");
            return jsonResult;
        }
        pipe.setCreateUser(loginUser.getId());
        pipeService.insert(pipe);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 修改建筑信息方法
     *
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping(value = "/updateArchitecture", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult updateArchitecture(Architecture architecture) throws Exception {
        JSONResult jsonResult = new JSONResult();
        architectureService.update(architecture);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 修改传感信息方法
     *
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping(value = "/updateSense", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult updateSense(Sense sense) throws Exception {
        JSONResult jsonResult = new JSONResult();
        senseService.update(sense);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 修改井盖信息方法
     *
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping(value = "/updateManhole", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult updateManhole(Manhole manhole) throws Exception {
        JSONResult jsonResult = new JSONResult();
        manholeService.update(manhole);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 修改涵闸信息方法
     *
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping(value = "/updateGates", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult updateGates(Gates gates) throws Exception {
        JSONResult jsonResult = new JSONResult();
        gatesService.update(gates);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 修改管网信息方法
     *
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping(value = "/updatePipe", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult updatePipe(Pipe pipe) throws Exception {
        JSONResult jsonResult = new JSONResult();
        pipeService.update(pipe);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 删除建筑信息方法
     *
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping(value = "/deleteArchitecture", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult deleteArchitecture(Architecture architecture) throws Exception {
        JSONResult jsonResult = new JSONResult();
        architectureService.delete(architecture.getId());
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 删除传感信息方法
     *
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping(value = "/deleteSense", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult deleteSense(Sense sense) throws Exception {
        JSONResult jsonResult = new JSONResult();
        senseService.delete(sense.getId());
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 删除井盖信息方法
     *
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping(value = "/deleteManhole", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult deleteManhole(Manhole manhole) throws Exception {
        JSONResult jsonResult = new JSONResult();
        manholeService.delete(manhole.getId());
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 删除涵闸信息方法
     *
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping(value = "/deleteGates", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult deleteGates(Gates gates) throws Exception {
        JSONResult jsonResult = new JSONResult();
        gatesService.delete(gates.getId());
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 删除管网信息方法
     *
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping(value = "/deletePipe", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult deletePipe(Pipe pipe) throws Exception {
        JSONResult jsonResult = new JSONResult();
        pipeService.delete(pipe.getId());
        jsonResult.setSuccess(true);
        return jsonResult;
    }




}
