package com.xm.controller.web;

import com.xm.pojo.EventInfo;
import com.xm.pojo.Sysuser;
import com.xm.service.EventInfoService;
import com.xm.util.JSONResult;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Administrator on 2017/5/1.
 */
@Controller
@RequestMapping("/web/eventInfo")
public class EventInfoController {
    @Resource
    private EventInfoService eventInfoService;


    /**
     * 跳转到事件管理页面方法
     * @return
     */
    @RequestMapping(value="/toEventInfoList")
    public ModelAndView toEventInfoList(HttpServletRequest request){
        ModelAndView modelAndView=new ModelAndView("eventInfoList");
        Sysuser loginUser=(Sysuser)request.getSession().getAttribute("sysuser");
        modelAndView.addObject("sysuser",loginUser);
        return modelAndView;
    }


    /**
     * 查询全部事件信息方法
     *
     * @param eventInfo
     * @return
     */
    @RequestMapping(value = "/selectEventInfoList", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult selectEventInfoList(EventInfo eventInfo) {
        JSONResult jsonResult = new JSONResult();
        List<EventInfo> list = eventInfoService.selectList(eventInfo);
        jsonResult.setData(list);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 查询事件详情方法
     *
     * @param eventInfo
     * @return
     */
    @RequestMapping(value = "/selectEventInfoDetail", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult selectEventInfoDetail(EventInfo eventInfo) {
        JSONResult jsonResult = new JSONResult();
        EventInfo rs = eventInfoService.selectOne(eventInfo);
        jsonResult.setData(rs);
        jsonResult.setSuccess(true);
        return jsonResult;
    }
}
