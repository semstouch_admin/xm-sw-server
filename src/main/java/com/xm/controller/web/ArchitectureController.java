package com.xm.controller.web;

import com.xm.pojo.*;
import com.xm.service.ArchitectureService;
import com.xm.service.GatesService;
import com.xm.service.ManholeService;
import com.xm.service.SenseService;
import com.xm.util.JSONResult;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Administrator on 2017/5/1.
 */
@Controller
@RequestMapping("/web/architecture")
public class ArchitectureController {
    @Resource
    private ArchitectureService architectureService;
    @Resource
    private SenseService senseService;
    @Resource
    private ManholeService manholeService;
    @Resource
    private GatesService gatesService;


    /**
     * 跳转到设施管理页面方法
     * @return
     */
    @RequestMapping(value="/toArchitectureList")
    public ModelAndView toArchitectureList(HttpServletRequest request){
        ModelAndView modelAndView=new ModelAndView("architectureList");
        Sysuser loginUser=(Sysuser)request.getSession().getAttribute("sysuser");
        modelAndView.addObject("sysuser",loginUser);
        return modelAndView;
    }


    /**
     * 查询建筑详情方法
     *
     * @param architecture
     * @return
     */
    @RequestMapping(value = "/selectArchitectureDetail", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult selectArchitectureDetail(Architecture architecture) {
        JSONResult jsonResult = new JSONResult();
        Architecture rs =architectureService.selectOne(architecture);
        jsonResult.setData(rs);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 查询传感详情方法
     *
     * @param sense
     * @return
     */
    @RequestMapping(value = "/selectSenseDetail", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult selectSenseDetail(Sense sense) {
        JSONResult jsonResult = new JSONResult();
        Sense rs = senseService.selectOne(sense);
        jsonResult.setData(rs);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 查询井盖详情方法
     *
     * @param manhole
     * @return
     */
    @RequestMapping(value = "/selectManholeDetail", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult selectManholeDetail(Manhole manhole) {
        JSONResult jsonResult = new JSONResult();
        Manhole rs = manholeService.selectOne(manhole);
        jsonResult.setData(rs);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 查询涵闸详情方法
     *
     * @param gates
     * @return
     */
    @RequestMapping(value = "/selectGatesDetail", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult selectGatesDetail(Gates gates) {
        JSONResult jsonResult = new JSONResult();
        Gates rs = gatesService.selectOne(gates);
        jsonResult.setData(rs);
        jsonResult.setSuccess(true);
        return jsonResult;
    }


}
