package com.xm.controller.web;

import com.xm.pojo.Sysuser;
import com.xm.service.SysuserService;
import com.xm.util.JSONResult;
import com.xm.util.MD5;
import com.xm.util.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Administrator on 2017/5/4.
 */
@Controller
@RequestMapping("/web/sysuser")
public class SysuserController {
    @Resource
    private SysuserService sysuserService;

    /**
     * 跳转到用户管理页面方法
     *
     * @return
     */
    @RequestMapping(value = "/toSysuserList")
    public ModelAndView toSysuserList(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView("sysuserList");
        Sysuser loginUser = (Sysuser) request.getSession().getAttribute("sysuser");
        modelAndView.addObject("sysuser", loginUser);
        if(loginUser.getLoginName().equals("hbskwater")==true){
            return modelAndView;
        }else{
            return modelAndView();
        }
    }

    private ModelAndView modelAndView() {
        return null;
    }

//    /**
//     * 跳转到权限提示页面方法
//     *
//     * @return
//     */
//    @RequestMapping(value = "/toLimits")
//    public ModelAndView toLimits(HttpServletRequest request) {
//        ModelAndView modelAndView = new ModelAndView("limits");
//        Sysuser loginUser = (Sysuser) request.getSession().getAttribute("sysuser");
//        modelAndView.addObject("sysuser", loginUser);
//        return modelAndView;
//    }

    /**
     * 跳转到个人中心页面方法
     *
     * @return
     */
    @RequestMapping(value = "/toPersonal")
    public ModelAndView toPersonal(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView("personal");
        Sysuser loginUser = (Sysuser) request.getSession().getAttribute("sysuser");
        modelAndView.addObject("sysuser", loginUser);
        return modelAndView;
    }


    /**
     * 查询全部用户信息方法
     *
     * @param sysuser
     * @return
     */
    @RequestMapping(value = "/selectSysuserList", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult selectSysuserList(Sysuser sysuser) {
        JSONResult jsonResult = new JSONResult();
        List<Sysuser> list = sysuserService.selectList(sysuser);
        jsonResult.setData(list);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 查询当前用户信息方法
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/selectPersonal", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult selectPersonal(HttpServletRequest request) {
        JSONResult jsonResult = new JSONResult();
        Sysuser sysuser = (Sysuser) request.getSession().getAttribute("sysuser");
        jsonResult.setData(sysuser);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 查询单个用户信息方法
     *
     * @param sysuser
     * @return
     */
    @RequestMapping(value = "/selectSysuserDetail", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult selectSysuserDetail(Sysuser sysuser) {
        JSONResult jsonResult = new JSONResult();
        Sysuser rs = sysuserService.selectOne(sysuser);
        jsonResult.setData(rs);
        jsonResult.setSuccess(true);
        return jsonResult;
    }
    

    /**
     * 新增用户信息方法
     *
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping(value = "/insertSysuser", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult insertSysuser(Sysuser sysuser, HttpServletRequest request) throws Exception {
        JSONResult jsonResult = new JSONResult();
        Sysuser loginUser = (Sysuser) request.getSession().getAttribute("sysuser");
        if (loginUser == null) {
            jsonResult.setSuccess(false);
            jsonResult.setMsg("用戶未登陸！");
            return jsonResult;
        }
        if (sysuser.getRoleType().equals("2")&&sysuserService.selectAmount(sysuser) < 10) {//限制APP用户个数
            sysuser.setActiveCode(StringUtils.getUUID());
            sysuser.setCreateUser(loginUser.getId());
            if(sysuser.getPwd()!=null&&(sysuser.getPwd().equals("")==false)){
                sysuser.setPwd(MD5.md5(sysuser.getPwd()));
            }
            sysuserService.insert(sysuser);
            jsonResult.setSuccess(true);
            return jsonResult;
        }else if(sysuser.getRoleType().equals("1")&&sysuserService.selectAmount(sysuser) < 10) {//限制系统用户个数
            sysuser.setCreateUser(loginUser.getId());
            if(sysuser.getPwd()!=null&&(sysuser.getPwd().equals("")==false)){
                sysuser.setPwd(MD5.md5(sysuser.getPwd()));
            }
            sysuserService.insert(sysuser);
            jsonResult.setSuccess(true);
            return jsonResult;
        }else{
            jsonResult.setSuccess(false);
            jsonResult.setMsg("超出用户限制个数，无法再添加！");
            return jsonResult;
        }

    }


    /**
     * 修改用户信息方法
     *
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping(value = "/updateSysuser", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult updateSysuser(Sysuser sysuser, String oldPwd, HttpServletRequest request) throws Exception {
        JSONResult jsonResult = new JSONResult();
        Sysuser loginUser = (Sysuser) request.getSession().getAttribute("sysuser");
        if (!loginUser.getPwd().equals(MD5.md5(oldPwd))) {  //判断用户数据库的密码跟前端传来的密码是否一致
            jsonResult.setSuccess(false);
            jsonResult.setMsg("原密码有误！");
        }
        sysuser.setPwd(MD5.md5(sysuser.getPwd()));  //对密码进行加密后存入数据库
        sysuserService.update(sysuser);
        jsonResult.setSuccess(true);
        return jsonResult;
    }


    /**
     * 删除用户信息方法
     *
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping(value = "/deleteSysuser", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult deleteSysuser(Sysuser sysuser) throws Exception {
        JSONResult jsonResult = new JSONResult();
        sysuserService.delete(sysuser.getId());
        jsonResult.setSuccess(true);
        return jsonResult;
    }

}
