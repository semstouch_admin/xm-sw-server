package com.xm.controller.web;

import com.xm.pojo.*;
import com.xm.service.*;
import com.xm.util.JSONMsg;
import com.xm.util.JSONResult;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @Description
 * @Author semstouch
 * @Date 2017/4/17
 **/
@Controller
@RequestMapping("/web/index")
public class IndexController {
    @Resource
    private ArchitectureService architectureService;
    @Resource
    private SenseService senseService;
    @Resource
    private ManholeService manholeService;
    @Resource
    private GatesService gatesService;
    @Resource
    private PipeService pipeService;
    @Resource
    private DangerService dangerService;
    @Resource
    private DangerInfoService dangerInfoService;
    @Resource
    private LocationLogService locationLogService;
    @Resource
    private SysuserService sysuserService;
    @Resource
    private EventInfoService eventInfoService;


    /**
     * 跳转到首页方法
     * @return
     */
    @RequestMapping(value="/toIndexList")
    public ModelAndView toIndexList(HttpServletRequest request){
        ModelAndView modelAndView=new ModelAndView("indexList");
        Sysuser loginUser=(Sysuser)request.getSession().getAttribute("sysuser");
        modelAndView.addObject("sysuser",loginUser);
        return modelAndView;
    }



    /**
     * 查询全部建筑信息方法
     *
     * @param architecture
     * @return
     */
    @RequestMapping(value = "/selectArchitectureList", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult selectArchitectureList(Architecture architecture) {
        JSONResult jsonResult = new JSONResult();
        List<Architecture> list = architectureService.selectList(architecture);
        jsonResult.setData(list);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 查询全部传感信息方法
     * @param sense
     * @return
     */
    @RequestMapping(value = "/selectSenseList", method = RequestMethod.GET)
    @ResponseBody
    public JSONMsg selectSenseList(Sense sense) {
        JSONMsg jsonMsg = new JSONMsg();
        List<Sense> list = senseService.selectList(sense);
        jsonMsg.setMsg(list);
        jsonMsg.setStatus(true);
        return jsonMsg;
    }

    /**
     * 查询全部井盖信息方法
     *
     * @param manhole
     * @return
     */
    @RequestMapping(value = "/selectManholeList", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult selectManholeList(Manhole manhole) {
        JSONResult jsonResult = new JSONResult();
        List<Manhole> list = manholeService.selectList(manhole);
        jsonResult.setData(list);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 查询全部涵闸信息方法
     *
     * @param gates
     * @return
     */
    @RequestMapping(value = "/selectGatesList", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult selectGatesList(Gates gates) {
        JSONResult jsonResult = new JSONResult();
        List<Gates> list = gatesService.selectList(gates);
        jsonResult.setData(list);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 查询全部管网信息方法
     *
     * @param pipe
     * @return
     */
    @RequestMapping(value = "/selectPipeList", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult selectPipeList(Pipe pipe) {
        JSONResult jsonResult = new JSONResult();
        List<Pipe> list = pipeService.selectList(pipe);
        jsonResult.setData(list);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 查询全部危险信息方法
     *
     * @param danger
     * @return
     */
    @RequestMapping(value = "/selectDangerList", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult selectDangerList(Danger danger) {
        JSONResult jsonResult = new JSONResult();
        List<Danger> list = dangerService.selectList(danger);
        jsonResult.setData(list);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 查询危险详情方法
     *
     * @param dangerInfo
     * @return
     */
    @RequestMapping(value = "/selectDangerInfoDetail", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult selectDangerInfoDetail(DangerInfo dangerInfo) {
        JSONResult jsonResult = new JSONResult();
        DangerInfo rs = dangerInfoService.selectOne(dangerInfo);
        jsonResult.setData(rs);
        jsonResult.setSuccess(true);
        return jsonResult;
    }


    /**
     * 查询全部用户定位日志方法
     *
     * @param locationLog
     * @return
     */
    @RequestMapping(value = "/selectLocationLogList", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult selectLocationLogList(LocationLog locationLog) {
        JSONResult jsonResult = new JSONResult();
        List<LocationLog> list = locationLogService.selectList(locationLog);
        jsonResult.setData(list);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 查询用户状态信息方法
     *
     * @param sysuser
     * @return
     */
    @RequestMapping(value = "/selectSysuserList", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult selectSysuserList(Sysuser sysuser) {
        JSONResult jsonResult = new JSONResult();
        List<Sysuser> list = sysuserService.selectList(sysuser);
        jsonResult.setData(list);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 查询全部事件信息方法
     *
     * @param eventInfo
     * @return
     */
    @RequestMapping(value = "/selectEventInfoList", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult selectEventInfoList(EventInfo eventInfo) {
        JSONResult jsonResult = new JSONResult();
        List<EventInfo> list = eventInfoService.selectList(eventInfo);
        jsonResult.setData(list);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

}
