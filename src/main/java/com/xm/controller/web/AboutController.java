package com.xm.controller.web;

import com.xm.pojo.Sysuser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Administrator on 2017/5/4.
 */
@Controller
@RequestMapping("/web/about")
public class AboutController {


    /**
     * 跳转到关于我们页面方法
     * @return
     */
    @RequestMapping(value="/toAbout")
    public ModelAndView toAbout(HttpServletRequest request){
        ModelAndView modelAndView=new ModelAndView("about");
        Sysuser loginUser=(Sysuser)request.getSession().getAttribute("sysuser");
        modelAndView.addObject("sysuser",loginUser);
        return modelAndView;
    }

    
}
