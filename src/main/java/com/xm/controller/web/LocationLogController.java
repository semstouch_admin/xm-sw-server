package com.xm.controller.web;

import com.xm.pojo.LocationLog;
import com.xm.pojo.Sysuser;
import com.xm.service.LocationLogService;
import com.xm.service.SysuserService;
import com.xm.util.JSONResult;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Administrator on 2017/5/1.
 */
@Controller
@RequestMapping("/web/locationLog")
public class LocationLogController {
    @Resource
    private LocationLogService locationLogService;
    @Resource
    private SysuserService sysuserService;


    /**
     * 跳转到人员调度页面方法
     * @return
     */
    @RequestMapping(value="/toLocationLogList")
    public ModelAndView toLocationLogList(HttpServletRequest request){
        ModelAndView modelAndView=new ModelAndView("locationLogList");
        Sysuser loginUser=(Sysuser)request.getSession().getAttribute("sysuser");
        modelAndView.addObject("sysuser",loginUser);
        return modelAndView;
    }


//    /**
//     * 查询全部用户定位日志方法
//     *
//     * @param locationLog
//     * @return
//     */
//    @RequestMapping(value = "/selectLocationLogList", method = RequestMethod.GET)
//    @ResponseBody
//    public JSONResult selectLocationLogList(LocationLog locationLog) {
//        JSONResult jsonResult = new JSONResult();
//        List<LocationLog> list = locationLogService.selectList(locationLog);
//        jsonResult.setData(list);
//        jsonResult.setSuccess(true);
//        return jsonResult;
//    }

//    /**
//     * 查询人员状态详情方法
//     *
//     * @param locationLog
//     * @return
//     */
//    @RequestMapping(value = "/selectLocationLogDetail", method = RequestMethod.GET)
//    @ResponseBody
//    public JSONResult selectLocationLogDetail(LocationLog locationLog) {
//        JSONResult jsonResult = new JSONResult();
//        LocationLog rs = locationLogService.selectOne(locationLog);
//        jsonResult.setData(rs);
//        jsonResult.setSuccess(true);
//        return jsonResult;
//    }

    /**
     * 查询人员信息
     * @param locationLog
     * @return
     */
    @RequestMapping(value="/selectAll",method=RequestMethod.GET)
    @ResponseBody
    public JSONResult selectAll(LocationLog locationLog){
        JSONResult jsonResult = new JSONResult();
        if (locationLog.getCreateTime()!=null){
            String startTime=locationLog.getCreateTime()+" 00:00:00";//拼接日期格式
            String endTime=locationLog.getCreateTime()+" 23:59:59";
            locationLog.setStartTime(startTime);
            locationLog.setEndTime(endTime);//赋值
        }
        List<LocationLog> list = locationLogService.selectAll(locationLog);
        jsonResult.setData(list);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

    /**
     * 查询单个人员轨迹
     *
     * @param locationLog
     * @return
     */
    @RequestMapping(value = "/selectLocationLog", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult selectLocationLog(LocationLog locationLog) {
        JSONResult jsonResult = new JSONResult();
        if (locationLog.getCreateTime()!=null){
            String startTime=locationLog.getCreateTime()+" 00:00:00";//拼接日期格式
            String endTime=locationLog.getCreateTime()+" 23:59:59";
            locationLog.setStartTime(startTime);
            locationLog.setEndTime(endTime);//赋值
        }
        List<LocationLog> list = locationLogService.selectLocationLog(locationLog);
        jsonResult.setData(list);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

}
