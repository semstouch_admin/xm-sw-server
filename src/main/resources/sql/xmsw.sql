/*
Navicat MySQL Data Transfer

Source Server         : wzl
Source Server Version : 50718
Source Host           : localhost:3306
Source Database       : xmsw

Target Server Type    : MYSQL
Target Server Version : 50718
File Encoding         : 65001

Date: 2017-05-22 21:47:44
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for xm_architecture
-- ----------------------------
DROP TABLE IF EXISTS `xm_architecture`;
CREATE TABLE `xm_architecture` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `sno` varchar(1000) DEFAULT NULL COMMENT '编号',
  `name` varchar(255) DEFAULT NULL COMMENT '设备名称',
  `address` varchar(100) DEFAULT NULL COMMENT '地址',
  `type` varchar(255) DEFAULT NULL COMMENT '类别',
  `age` varchar(255) DEFAULT NULL COMMENT '寿命',
  `personLiable` varchar(255) DEFAULT NULL COMMENT '责任人',
  `year` varchar(255) DEFAULT NULL COMMENT '年份',
  `department` varchar(1000) DEFAULT NULL COMMENT '部门',
  `spec` varchar(255) DEFAULT NULL COMMENT '规格',
  `lng` varchar(255) DEFAULT NULL COMMENT '经度',
  `lat` varchar(255) DEFAULT NULL COMMENT '维度',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注',
  `createTime` varchar(255) DEFAULT NULL COMMENT '创建时间',
  `createUser` int(11) DEFAULT NULL COMMENT '创建者ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='建筑信息表';

-- ----------------------------
-- Records of xm_architecture
-- ----------------------------
INSERT INTO `xm_architecture` VALUES ('1', 'M0001', '金山调水泵站', '思明区环岛路软件园二期', '泵站', '15', '廖权峰', '2005/06/28', '市场部', '10万', '118.184495', '24.498414', '老旧，待更新', '2017-05-15 15:35:50', '1');
INSERT INTO `xm_architecture` VALUES ('2', 'M0002', '金山调水泵站', '思明区环岛路软件园二期', '泵站', '20', '钟信魁', '2006/07/12', '销售部', '10万', '118.186291', '24.492429', '破损，待维护', '2017-05-16 16:40:35', '2');
INSERT INTO `xm_architecture` VALUES ('3', 'M0003', '金山调水泵站', '思明区环岛路软件园二期', '泵站', '25', '杨智翔', '2007/12/25', '研发部', '10万', '118.192543', '24.494731', '破损，待维护', '2017-05-17 18:20:35', '3');

-- ----------------------------
-- Table structure for xm_danger
-- ----------------------------
DROP TABLE IF EXISTS `xm_danger`;
CREATE TABLE `xm_danger` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `title` varchar(255) DEFAULT NULL COMMENT '危险标题',
  `type` varchar(3) DEFAULT NULL COMMENT '危险类型',
  `code` varchar(3) DEFAULT '1',
  `status` varchar(3) DEFAULT '0' COMMENT '处理进度（0 未处理  1 已完成）',
  `lng` varchar(255) DEFAULT NULL COMMENT '上报经度',
  `lat` varchar(255) DEFAULT NULL COMMENT '上报维度',
  `address` varchar(1000) DEFAULT NULL COMMENT '上报地址',
  `pic` varchar(1000) DEFAULT NULL COMMENT '图片信息',
  `num` int(11) DEFAULT NULL COMMENT '编号',
  `content` varchar(255) DEFAULT NULL COMMENT '内容（移动端查询用）',
  `createTime` varchar(255) DEFAULT NULL COMMENT '创建时间',
  `createUser` int(11) DEFAULT NULL COMMENT '创建者ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='危险信息表';

-- ----------------------------
-- Records of xm_danger
-- ----------------------------
INSERT INTO `xm_danger` VALUES ('1', '检修井盖破损', '1', '1', '1', '118.1901', '24.493153', '湖里区仙岳路辅路管理所附近', '/upload/149457550367583755.jpg,/upload/149457550367583755.jpg,/upload/149457544552165629.jpg', '9529', null, '2017-05-15 15:35:50', '1');
INSERT INTO `xm_danger` VALUES ('2', '检修涵闸破损', '2', '1', '0', '118.1924', '24.491048', '湖里区仙岳路湖边水库附近', '/upload/149457544552165629.jpg,/upload/149457544552165629.jpg,/upload/149457544552165629.jpg', '9528', null, '2017-05-16 16:40:35', '2');
INSERT INTO `xm_danger` VALUES ('3', '检修井盖破损', '3', '1', '1', '118.185357', '24.497625', '思明区环岛路软件园二期附近', '/upload/149457550367583755.jpg,/upload/149457550367583755.jpg,/upload/149457544552165629.jpg', '9527', null, '2017-05-17 18:20:35', '3');
INSERT INTO `xm_danger` VALUES ('4', '检修井盖破损', '4', '1', '0', '118.189057', '24.495014', '思明区中山路步行街附近', '/upload/149457550367583755.jpg,/upload/149457550367583755.jpg,/upload/149457544552165629.jpg', '9526', null, '2017-05-18 12:20:35', '4');

-- ----------------------------
-- Table structure for xm_dangerinfo
-- ----------------------------
DROP TABLE IF EXISTS `xm_dangerinfo`;
CREATE TABLE `xm_dangerinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dangerID` int(11) DEFAULT NULL,
  `pic` varchar(1000) DEFAULT NULL,
  `road` varchar(255) DEFAULT NULL COMMENT '道路信息',
  `content` varchar(1000) DEFAULT NULL COMMENT '内容',
  `status` varchar(255) DEFAULT '0' COMMENT '处理进度（0 未处理  1 已完成）',
  `num` int(11) DEFAULT NULL COMMENT '编号',
  `createUser` int(11) DEFAULT NULL,
  `createTime` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='危险详情表';

-- ----------------------------
-- Records of xm_dangerinfo
-- ----------------------------
INSERT INTO `xm_dangerinfo` VALUES ('1', '1', '/upload/149457571584340539.jpg,/upload/149457571584340539.jpg,/upload/149457571584340539.jpg', '思明区金山路', 'DN1400湖边补调管道破损严重，引起爆管，金山路辅路出现约300平方米积水，最深积水约300mm。目前已经造成车辆滞流。', '1', '9529', '1', '2017-05-15 15:35:50');
INSERT INTO `xm_dangerinfo` VALUES ('2', '2', '/upload/149457571584340539.jpg,/upload/149457571584340539.jpg,/upload/149457571584340539.jpg', '湖里区仙岳路', 'DN1400湖边补调管道破损严重，引起爆管，金山路辅路出现约300平方米积水，最深积水约300mm。目前已经造成车辆滞流。', '0', '9528', '2', '2017-05-16 16:40:35');
INSERT INTO `xm_dangerinfo` VALUES ('3', '3', '/upload/149457577047447339.jpg,/upload/149457577047447339.jpg,/upload/149457577047447339.jpg', '湖里区环岛路', 'DN1400湖边补调管道破损严重，引起爆管，金山路辅路出现约300平方米积水，最深积水约300mm。目前已经造成车辆滞流。', '1', '9527', '3', '2017-05-17 18:20:35');
INSERT INTO `xm_dangerinfo` VALUES ('4', '4', '/upload/149457577047447339.jpg,/upload/149457577047447339.jpg,/upload/149457577047447339.jpg', '思明区中山路', 'DN1400湖边补调管道破损严重，引起爆管，金山路辅路出现约300平方米积水，最深积水约300mm。目前已经造成车辆滞流。', '0', '9526', '4', '2017-05-18 12:20:35');

-- ----------------------------
-- Table structure for xm_eventinfo
-- ----------------------------
DROP TABLE IF EXISTS `xm_eventinfo`;
CREATE TABLE `xm_eventinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `title` varchar(255) DEFAULT NULL COMMENT '事件标题',
  `type` varchar(3) DEFAULT NULL COMMENT '事件类型',
  `code` varchar(3) DEFAULT '0',
  `status` varchar(3) DEFAULT '0' COMMENT '处理进度（0 未处理  1 已完成）',
  `lng` varchar(255) DEFAULT NULL COMMENT '上报经度',
  `lat` varchar(255) DEFAULT NULL COMMENT '上报维度',
  `address` varchar(1000) DEFAULT NULL COMMENT '上报地址',
  `content` varchar(1000) DEFAULT NULL COMMENT '事件内容',
  `pic` varchar(1000) DEFAULT NULL COMMENT '图片信息',
  `num` int(11) DEFAULT NULL COMMENT '编号',
  `createTime` varchar(255) DEFAULT NULL COMMENT '创建时间',
  `createUser` int(11) DEFAULT NULL COMMENT '创建者ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='事件信息表';

-- ----------------------------
-- Records of xm_eventinfo
-- ----------------------------
INSERT INTO `xm_eventinfo` VALUES ('1', '湖边补调管道破损', '1', '0', '1', '118.187082', '24.497099', '湖里区仙岳路辅路管理所附近', 'DN1400湖边补调管道破损严重，引起爆管，仙岳路辅路出现约300平方米积水，最深积水约300mm。目前已经造成车辆滞流。', '/upload/149457536395852723.jpg,/upload/149457536395852723.jpg,/upload/149457536395852723.jpg', '9529', '2017-05-15 15:35:50', '1');
INSERT INTO `xm_eventinfo` VALUES ('2', '金山路补调管道破损', '2', '0', '1', '118.187082', '24.491903', '思明区金山路附近', 'DN1400湖边补调管道破损严重，引起爆管，仙岳路辅路出现约300平方米积水，最深积水约300mm。目前已经造成车辆滞流。', '/upload/149457544552165629.jpg,/upload/149457544552165629.jpg,/upload/149457544552165629.jpg', '9528', '2017-05-16 16:40:35', '2');
INSERT INTO `xm_eventinfo` VALUES ('3', '湖里区仙岳路补调管道破损', '3', '0', '1', '118.187082', '24.494073', '湖里区环岛干道万达附近', 'DN1400湖边补调管道破损严重，引起爆管，仙岳路辅路出现约300平方米积水，最深积水约300mm。目前已经造成车辆滞流。', '/upload/149457550367583755.jpg,/upload/149457550367583755.jpg,/upload/149457544552165629.jpg', '9527', '2017-05-17 18:20:35', '3');
INSERT INTO `xm_eventinfo` VALUES ('4', '中山路步行街补调管道破损', '4', '0', '0', '118.187082', '24.494073', '思明区中山路步行街', 'DN1400湖边补调管道破损严重，引起爆管，仙岳路辅路出现约300平方米积水，最深积水约300mm。目前已经造成车辆滞流。', '/upload/149457550367583755.jpg,/upload/149457550367583755.jpg,/upload/149457544552165629.jpg', '9526', '2017-05-17 18:20:35', '3');

-- ----------------------------
-- Table structure for xm_gates
-- ----------------------------
DROP TABLE IF EXISTS `xm_gates`;
CREATE TABLE `xm_gates` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `sno` varchar(1000) DEFAULT NULL COMMENT '编号',
  `name` varchar(255) DEFAULT NULL COMMENT '设备名称',
  `address` varchar(100) DEFAULT NULL COMMENT '地址',
  `type` varchar(255) DEFAULT NULL COMMENT '类别',
  `age` varchar(255) DEFAULT NULL COMMENT '寿命',
  `personLiable` varchar(255) DEFAULT NULL COMMENT '责任人',
  `year` varchar(255) DEFAULT NULL COMMENT '年份',
  `department` varchar(1000) DEFAULT NULL COMMENT '部门',
  `spec` varchar(255) DEFAULT NULL COMMENT '规格',
  `lng` varchar(255) DEFAULT NULL COMMENT '经度',
  `lat` varchar(255) DEFAULT NULL COMMENT '维度',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注',
  `createTime` varchar(255) DEFAULT NULL COMMENT '创建时间',
  `createUser` int(11) DEFAULT NULL COMMENT '创建者ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='井盖信息表';

-- ----------------------------
-- Records of xm_gates
-- ----------------------------
INSERT INTO `xm_gates` VALUES ('1', 'H0010', '市政侧放水塔', '1', '放水塔', '20', '刘星', '2002/09/25', '研发部', '日排水量20万', '118.191394', '24.495126', '老旧，待更新', '2017-05-15 15:35:50', '1');
INSERT INTO `xm_gates` VALUES ('2', 'H0012', '市政侧放水塔', '2', '放水塔', '25', '夏雪', '2003/02/15', '市场部', '日排水量25万', '118.185932', '24.496836', '破损，待维护', '2017-05-16 16:40:35', '2');
INSERT INTO `xm_gates` VALUES ('3', 'H0013', '市政侧放水塔', '3', '放水塔', '30', '陈兵', '2004/05/12', '销售部', '日排水量30万', '118.187513', '24.49677', '破损，待维护', '2017-05-17 18:20:35', '3');

-- ----------------------------
-- Table structure for xm_locationlog
-- ----------------------------
DROP TABLE IF EXISTS `xm_locationlog`;
CREATE TABLE `xm_locationlog` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `type` varchar(3) DEFAULT NULL COMMENT '定位类型',
  `lng` varchar(255) DEFAULT NULL COMMENT '定位经度',
  `lat` varchar(255) DEFAULT NULL COMMENT '定位维度',
  `address` varchar(255) DEFAULT NULL COMMENT '位置',
  `createTime` varchar(255) DEFAULT NULL COMMENT '创建时间',
  `createUser` int(11) DEFAULT NULL COMMENT '创建者ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='用户定位日志表';

-- ----------------------------
-- Records of xm_locationlog
-- ----------------------------
INSERT INTO `xm_locationlog` VALUES ('1', '1', '118.192975', '24.48993', null, '2017-05-15 15:35:50', '1');
INSERT INTO `xm_locationlog` VALUES ('2', '2', '118.183417', '24.493218', null, '2017-05-16 16:40:35', '2');
INSERT INTO `xm_locationlog` VALUES ('3', '3', '118.189148', '24.495109', null, '2017-05-17 18:20:35', '3');

-- ----------------------------
-- Table structure for xm_manhole
-- ----------------------------
DROP TABLE IF EXISTS `xm_manhole`;
CREATE TABLE `xm_manhole` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `sno` varchar(1000) DEFAULT NULL COMMENT '编号',
  `name` varchar(255) DEFAULT NULL COMMENT '设备名称',
  `address` varchar(100) DEFAULT NULL COMMENT '地址',
  `ladder` varchar(255) DEFAULT NULL COMMENT '爬梯',
  `quality` varchar(255) DEFAULT NULL COMMENT '材质',
  `type` varchar(255) DEFAULT NULL COMMENT '类别',
  `age` varchar(255) DEFAULT NULL COMMENT '寿命',
  `personLiable` varchar(255) DEFAULT NULL COMMENT '责任人',
  `year` varchar(255) DEFAULT NULL COMMENT '年份',
  `shape` varchar(255) DEFAULT NULL COMMENT '形状',
  `depth` varchar(255) DEFAULT NULL COMMENT '井深',
  `protect` varchar(1000) DEFAULT NULL COMMENT '防护',
  `department` varchar(1000) DEFAULT NULL COMMENT '部门',
  `spec` varchar(255) DEFAULT NULL COMMENT '规格',
  `lng` varchar(255) DEFAULT NULL COMMENT '经度',
  `lat` varchar(255) DEFAULT NULL COMMENT '维度',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注',
  `createTime` varchar(255) DEFAULT NULL COMMENT '创建时间',
  `createUser` int(11) DEFAULT NULL COMMENT '创建者ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='井盖信息表';

-- ----------------------------
-- Records of xm_manhole
-- ----------------------------
INSERT INTO `xm_manhole` VALUES ('1', 'J0010', '查询供水井盖', '湖里区吕岭路中国工商银行附近', '有', '铸铁', '供水井盖', '20', '李雯雯', '1', '1', '1', '1', '1', '700mm', '1', '1', '1', '2017-05-15 15:35:50', '1');
INSERT INTO `xm_manhole` VALUES ('2', 'J0011', '金山小区供水井盖', '湖里区吕岭路中国工商银行附近', '有', '铸铁', '供水井盖', '25', '王小虎', '2', '2', '2', '2', '2', '800mm', '2', '2', '2', '2017-05-16 16:40:35', '2');
INSERT INTO `xm_manhole` VALUES ('3', 'J0012', '金山小区供水井盖', '湖里区吕岭路中国工商银行附近', '有', '铸铁', '供水井盖', '30', '韩醒', '3', '3', '3', '3', '3', '900mm', '3', '3', '3', '2017-05-17 18:20:35', '3');

-- ----------------------------
-- Table structure for xm_pipe
-- ----------------------------
DROP TABLE IF EXISTS `xm_pipe`;
CREATE TABLE `xm_pipe` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `sno` varchar(1000) DEFAULT NULL COMMENT '编号',
  `name` varchar(255) DEFAULT NULL COMMENT '设备名称',
  `startAddress` varchar(1000) DEFAULT NULL COMMENT '起点地址',
  `endAddress` varchar(1000) DEFAULT NULL COMMENT '终点地址',
  `thickness` varchar(255) DEFAULT NULL COMMENT '壁厚',
  `type` varchar(255) DEFAULT NULL COMMENT '类别',
  `quality` varchar(255) DEFAULT NULL COMMENT '材质',
  `spec` varchar(255) DEFAULT NULL COMMENT '规格',
  `age` varchar(255) DEFAULT NULL COMMENT '寿命',
  `year` varchar(255) DEFAULT NULL COMMENT '年份',
  `depth` varchar(255) DEFAULT NULL COMMENT '埋深',
  `personLiable` varchar(255) DEFAULT NULL COMMENT '责任人',
  `department` varchar(1000) DEFAULT NULL,
  `startLng` varchar(255) DEFAULT NULL COMMENT '设备起点经度',
  `startLat` varchar(255) DEFAULT NULL COMMENT '设备起点维度',
  `endLng` varchar(255) DEFAULT NULL COMMENT '设备终点经度',
  `endLat` varchar(255) DEFAULT NULL COMMENT '设备终点纬度',
  `altitude` varchar(1000) DEFAULT NULL COMMENT '高程',
  `remarks` varchar(1000) DEFAULT NULL COMMENT '备注',
  `lineColor` varchar(255) DEFAULT NULL COMMENT '线色',
  `lineWidth` varchar(255) DEFAULT NULL COMMENT '线宽',
  `lineLength` varchar(255) DEFAULT '' COMMENT '线标',
  `createTime` varchar(255) DEFAULT NULL COMMENT '创建时间',
  `createUser` int(11) DEFAULT NULL COMMENT '创建者ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='管网信息表';

-- ----------------------------
-- Records of xm_pipe
-- ----------------------------
INSERT INTO `xm_pipe` VALUES ('1', 'G00220', '湖边补调管道', '软件园二期西门', '软件园二期栋东门', '50mm', '供水管网', '水泥', 'DN1400', '20', '2009/06/09', '1200mm', '刘倩倩', '研发部', '118.18392', '24.494945', '118.1878', '24.496424', '5000mm', '老旧，待更新', '红色', '5', '1000', '2017-05-15 15:35:50', '1');
INSERT INTO `xm_pipe` VALUES ('2', 'G00220', '湖边补调管道', '软件园二期西门', '软件园二期东门', '60mm', '供水管网', '水泥', 'DN1400', '25', '2009/05/20', '1000mm', '张艾丽', '销售部', '118.18798', '24.492462', '118.191358', '24.494879', '5600mm', '老旧，待更新', '红色', '5', '2000', '2017-05-16 16:40:35', '2');
INSERT INTO `xm_pipe` VALUES ('3', 'G00220', '湖边补调管道', '软件园二期西门', '软件园二期东门', '70mm', '供水管网', '水泥', 'DN1400', '30', '2009/07/18', '1400mm', '李雯', '市场部', '118.191088', '24.491459', '118.193478', '24.495142', '6000mm', '老旧，待更新', '蓝色', '5', '3000', '2017-05-17 18:20:35', '3');
INSERT INTO `xm_pipe` VALUES ('4', 'G00220', '湖边补调管道', '软件园二期西门', '软件园二期东门', '60mm', '供水管网', '水泥', 'DN1400', '40', '2009/07/18', '1400mm', '郑楚', '市场部', '118.193543', '24.49197', '118.182045', '24.497232', '5800mm', '老旧，待更新', '蓝色', '8', '6000', '2017-05-14 17:01:49', '2');

-- ----------------------------
-- Table structure for xm_sense
-- ----------------------------
DROP TABLE IF EXISTS `xm_sense`;
CREATE TABLE `xm_sense` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `sno` varchar(1000) DEFAULT NULL COMMENT '编号',
  `name` varchar(255) DEFAULT NULL COMMENT '设备名称',
  `address` varchar(100) DEFAULT NULL COMMENT '地址',
  `quality` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL COMMENT '类别',
  `age` varchar(255) DEFAULT NULL COMMENT '寿命',
  `personLiable` varchar(255) DEFAULT NULL COMMENT '责任人',
  `year` varchar(255) DEFAULT NULL COMMENT '年份',
  `department` varchar(1000) DEFAULT NULL COMMENT '部门',
  `spec` varchar(255) DEFAULT NULL COMMENT '规格',
  `lng` varchar(255) DEFAULT NULL COMMENT '经度',
  `lat` varchar(255) DEFAULT NULL COMMENT '维度',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注',
  `createTime` varchar(255) DEFAULT NULL COMMENT '创建时间',
  `createUser` int(11) DEFAULT NULL COMMENT '创建者ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='传感信息表';

-- ----------------------------
-- Records of xm_sense
-- ----------------------------
INSERT INTO `xm_sense` VALUES ('1', 'C0010', '金泰路压力传感', '湖里区仙岳路SM广场附近', 'ABS', '压力传感', '6', '张晓晓', '2012/07/14', '市场部', 'DN800', '118.182896', '24.494057', '老旧，待更新', '2017-05-15 15:35:50', '1');
INSERT INTO `xm_sense` VALUES ('2', 'C0012', '金泰路压力传感', '思明区金山路金山小区附近', 'ABS', '压力传感', '7', '刘琦', '2013/03/28', '销售部', 'DN850', '118.185932', '24.492807', '老旧，待更新', '2017-05-16 16:40:35', '2');
INSERT INTO `xm_sense` VALUES ('3', 'C0013', '金泰路压力传感', '湖里区仙岳路湖边水库附近', 'ABS', '压力传感', '8', '赵品轩', '2014/01/23', '研发部', 'DN900', '118.190495', '24.49141', '老旧，待更新', '2017-05-17 18:20:35', '3');

-- ----------------------------
-- Table structure for xm_sysuser
-- ----------------------------
DROP TABLE IF EXISTS `xm_sysuser`;
CREATE TABLE `xm_sysuser` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `loginName` varchar(255) DEFAULT NULL COMMENT '登陆帐号',
  `pwd` varchar(32) DEFAULT NULL COMMENT '登陆密码',
  `nickName` varchar(255) DEFAULT NULL COMMENT '真实姓名',
  `phone` varchar(255) DEFAULT NULL COMMENT '手机号码',
  `userIcon` varchar(255) DEFAULT NULL COMMENT '用户头像',
  `department` varchar(255) DEFAULT NULL COMMENT '所属部门名称',
  `roleType` varchar(3) DEFAULT NULL COMMENT '用户角色（1 系统用户  2  APP用户）',
  `activeCode` varchar(255) DEFAULT NULL COMMENT '激活码',
  `activeTime` varchar(255) DEFAULT NULL COMMENT '激活时间',
  `status` varchar(3) DEFAULT NULL COMMENT '用户状态(1 启用  0 禁用)',
  `createTime` varchar(255) DEFAULT NULL COMMENT '创建时间',
  `createUser` int(11) DEFAULT NULL COMMENT '创建者ID',
  `remarks` varchar(1000) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='用户信息表';

-- ----------------------------
-- Records of xm_sysuser
-- ----------------------------
INSERT INTO `xm_sysuser` VALUES ('1', '15711593311', 'e10adc3949ba59abbe56e057f20f883e', '向绍军', '15711593311', '/upload/149457509700946455.jpeg', '研发部', '1', '888888', '1', '1', '2017-05-15 15:35:50', '1', null);
INSERT INTO `xm_sysuser` VALUES ('2', '15711593312', 'e10adc3949ba59abbe56e057f20f883e', '曾伟伟', '15711593312', '/upload/149457509700946455.jpeg', '研发部', '1', '888888', '1', '1', '2017-05-16 16:40:35', '2', null);
INSERT INTO `xm_sysuser` VALUES ('3', '15711593313', 'e10adc3949ba59abbe56e057f20f883e', '付建树', '15711593313', '/upload/149457509700946455.jpeg', '研发部', '1', '888888', '1', '1', '2017-05-17 18:20:352017-05-17 18:20:35', '3', '');
