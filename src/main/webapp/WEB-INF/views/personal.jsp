<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/5/17
  Time: 11:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <base href="<%=basePath%>">
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <title>个人中心</title>
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/js/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/js/lib/bootstrap/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/css/base.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/css/columns.css">
    <link rel="stylesheet" type="text/css" href="https://at.alicdn.com/t/font_aqznujt9mlpu8fr.css">
</head>
<body>
<div class="personal-wrap wrap">
    <!-- 头部页面 -->
    <header>
        <div class="container">
            <div class="logo">
                <img src="<%=basePath%>static/images/logo.png">

                <div class="title">
                    <h1>水务生产调度系统</h1>

                    <h3>湖边水库管理所</h3>
                </div>
            </div>
            <div class="state-message">
                <div class="nav">
                    <ul>
                        <li><a href="<%=basePath%>rest/web/index/toIndexList">实时监控</a></li>
                        <li><a href="<%=basePath%>rest/web/eventInfo/toEventInfoList ">事件管理</a></li>
                        <li><a href="<%=basePath%>rest/web/danger/toDangerList">危险管控</a></li>
                        <li><a href="<%=basePath%>rest/web/locationLog/toLocationLogList">人员调度</a></li>
                        <li><a href="<%=basePath%>rest/web/architecture/toArchitectureList ">设施管理</a></li>
                        <li><a href="<%=basePath%>rest/web/information/toInformationList">信息管理</a></li>
                        <li><a href="<%=basePath%>rest/web/sysuser/toSysuserList">用户管理</a></li>
                        <li><a href="<%=basePath%>rest/web/about/toAbout">关于我们</a></li>
                    </ul>
                </div>
                <div class="user-message">
                    <div class="dropdown clearfix">
                        <p type="button" id="dropdownMenuDivider" data-toggle="dropdown" aria-haspopup="true"
                           aria-expanded="true">
                            ${sysuser.nickName}
                            <span class="skuiwu-caret caret"></span>
                        </p>
                        <ul class="skuiwu-menu  dropdown-menu" aria-labelledby="dropdownMenuDivider">
                            <li><a href="#">个人中心</a></li>
                            <li><a href="<%=basePath%>rest/web/toLoginOut">退出登录</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!--内容页面 -->
    <div class="content">
        <div class="shuiwu-personal">

            <%--<div class="image-form">--%>
                <%--<h4>编辑头像</h4>--%>

                <%--<form>--%>
                    <%--<span class="iconfont icon-tupian"></span>--%>

                    <%--<div class="form-group">--%>
                        <%--<input type="file" id="exampleInputFile">--%>

                        <%--<p class="help-block">请选择jpeg、gif，小于2M的图片 （使用高质量图片，可生成高清头像）--%>
                        <%--</p>--%>
                        <%--<button type="submit" class="submit-btn">保存</button>--%>
                    <%--</div>--%>
                <%--</form>--%>
            <%--</div>--%>

        </div>
    </div>
</div>
<script type="text/javascript" src="<%=basePath%>static/js/lib/jquery.min.js"></script>
<script type="text/javascript" src="<%=basePath%>static/js/lib/bootstrap/bootstrap.min.js"></script>
<script type="text/javascript" src="<%=basePath%>static/js/index.js"></script>
<script type="text/javascript" src="<%=basePath%>static/js/personal.js"></script>

</body>
</html>

