<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <base href="<%=basePath%>">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no"/>
    <title>人员调度</title>
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/js/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/js/lib/bootstrap/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css"
          href="<%=basePath%>static/js/lib/bootstrap/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="https://api.map.baidu.com/library/DrawingManager/1.4/src/DrawingManager_min.css">
    <link rel="stylesheet" href="https://api.map.baidu.com/library/SearchInfoWindow/1.4/src/SearchInfoWindow_min.css"/>
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/css/base.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/css/layout.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/css/master.css">
    <link rel="stylesheet" type="text/css" href="https://at.alicdn.com/t/font_cqeinxuuef3qsemi.css">
</head>
<body>
<div class="wrap">
    <!-- 头部页面 -->
    <header>
        <div class="container">
            <div class="logo">
                <img src="<%=basePath%>/static/images/logo.png">

                <div class="title">
                    <h1>水务生产调度系统</h1>

                    <h3>湖边水库管理所</h3>
                </div>
            </div>
            <div class="state-message">
                <div class="nav">
                    <ul>
                        <li><a href="<%=basePath%>rest/web/index/toIndexList">实时监控</a></li>
                        <li><a href="<%=basePath%>rest/web/eventInfo/toEventInfoList ">事件管理</a></li>
                        <li><a href="<%=basePath%>rest/web/danger/toDangerList">危险管控</a></li>
                        <li><a class="active">人员调度</a></li>
                        <li><a href="<%=basePath%>rest/web/architecture/toArchitectureList">设施管理</a></li>
                        <li><a href="<%=basePath%>rest/web/information/toInformationList">信息管理</a></li>
                        <li><a href="<%=basePath%>rest/web/sysuser/toSysuserList">用户管理</a></li>
                        <li><a href="<%=basePath%>rest/web/about/toAbout">关于我们</a></li>
                    </ul>
                </div>
                <div class="user-message">
                    <div class="dropdown clearfix">
                        <p type="button" id="dropdownMenuDivider" data-toggle="dropdown" aria-haspopup="true"
                           aria-expanded="true">
                            ${sysuser.nickName}
                            <span class="skuiwu-caret caret"></span>
                        </p>
                        <ul class="skuiwu-menu  dropdown-menu" aria-labelledby="dropdownMenuDivider">
                            <li><a href="<%=basePath%>rest/web/sysuser/toPersonal">个人中心</a></li>
                            <li><a href="<%=basePath%>rest/web/toLoginOut">退出登录</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!--内容页面 -->
    <div class="people-content content">
        <div class="people-bs-example  bs-example bs-example-tabs" data-example-id="togglable-tabs">
            <ul id="myTabs" class="people-nav nav nav-tabs">

                <li class="active" id="message-page"><a href="#message" id="message-tab" data-toggle="tab"
                                      aria-controls="cover" aria-expanded="false">信息</a>
                </li>
                <li class="" id="path-page"><a href="#path" role="tab" id="path-tab" data-toggle="tab"
                                aria-controls="build" aria-expanded="true">轨迹</a>
                </li>
            </ul>
            <div id="myTabContent" class="tab-content">
                <div class="tab-pane fade active in" id="message" aria-labelledby="message-tab">
                    <div class="search-input">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="请输入姓名" id="nickName"
                                   aria-describedby="blurBtn">
                            <button class="input-group-addon" id="blurBtn"><img
                                    src="<%=basePath%>/static/images/search.png">
                            </button>
                        </div>
                        <div class="trajectory">
                            <input type="hidden" name="createTime" id="createTime">
                            <button class="btn active" id="todayBtn">今日</button>
                            <button class="btn" id="yesterdayBtn">昨天</button>
                            <button class="btn" id="dateBtn">自定义日期</button>
                        </div>
                        <div class="report-time">
                            <div id="myScrollspy" class="skuiwu-navbar navbar navbar-default navbar-static"
                                 role="navigation">
                                <ul class="header-ul">
                                    <li>最近在线</li>
                                    <li>状态</li>
                                    <li>姓名</li>
                                </ul>
                            </div>
                            <div data-spy="scroll" data-target="#myScrollspy" data-offset="0" class="skuiwu-scroll">
                                <ul class="content-ul" id="reportPeople">
                                </ul>
                            </div>
                        </div>
                        <div class="dealWith-div">
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="path" aria-labelledby="path-tab">
                    <div class="search-input">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="请输入姓名" id="nickPathName"
                                   aria-describedby="blurBtn">
                            <button class="input-group-addon" id="blurPathBtn"><img
                                    src="<%=basePath%>/static/images/search.png">
                            </button>
                        </div>
                        <div class="trajectory">
                            <input type="hidden" name="createTime" id="createPathTime">
                            <button class="btn active" id="todayPathBtn">今日</button>
                            <button class="btn" id="yesterdayPathBtn">昨天</button>
                            <button class="btn" id="datePathBtn">自定义日期</button>
                        </div>
                        <div class="report-time">
                            <div id="myPathScrollspy" class="skuiwu-navbar navbar navbar-default navbar-static"
                                 role="navigation">
                                <ul class="header-ul">
                                    <li>最近在线</li>
                                    <li>状态</li>
                                    <li>姓名</li>
                                </ul>
                            </div>
                            <div data-spy="scroll" data-target="#myPathScrollspy" data-offset="0" class="skuiwu-scroll">
                                <ul class="content-ul" id="reportPathPeople">
                                </ul>
                            </div>
                        </div>
                        <div class="dealPathWith-div">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="left-icon">
            <span class="iconfont icon-zuo"></span>
        </div>
        <div id="allmap">
            <div id="map">
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<%=basePath%>static/js/lib/jquery.min.js"></script>
<script type="text/javascript" src="<%=basePath%>static/js/lib/bootstrap/bootstrap.min.js"></script>
<script type="text/javascript" src="<%=basePath%>static/js/lib/bootstrap/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="<%=basePath%>static/js/lib/bootstrap/bootstrap-datetimepicker.zh-CN.js"></script>
<script type="text/javascript" src="https://api.map.baidu.com/api?v=2.0&ak=ehGF32jTPfCia8QrpLwlKfNpGSX5em2G"></script>
<script type="text/javascript"
        src="https://api.map.baidu.com/library/DrawingManager/1.4/src/DrawingManager_min.js"></script>
<script type="text/javascript"
        src="https://api.map.baidu.com/library/SearchInfoWindow/1.4/src/SearchInfoWindow_min.js"></script>
<script type="text/javascript" src="<%=basePath%>static/js/index.js"></script>
<script type="text/javascript" src="<%=basePath%>static/js/people.js"></script>
<script type="text/javascript">
    $(function() {
        /*时间插件*/
        //信息页面
        $('#dateBtn').datetimepicker({
            language: 'zh-CN',
            minView: "month",
            format: "yyyy-mm-dd",
        }).on('changeDate', function (ev) {
            var createTime = new Date(ev.date).Format("yyyy-MM-dd");
            $("#createTime").val(createTime);
            $('#dateBtn').html(createTime);
            $('#dateBtn').datetimepicker('hide');
            queryPeople();
        });
        //轨迹页面
        $('#datePathBtn').datetimepicker({
            language: 'zh-CN',
            minView: "month",
            format: "yyyy-mm-dd",
        }).on('changeDate', function (ev) {
            var createTime = new Date(ev.date).Format("yyyy-MM-dd");
            $("#createPathTime").val(createTime);
            $('#datePathBtn').html(createTime);
            $('#datePathBtn').datetimepicker('hide');
            queryTrajectory();
        });
        //昨天按钮事件
        $('#yesterdayBtn').click(function () {
            var createTime = new Date(new Date() - 24 * 60 * 60 * 1000).Format("yyyy-MM-dd");
            $("#createTime").val(createTime);
            queryPeople();
        });
        //轨迹页面
        $('#yesterdayPathBtn').click(function () {
            var createTime = new Date(new Date() - 24 * 60 * 60 * 1000).Format("yyyy-MM-dd");
            $("#createPathTime").val(createTime);
            queryTrajectory();
        });
        //今天按钮事件
        $('#todayBtn').click(function () {
            var createTime = new Date().Format("yyyy-MM-dd");
            $("#createTime").val(createTime);
            queryPeople();
        });
        //轨迹页面
        $('#todayPathBtn').click(function () {
            var createTime = new Date().Format("yyyy-MM-dd");
            $("#createPathTime").val(createTime);
            queryTrajectory();
        })
    });
</script>
</body>
</html>
