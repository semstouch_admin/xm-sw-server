<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
  <base href="<%=basePath%>">
  <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
  <title>登录页</title>
  <link rel="stylesheet" type="text/css" href="<%=basePath%>static/js/lib/bootstrap/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="<%=basePath%>static/js/lib/bootstrap/bootstrap-theme.min.css">
  <link rel="stylesheet" type="text/css" href="<%=basePath%>static/css/style.css">
  <link rel="stylesheet" type="text/css" href="https://at.alicdn.com/t/font_i68vcmq939grpb9.css">
</head>
<body>
<div class="wrap">
  <div class="content">
    <div class="shuiwuLogin">
      <div class="title">
        <div>
          <h1>水务生产调度系统</h1>
          <h2>Water Production Scheduling System</h2>
          <h6>万宾云平台</h6>

        </div>
      </div>
      <div class="login-form">
        <form>
          <div class="input-wrap">
            <div>
              <input type="text" placeholder="请输入账号" id="loginName">
              <span class="iconfont icon-renyuan1" aria-hidden="true" ></span>
            </div>
            <div>
              <input type="password" placeholder="请输入密码" id="pwd">
              <span class="iconfont icon-mima" aria-hidden="true" ></span>
            </div>
          </div>
          <a class="btn" id="login" type="button">登录</a>
        </form>
      </div>
    </div>
    <div class="index-word"><p>© 2017 WitBee Tech All Rights Reserved.</p><p>万宾云提供计算服务</p></div>
  </div>
</div>
<script type="text/javascript" src="<%=basePath%>static/js/lib/jquery.min.js"></script>
<script type="text/javascript" src="<%=basePath%>static/js/lib/bootstrap/bootstrap.min.js"></script>
<script type="text/javascript" src="<%=basePath%>static/js/login.js"></script>
</body>
</html>
