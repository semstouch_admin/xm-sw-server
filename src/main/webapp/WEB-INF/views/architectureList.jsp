<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/5/10
  Time: 16:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <base href="<%=basePath%>">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no"/>
    <title>设施管控</title>
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/js/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/js/lib/bootstrap/bootstrap-theme.min.css">
    <link rel="stylesheet" href="https://api.map.baidu.com/library/DrawingManager/1.4/src/DrawingManager_min.css">
    <link rel="stylesheet" href="https://api.map.baidu.com/library/SearchInfoWindow/1.4/src/SearchInfoWindow_min.css"/>
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/css/base.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/css/layout.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/css/master.css">
    <link rel="stylesheet" type="text/css" href="https://at.alicdn.com/t/font_cqeinxuuef3qsemi.css">
</head>
<body>
<div class="wrap">
    <!-- 头部页面 -->
    <header>
        <div class="container">
            <div class="logo">
                <img src="<%=basePath%>static/images/logo.png">
                <div class="title">
                    <h1>水务生产调度系统</h1>
                    <h3>湖边水库管理所</h3>
                </div>
            </div>
            <div class="state-message">
                <div class="nav">
                    <ul>
                        <li><a href="<%=basePath%>rest/web/index/toIndexList">实时监控</a></li>
                        <li><a href="<%=basePath%>rest/web/eventInfo/toEventInfoList ">事件管理</a></li>
                        <li><a href="<%=basePath%>rest/web/danger/toDangerList">危险管控</a></li>
                        <li><a href="<%=basePath%>rest/web/locationLog/toLocationLogList">人员调度</a></li>
                        <li><a class="active">设施管理</a></li>
                        <li><a href="<%=basePath%>rest/web/information/toInformationList">信息管理</a></li>
                        <li><a href="<%=basePath%>rest/web/sysuser/toSysuserList">用户管理</a></li>
                        <li><a href="<%=basePath%>rest/web/about/toAbout">关于我们</a></li>
                    </ul>
                </div>
                <div class="user-message">
                    <div class="dropdown clearfix">
                        <p type="button" id="dropdownMenuDivider" data-toggle="dropdown" aria-haspopup="true"
                           aria-expanded="true">
                            ${sysuser.nickName}
                            <span class="skuiwu-caret caret"></span>
                        </p>
                        <ul class="skuiwu-menu  dropdown-menu" aria-labelledby="dropdownMenuDivider">
                            <li><a href="<%=basePath%>rest/web/sysuser/toPersonal">个人中心</a></li>
                            <li><a href="<%=basePath%>rest/web/toLoginOut">退出登录</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!--内容页面 -->
    <div class="facilities-content content">
        <div class="search-input">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="搜设备名称" id="name" aria-describedby="blurBtn">
                <button class="input-group-addon" id="blurBtn"><img src="<%=basePath%>/static/images/search.png">
                </button>
            </div>
            <div class="skuiwu-bs-example  bs-example bs-example-tabs" data-example-id="togglable-tabs">
                <ul id="myTabs" class="facilities-nav nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#cover" id="cover-tab" role="tab" data-toggle="tab"
                                                              aria-controls="cover" aria-expanded="false">井盖</a>
                    </li>
                    <li role="presentation" class=""><a href="#build" role="tab" id="build-tab" data-toggle="tab"
                                                        aria-controls="build" aria-expanded="true">建筑</a>
                    </li>
                    <li role="presentation" class=""><a href="#sense" role="tab" id="sense-tab" data-toggle="tab"
                                                        aria-controls="sense" aria-expanded="true">传感</a>
                    </li>
                    <li role="presentation" class=""><a href="#box" role="tab" id="box-tab" data-toggle="tab"
                                                        aria-controls="box" aria-expanded="true">涵闸</a>
                    </li>
                </ul>
                <div id="myTabContent" class="tab-content">
                    <div role="tabpanel" class="tab-pane fade active in" id="cover" aria-labelledby="cover-tab">
                        <div class="report-time">
                            <div id="coverScrollspy" class="skuiwu-navbar navbar navbar-default navbar-static"
                                 role="navigation">
                                <ul class="header-ul">
                                    <li>编号</li>
                                    <li>名称</li>
                                </ul>
                            </div>
                            <div data-spy="scroll" data-target="#coverScrollspy" data-offset="0" class="skuiwu-scroll">
                                <ul class="content-ul" id="reportCover">

                                </ul>
                            </div>

                        </div>
                        <div class="dealWith-div" id="coverTotal">

                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="build" aria-labelledby="build-tab">
                        <div class="report-time">
                            <div id="buildScrollspy" class="skuiwu-navbar navbar navbar-default navbar-static"
                                 role="navigation">
                                <ul class="header-ul">
                                    <li>编号</li>

                                    <li>名称</li>
                                </ul>
                            </div>
                            <div data-spy="scroll" data-target="#buildScrollspy" data-offset="0" class="skuiwu-scroll">
                                <ul class="content-ul" id="reportBuild">

                                </ul>
                            </div>

                        </div>
                        <div class="dealWith-div" id="buildTotal">

                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="sense" aria-labelledby="sense-tab">
                        <div class="report-time">
                            <div id="senseScrollspy" class="skuiwu-navbar navbar navbar-default navbar-static"
                                 role="navigation">
                                <ul class="header-ul">
                                    <li>编号</li>

                                    <li>名称</li>
                                </ul>
                            </div>
                            <div data-spy="scroll" data-target="#senseScrollspy" data-offset="0" class="skuiwu-scroll">
                                <ul class="content-ul" id="reportSense">

                                </ul>
                            </div>
                        </div>
                        <div class="dealWith-div" id="senseTotal">

                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="box" aria-labelledby="box-tab">
                        <div class="report-time">
                            <div id="boxScrollspy" class="skuiwu-navbar navbar navbar-default navbar-static"
                                 role="navigation">
                                <ul class="header-ul">
                                    <li>编号</li>

                                    <li>名称</li>
                                </ul>
                            </div>
                            <div data-spy="scroll" data-target="#myScrollspy" data-offset="0" class="skuiwu-scroll">
                                <ul class="content-ul" id="reportHan">

                                </ul>
                            </div>
                        </div>
                        <div class="dealWith-div" id="boxTotal">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="left-icon">
            <span class="iconfont icon-zuo"></span>
        </div>
        <div id="allmap">
            <div id="map">
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<%=basePath%>static/js/lib/jquery.min.js"></script>
<script type="text/javascript" src="<%=basePath%>static/js/lib/bootstrap/bootstrap.min.js"></script>
<script type="text/javascript" src="https://api.map.baidu.com/api?v=2.0&ak=ehGF32jTPfCia8QrpLwlKfNpGSX5em2G"></script>
<script type="text/javascript"
        src="https://api.map.baidu.com/library/DrawingManager/1.4/src/DrawingManager_min.js"></script>
<script type="text/javascript"
        src="https://api.map.baidu.com/library/SearchInfoWindow/1.4/src/SearchInfoWindow_min.js"></script>
<script type="text/javascript" src="<%=basePath%>static/js/index.js"></script>
<script type="text/javascript" src="<%=basePath%>static/js/facilities.js"></script>

</body>
</html>
