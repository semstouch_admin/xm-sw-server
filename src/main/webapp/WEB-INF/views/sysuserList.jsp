<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <base href="<%=basePath%>">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>用户管理</title>
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/js/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/js/lib/bootstrap/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/css/base.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/css/module.css">
</head>
<body>
<div class="wrap" style="height:1200px;">
    <!-- 头部页面 -->
    <header>
        <div class="container">
            <div class="logo">
                <img src="<%=basePath%>static/images/logo.png">

                <div class="title">
                    <h1>水务生产调度系统</h1>
                    <h3>湖边水库管理所</h3>
                </div>
            </div>
            <div class="state-message">
                <div class="nav">
                    <ul>
                        <li><a href="<%=basePath%>rest/web/index/toIndexList">实时监控</a></li>
                        <li><a href="<%=basePath%>rest/web/eventInfo/toEventInfoList ">事件管理</a></li>
                        <li><a href="<%=basePath%>rest/web/danger/toDangerList">危险管控</a></li>
                        <li><a href="<%=basePath%>rest/web/locationLog/toLocationLogList">人员调度</a></li>
                        <li><a href="<%=basePath%>rest/web/architecture/toArchitectureList ">设施管理</a></li>
                        <li><a href="<%=basePath%>rest/web/information/toInformationList">信息管理</a></li>
                        <li><a class="active">用户管理</a></li>
                        <li><a href="<%=basePath%>rest/web/about/toAbout">关于我们</a></li>
                    </ul>
                </div>
                <div class="user-message">
                    <div class="dropdown clearfix">
                        <p type="button" id="dropdownMenuDivider" data-toggle="dropdown" aria-haspopup="true"
                           aria-expanded="true">
                            ${sysuser.nickName}
                            <span class="skuiwu-caret caret"></span>
                        </p>
                        <ul class="skuiwu-menu  dropdown-menu" aria-labelledby="dropdownMenuDivider">
                            <li><a href="<%=basePath%>rest/web/sysuser/toPersonal">个人中心</a></li>
                            <li><a href="<%=basePath%>rest/web/toLoginOut">退出登录</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!--内容页面 -->
    <div class="user-content content">
        <div class="container">
            <div class="add-user">
                <div class="search-input">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="请输入关键字" id="name"
                               aria-describedby="blurBtn">
                        <span class="input-group-addon" id="blurBtn"><img
                                src="<%=basePath%>static/images/search.png"></span>
                    </div>
                </div>
                <div class="add-btn">
                    <button class=" btn" data-toggle="modal" data-target="#add-userModal">新增账号</button>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered" id="user-list">
                    <tbody></tbody>
                </table>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="add-userModal" tabindex="-1" role="dialog" aria-labelledby="addUser">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="addUser">新增账号</h4>
                    </div>
                    <div class="modal-body m-body">
                        <form id="add-form">
                            <div class="input-group">
                                <label><span>类型:</span></label>
                                <select class="form-control" id="roleType" name="roleType">
                                    <option value="1">系统用户</option>
                                    <option value="2">APP用户</option>
                                </select>
                            </div>
                            <div class="input-group">
                                <label><span>账号:</span></label>
                                <input class="form-control" type="text" id="loginName" name="loginName">
                            </div>
                            <div class="pwd-input-group input-group">
                                <label><span>密码:</span></label>
                                <input class="form-control" type="text" id="pwd" name="pwd" placeholder="">
                            </div>
                            <div class="input-group ">
                                <label><span>姓名:</span></label>
                                <input class="form-control" type="text" id="nickName" name="nickName">
                            </div>
                            <div class="input-group">
                                <label><span>手机号:</span></label>
                                <input class="form-control" type="text" id="phone" name="phone">
                            </div>
                            <div class="input-group">
                                <label><span>所属部门:</span></label>
                                <input class="form-control" type="text" id="department" name="department">
                            </div>
                            <div class="input-group">
                                <label><span>状态:</span></label>
                                <select class="form-control" id="status" name="status">
                                    <option value="1">启用</option>
                                    <option value="0">禁用</option>
                                </select>
                            </div>
                            <div class="remark-input input-group">
                                <label><span>备注:</span></label>
                                <textarea class="form-control" type="text" id="remarks" name="remarks"></textarea>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn ensure-btn" id="addBtn">确定</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="delete-userModal" tabindex="-1" role="dialog" aria-labelledby="prompt">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="prompt">提示信息</h4>
                    </div>
                    <div class="modal-body">
                        <div>
                            <input id="dataId" type="hidden" value=""/>
                        </div>
                        <p>您确认要删除吗？</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="delBtn btn ensure-btn">确定</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="edit-userModal" tabindex="-1" role="dialog" aria-labelledby="edit">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="edit">编辑账号</h4>
                    </div>
                    <div class="modal-body m-body">
                        <div class="input-group">
                            <input id="id" type="hidden" value=""/>
                        </div>
                        <div class="input-group">
                            <label><span>类型:</span></label>
                            <select class="form-control" id="edit-roleType" disabled="disabled" name="roleType">
                                <option value="1">系统用户</option>
                                <option value="2">APP用户</option>
                            </select>
                        </div>
                        <div class="input-group">
                            <label><span>账号:</span></label>
                            <input class="form-control" type="text" id="edit-loginName" disabled="disabled">
                        </div>
                        <div class="input-group">
                            <label><span>密码:</span></label>
                            <input class="form-control" type="text" id="edit-pwd">
                        </div>
                        <div class="input-group ">
                            <label><span>姓名:</span></label>
                            <input class="form-control" type="text" id="edit-nickName">
                        </div>
                        <div class="input-group">
                            <label><span>手机号:</span></label>
                            <input class="form-control" type="text" id="edit-phone">
                        </div>
                        <div class="input-group">
                            <label><span>所属部门:</span></label>
                            <input class="form-control" type="text" id="edit-department">
                        </div>
                        <div class="input-group">
                            <label><span>状态:</span></label>
                            <select class="form-control" id="edit-status" name="status">
                                <option value="1">启用</option>
                                <option value="0">禁用</option>
                            </select>
                        </div>
                        <div class="remark-input input-group">
                            <label><span>备注:</span></label>
                            <textarea class="form-control" type="text" id="edit-remarks"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn ensure-btn" id="submitBtn">确定</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<%=basePath%>static/js/lib/jquery.min.js"></script>
<script type="text/javascript" src="<%=basePath%>static/js/lib/bootstrap/bootstrap.min.js"></script>
<script type="text/javascript" src="<%=basePath%>static/js/index.js"></script>
<script type="text/javascript" src="<%=basePath%>static/js/user.js"></script>

</body>
</html>
