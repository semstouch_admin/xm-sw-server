<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <base href="<%=basePath%>">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no"/>
    <title>危险管控</title>
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/js/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/js/lib/bootstrap/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/js/lib/viewer/viewer.min.css">
    <link rel="stylesheet" href="https://api.map.baidu.com/library/DrawingManager/1.4/src/DrawingManager_min.css">
    <link rel="stylesheet" href="https://api.map.baidu.com/library/SearchInfoWindow/1.4/src/SearchInfoWindow_min.css"/>
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/css/base.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/css/layout.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/css/master.css">
    <link rel="stylesheet" type="text/css" href="https://at.alicdn.com/t/font_cqeinxuuef3qsemi.css">
</head>
<body>
<div class="wrap">
    <!-- 头部页面 -->
    <header>
        <div class="container">
            <div class="logo">
                <img src="<%=basePath%>static/images/logo.png">

                <div class="title">
                    <h1>水务生产调度系统</h1>

                    <h3>湖边水库管理所</h3>
                </div>
            </div>
            <div class="state-message">
                <div class="nav">
                    <ul>
                        <li><a href="<%=basePath%>rest/web/index/toIndexList">实时监控</a></li>
                        <li><a href="<%=basePath%>rest/web/eventInfo/toEventInfoList ">事件管理</a></li>
                        <li><a class="active">危险管控</a></li>
                        <li><a href="<%=basePath%>rest/web/locationLog/toLocationLogList">人员调度</a></li>
                        <li><a href="<%=basePath%>rest/web/architecture/toArchitectureList">设施管理</a></li>
                        <li><a href="<%=basePath%>rest/web/information/toInformationList">信息管理</a></li>
                        <li><a href="<%=basePath%>rest/web/sysuser/toSysuserList">用户管理</a></li>
                        <li><a href="<%=basePath%>rest/web/about/toAbout">关于我们</a></li>
                    </ul>
                </div>
                <div class="user-message">
                    <div class="dropdown clearfix">
                        <p type="button" id="dropdownMenuDivider" data-toggle="dropdown" aria-haspopup="true"
                           aria-expanded="true">
                            ${sysuser.nickName}
                            <span class="skuiwu-caret caret"></span>
                        </p>
                        <ul class="skuiwu-menu  dropdown-menu" aria-labelledby="dropdownMenuDivider">
                            <li><a href="<%=basePath%>rest/web/sysuser/toPersonal">个人中心</a></li>
                            <li><a href="<%=basePath%>rest/web/toLoginOut">退出登录</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!--内容页面 -->
    <div class="danger-content content">
        <div class="search-input">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="请输入关键字" id="title"  aria-describedby="blurBtn">
                <button class="input-group-addon" id="blurBtn"><img src="<%=basePath%>static/images/search.png">
                </button>
            </div>
            <div class="report-time">
                <div id="myScrollspy" class="skuiwu-navbar navbar navbar-default navbar-static" role="navigation">
                    <ul class="header-ul">
                        <li>上报时间</li>
                        <li>状态</li>
                        <li>主题</li>
                    </ul>
                </div>
                <div data-spy="scroll" data-target="#myScrollspy" data-offset="0" class="skuiwu-scroll">
                    <ul class="content-ul" id="reportDanger">

                    </ul>
                </div>
            </div>
            <div class="dealWith-div">
            </div>
        </div>
        <div class="left-icon">
            <span class="iconfont icon-zuo"></span>
        </div>
        <div id="allmap">
            <div id="map">
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<%=basePath%>static/js/lib/jquery.min.js"></script>
<script type="text/javascript" src="<%=basePath%>static/js/lib/bootstrap/bootstrap.min.js"></script>
<script type="text/javascript" src="<%=basePath%>static/js/lib/viewer/viewer.min.js"></script>
<script type="text/javascript" src="https://api.map.baidu.com/api?v=2.0&ak=ehGF32jTPfCia8QrpLwlKfNpGSX5em2G"></script>
<script type="text/javascript"
        src="https://api.map.baidu.com/library/DrawingManager/1.4/src/DrawingManager_min.js"></script>
<script type="text/javascript"
        src="https://api.map.baidu.com/library/SearchInfoWindow/1.4/src/SearchInfoWindow_min.js"></script>
<script type="text/javascript" src="<%=basePath%>static/js/index.js"></script>
<script type="text/javascript" src="<%=basePath%>static/js/danger.js"></script>

</body>
</html>
