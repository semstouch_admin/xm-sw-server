<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/5/8
  Time: 16:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <base href="<%=basePath%>">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <title>实时监控</title>
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/js/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/js/lib/bootstrap/bootstrap-theme.min.css">
    <link rel="stylesheet" href="https://api.map.baidu.com/library/DrawingManager/1.4/src/DrawingManager_min.css">
    <link rel="stylesheet" href="https://api.map.baidu.com/library/SearchInfoWindow/1.4/src/SearchInfoWindow_min.css" />
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/css/base.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/css/layout.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/css/master.css">
    <link rel="stylesheet" type="text/css" href="https://at.alicdn.com/t/font_etbob2acsdpr2j4i.css">
</head>
<body>
<div class="wrap">
    <!-- 头部页面 -->
    <header>
        <div class="container">
            <div class="logo">
                <img src="<%=basePath%>static/images/logo.png">
                <div class="title">
                    <h1>水务生产调度系统</h1>
                    <h3>湖边水库管理所</h3>
                </div>
            </div>
            <div class="state-message">
                <div class="nav">
                    <ul>
                        <li><a class="active">实时监控</a></li>
                        <li><a href="<%=basePath%>rest/web/eventInfo/toEventInfoList">事件管理</a></li>
                        <li><a href="<%=basePath%>rest/web/danger/toDangerList">危险管控</a></li>
                        <li><a href="<%=basePath%>rest/web/locationLog/toLocationLogList">人员调度</a></li>
                        <li><a href="<%=basePath%>rest/web/architecture/toArchitectureList">设施管理</a></li>
                        <li><a href="<%=basePath%>rest/web/information/toInformationList">信息管理</a></li>
                        <li><a href="<%=basePath%>rest/web/sysuser/toSysuserList">用户管理</a></li>
                        <li><a href="<%=basePath%>rest/web/about/toAbout">关于我们</a></li>
                    </ul>
                </div>
                <div class="user-message">
                    <div class="dropdown clearfix">
                        <p type="button" id="dropdownMenuDivider" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            ${sysuser.nickName}
                            <span class="skuiwu-caret caret"></span>
                        </p>
                        <ul class="skuiwu-menu  dropdown-menu" aria-labelledby="dropdownMenuDivider">
                            <li><a href="<%=basePath%>rest/web/sysuser/toPersonal">个人中心</a></li>
                            <li><a href="<%=basePath%>rest/web/toLoginOut">退出登录</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!--内容页面 -->
    <div class="time-content content">
        <%--<div class="search-input">--%>
            <%--<div class="input-group">--%>
                <%--<input type="text" class="form-control" placeholder="搜地点" aria-describedby="basic-addon2">--%>
                <%--<button class="input-group-addon" id="basic-addon2"><img src="<%=basePath%>/static/images/search.png"></button>--%>
            <%--</div>--%>
        <%--</div>--%>
        <div id="allmap">
            <div id="map">
            </div>
        </div>
        <div class="twoMenu">
            <ul>
                <li class="active"  data-type="f" onclick="queryDevice();"><span class="iconfont icon-shijian-"></span>
                    <p>设施</p>
                </li>
                <li  data-type="n" onclick="queryPipe();"><span class="iconfont icon-earth"></span>
                    <p>管网</p>
                </li>
                <li data-type="d" onclick="queryDanger();"><span class="iconfont icon-iconset0211"></span>
                    <p>危险</p>
                </li>
                <li  data-type="p" onclick="queryPeople();"><span class="iconfont icon-renyuan"></span>
                    <p>人员</p>
                </li>
                <li  data-type="e" onclick="queryEvent();"><span class="iconfont icon-shijian"></span>
                    <p>事件</p>
                </li>
            </ul>
        </div>
    </div>
</div>
<script type="text/javascript" src="<%=basePath%>static/js/lib/jquery.min.js"></script>
<script type="text/javascript" src="<%=basePath%>static/js/lib/bootstrap/bootstrap.min.js"></script>
<script type="text/javascript" src="https://api.map.baidu.com/api?v=2.0&ak=ehGF32jTPfCia8QrpLwlKfNpGSX5em2G"></script>
<script type="text/javascript" src="https://api.map.baidu.com/library/DrawingManager/1.4/src/DrawingManager_min.js"></script>
<script type="text/javascript" src="https://api.map.baidu.com/library/SearchInfoWindow/1.4/src/SearchInfoWindow_min.js"></script>
<script type="text/javascript" src="<%=basePath%>static/js/index.js"></script>
<script type="text/javascript" src="<%=basePath%>static/js/indexList.js"></script>

</body>
</html>
