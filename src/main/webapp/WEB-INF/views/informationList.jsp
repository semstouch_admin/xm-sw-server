<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <base href="<%=basePath%>">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no"/>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no"/>
    <title>信息管理</title>
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/js/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/js/lib/bootstrap/bootstrap-theme.min.css">
    <link rel="stylesheet" href="https://api.map.baidu.com/library/DrawingManager/1.4/src/DrawingManager_min.css">
    <link rel="stylesheet" href="https://api.map.baidu.com/library/SearchInfoWindow/1.4/src/SearchInfoWindow_min.css"/>
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/css/base.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/css/master.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>static/css/themes.css">
</head>
<body>
<div class="wrap">
    <!-- 头部页面 -->
    <header>
        <div class="container">
            <div class="logo">
                <img src="<%=basePath%>/static/images/logo.png">

                <div class="title">
                    <h1>水务生产调度系统</h1>

                    <h3>湖边水库管理所</h3>
                </div>
            </div>
            <div class="state-message">
                <div class="nav">
                    <ul>
                        <li><a href="<%=basePath%>rest/web/index/toIndexList">实时监控</a></li>
                        <li><a href="<%=basePath%>rest/web/eventInfo/toEventInfoList">事件管理</a></li>
                        <li><a href="<%=basePath%>rest/web/danger/toDangerList">危险管控</a></li>
                        <li><a href="<%=basePath%>rest/web/locationLog/toLocationLogList">人员调度</a></li>
                        <li><a href="<%=basePath%>rest/web/architecture/toArchitectureList">设施管理</a></li>
                        <li><a class="active">信息管理</a></li>
                        <li><a href="<%=basePath%>rest/web/sysuser/toSysuserList">用户管理</a></li>
                        <li><a href="<%=basePath%>rest/web/about/toAbout">关于我们</a></li>
                    </ul>
                </div>
                <div class="user-message">
                    <div class="dropdown clearfix">
                        <p type="button" id="dropdownMenuDivider" data-toggle="dropdown" aria-haspopup="true"
                           aria-expanded="true">
                            ${sysuser.nickName}
                            <span class="skuiwu-caret caret"></span>
                        </p>
                        <ul class="skuiwu-menu  dropdown-menu" aria-labelledby="dropdownMenuDivider">
                            <li><a href="<%=basePath%>rest/web/sysuser/toPersonal">个人中心</a></li>
                            <li><a href="<%=basePath%>rest/web/toLoginOut">退出登录</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!--内容页面 -->
    <div class="message-content content">
        <div class="search-input">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="请输入设备的名称" id="name" aria-describedby="blurBtn">
                <span class="input-group-addon" id="blurBtn"><img src="<%=basePath%>static/images/search.png"></span>
            </div>
        </div>
        <div class="add-btn dropdown clearfix">
            <button class="btn" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="true">新增设施
            </button>
            <ul class="skuiwu-menu  dropdown-menu" aria-labelledby="dropdownMenu1">
                <li class="build"><a data-type="b">建筑</a></li>
                <li class="sense"><a data-type="s">传感</a></li>
                <li class="cover"><a data-type="c">井盖</a></li>
                <li class="han"><a data-type="h">涵闸</a></li>
                <li class="pipe"><a data-type="p">管网</a></li>
            </ul>
        </div>
        <div id="allmap">
            <div id="map">
            </div>
        </div>
        <div class="modal fade" id="delete-userModal" tabindex="-1" role="dialog" aria-labelledby="prompt">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="prompt">提示信息</h4></div>
                    <div class="modal-body">
                        <div><input id="dataId" type="hidden" value=""/></div>
                        <p>您确认要删除吗？</p></div>
                    <div class="modal-footer">
                        <button type="button" class="delBtn btn ensure-btn">确定</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript" src="<%=basePath%>static/js/lib/jquery.min.js"></script>
<script type="text/javascript" src="<%=basePath%>static/js/lib/bootstrap/bootstrap.min.js"></script>
<script type="text/javascript" src="https://api.map.baidu.com/api?v=2.0&ak=ehGF32jTPfCia8QrpLwlKfNpGSX5em2G"></script>
<script type="text/javascript"
        src="https://api.map.baidu.com/library/DrawingManager/1.4/src/DrawingManager_min.js"></script>
<script type="text/javascript"
        src="https://api.map.baidu.com/library/SearchInfoWindow/1.4/src/SearchInfoWindow_min.js"></script>
<script type="text/javascript" src="https://api.map.baidu.com/library/CurveLine/1.5/src/CurveLine.min.js"></script>
<script type="text/javascript" src="<%=basePath%>static/js/index.js"></script>
<script type="text/javascript" src="<%=basePath%>static/js/information.js"></script>
</body>
</html>
