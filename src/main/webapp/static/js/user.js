/**
 * Created by Administrator on 2017/5/8.
 */
/**查询全部用户**/
function queryUser() {
    $.ajax({
        type: "GET",
        url: "rest/web/sysuser/selectSysuserList",
        dataType: "json",
        success: function (data) {
            var userList = data.data;
            if (userList.lenght == 0) {
                $("#user-list").html("暂时查不到您要的数据！");
            } else {
                var userListHtml = "<tr><th>序号</th> <th>类型</th> <th>账号</th><th>姓名</th><th>手机号</th> <th>所属部门</th> <th>授权码</th> <th>操作</th> </tr>";
                $.each(userList, function (i, n) {
                    i += 1;
                    var roleName = (n.roleType == "1" ? "系统用户" : "APP用户");
                    userListHtml = userListHtml + "<tr> <td> <label>"
                        + i + "</label></td><td><span>"
                        + roleName + "</span></td><td><span>"
                        + n.loginName + "</span></td><td><span>"
                        + n.nickName + "</span></td><td><span>"
                        + n.phone + "</span></td><td><span>"
                        + n.department + "</span></td><td><span>"
                        + (n.activeCode == null ? "-" : n.activeCode)  + "</span></td><td class='user-edit'><a class='editBtn' data-toggle='modal' data-target='#edit-userModal' data-id='"
                        + n.id + "'>编辑&nbsp;&nbsp;</a><a class='deleteBtn' data-toggle='modal' data-target='#delete-userModal'  data-id='"
                        + n.id + "'>删除</a></td></tr>"
                });
                $("#user-list").html(userListHtml);
                //这个编辑时先把id传到模态窗口的
                $(".deleteBtn").click(function () {
                    $("#dataId").val($(this).attr("data-id"));
                });
                //这个delBtn是模态窗口的确定按钮c
                $(".delBtn").click(function () {
                    delUser($("#dataId").val());
                });
                $(".editBtn").click(function () {
                    querySingleUser($(this).attr("data-id"));
                })
            }
        }
    });
}
/**查询单个用户**/
function querySingleUser(id) {
    var param = "id=" + id;
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "rest/web/sysuser/selectSysuserDetail",
        data: param,
        success: function (data) {
            var user = data.data;
            $("#id").val(user.id);
            $("#edit-roleType").val(user.roleType);
            $("#edit-loginName ").val(user.loginName);
            $("#edit-nickName").val(user.nickName);
            $("#edit-phone").val(user.phone);
            $("#edit-department").val(user.department);
            $("#edit-remarks").val(user.remarks);
            $("#edit-status").val(user.status);
            //给提交按钮绑定事件
            $("#submitBtn").click(function () {
                updateUser();
            })
        }
    });
}
/**
 * @decription：更新用户方法
 * @author：xiaoxiaoxia
 */
function updateUser() {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: 'rest/web/sysuser/updateSysuser',
        data: {
            id: $("#id").val(),
            roleType: $("#edit-roleType").val(),
            loginName: $("#edit-loginName").val(),
            pwd: $("#edit-pwd").val(),
            nickName: $("#edit-nickName").val(),
            phone: $("#edit-phone").val(),
            activeCode: $("#edit-activeCode").val(),
            department: $("#edit-department").val(),
            remarks: $("#edit-remarks").val(),
            status: $("#edit-status").val()
        },
        success: function (data) {
            window.location.href = "rest/web/sysuser/toSysuserList ";
        }
    });
}
/**删除用户**/
function delUser(id) {
    var param = "id=" + id;
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "rest/web/sysuser/deleteSysuser",
        data: param,
        success: function () {
            window.location.href = "rest/web/sysuser/toSysuserList ";
        }
    });
}

/**添加用户**/
$("#addBtn").click(function () {
    var param = $("#add-form").serialize();
    $.ajax({
        url: "rest/web/sysuser/insertSysuser",
        type: "POST",
        data: param,
        success: function (data) {
                window.location.href = "rest/web/sysuser/toSysuserList ";

        }
    });
});
//系统用户加密码，APP用户不加密码
$("#roleType").change(function(){
    if($(this).val()=='2') {
        $(".pwd-input-group").hide();
    }
    else{
        $(".pwd-input-group").show();
    }
});
/**搜索框的模糊查询**/
$("#blurBtn").click(function () {
    var nickName = $("#name").val();
    $.ajax({
        type: "GET",
        url: "rest/web/sysuser/selectSysuserList",
        dataType: "json",
        data: {nickName: nickName},
        success: function (data) {
            var userList = data.data;
            if (userList.lenght == 0) {
                $("#user-list").html("暂时查不到您要的数据！");
            } else {
                var userListHtml = "<tr><th>序号</th> <th>类型</th> <th>账号</th><th>密码</th><th>姓名</th><th>手机号</th> <th>所属部门</th> <th>授权码</th> <th>操作</th> </tr>";
                $.each(userList, function (i, n) {
                    i += 1;
                    var roleName = (n.roleType == "1" ? "系统用户" : "APP用户");
                    userListHtml = userListHtml + "<tr> <td> <label>"
                        + i + "</label></td><td><span>"
                        + roleName + "</span></td><td><span>"
                        + n.loginName + "</span></td><td><span>"
                        + n.pwd+ "</span></td><td><span>"
                        + n.nickName + "</span></td><td><span>"
                        + n.phone + "</span></td><td><span>"
                        + n.department + "</span></td><td><span>"
                        + (n.activeCode == null ? "-" : n.activeCode) + "</span></td><td class='user-edit'><a class='editBtn' data-toggle='modal' data-target='#edit-userModal' data-id='"
                        + n.id + "'>编辑&nbsp;&nbsp;</a><a class='deleteBtn' data-toggle='modal' data-target='#delete-userModal'  data-id='"
                        + n.id + "'>删除</a></td></tr>"
                });
                $("#user-list").html(userListHtml);
                //这个编辑时先把id传到模态窗口的
                $(".deleteBtn").click(function () {
                    $("#dataId").val($(this).attr("data-id"));
                });
                //这个delBtn是模态窗口的确定按钮
                $(".delBtn").click(function () {
                    delUser($("#dataId").val());
                });
                $(".editBtn").click(function () {
                    querySingleUser($(this).attr("data-id"));
                })
            }
        }
    });
});
queryUser();