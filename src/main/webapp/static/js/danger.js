/**
 * Created by Administrator on 2017/5/10.
 */
var map = new BMap.Map("map");  // 创建Map实例
var point = new BMap.Point(118.10388605, 24.48923061);// 初始化地图,用城市名设置地图中心点
map.centerAndZoom(point, 15);//
map.enableScrollWheelZoom();//地图可以缩放

//声明两个对象用来缓存marker和dangerWindow，使用id来查询和添加缓存
var cacheMap = null;
var infoWindowMap = null;

/**
 * 自定义map
 * @constructor
 */
function Map() {

    var mapObj = {};

    this.put = function (key, value) {
        mapObj[key] = value;
    };

    this.remove = function (key) {
        if (mapObj.hasOwnProperty(key)) {
            delete mapObj[key];
        }
    };

    this.get = function (key) {
        if (mapObj.hasOwnProperty(key)) {
            return mapObj[key];
        }
        return null;
    };

    this.getKeys = function () {
        var keys = [];
        for (var k in mapObj) {
            keys.push(k);
        }
        return keys;
    };

    // 遍历map
    this.each = function (fn) {
        for (var key in mapObj) {
            fn(key, mapObj[key]);
        }
    };
    this.toString = function () {
        var str = "{";
        for (var k in mapObj) {
            str += "\"" + k + "\" : \"" + mapObj[k] + "\",";
        }
        str = str.substring(0, str.length - 1);
        str += "}";
        return str;
    }

}

/**查询在百度地图上全部危险**/
function queryDanger() {
    $.ajax({
        type: "GET",
        url: "/rest/web/danger/selectDangerList",
        dataType: "json",
        data: {
            title: $("#title").val()
        },
        success: function (data) {
            cacheMap = new Map();
            infoWindowMap = new Map();

            var dangerList = data.data;
            $.each(dangerList, function (i, o) {
                setEventPoint(o);
            });
            var reportDangerHtml = "";
            var statusTotal = 0;//声明未处理的总数
            $.each(dangerList, function (i, n) {
                //查询未处理的次数
                if (n.status == "0") {
                    statusTotal = statusTotal + 1;
                }
                var status = (n.status == "0" ? "未处理" : "已完成");
                if (i == 0) {
                    getSingleDanger(n.id);
                    reportDangerHtml = reportDangerHtml + "<li data-id='" + n.id + "'> <p>" + n.createTime + "</p> <p>" + status + "</p> <p>" + n.title + "</p></li>";
                }
                else {
                    reportDangerHtml = reportDangerHtml + "<li data-id='" + n.id + "'> <p>" + n.createTime + "</p> <p>" + status + "</p><p>" + n.title + "</p></li>";
                }
            });
            $("#reportDanger").html(reportDangerHtml);
            $("#reportDanger>li").click(function () {
                getSingleDanger($(this).attr("data-id"));
            });
            //求出总数的方法
            var data = data.data;
            var dealWith = "";
            dealWith += "<div><p class='statusTotal'>未处理：<span>" + statusTotal + "</span></p><p>/总数：<span>" + data.length + "</span></p></div>";
            $(".dealWith-div").html(dealWith);
        }
    });
}
/**
 * 根据id把缓存的marker和dangerWindow拼装后调用map的openInfoWindow方法弹窗展示
 * @param id
 */
function getSingleDanger(id) {
    var infoWindow = infoWindowMap.get(id);
    var marker = cacheMap.get(id);
    querySingleDanger(id, infoWindow, marker)
}
/**
 * 绘制点危险
 * @param obj
 */

function setEventPoint(obj) {
    var dangerContent = "<div class='skuiwu-danger danger' ><div class='danger-info skuiwu-navbar navbar navbar-default navbar-static' id='myScrollspy'>" +
        "<div class='header'><h3>危险管控&nbsp;&nbsp;&nbsp;" + obj.id + "</h3></div>" +
        "<div class='content'>" +
        "<div class='message'><h2>" + obj.title + "</h2></div>" +
        "<div class='details'><h6>地址：</h6><p>" + obj.address + "</p></div>" +
        "<div class='classify'><h6>坐标：</h6><p class='coordinate'>" + obj.lng + "," + obj.lat + "</p></div>" +
        "</div></div>" +
        "</div>";
    var dangerWindow = new BMap.InfoWindow(dangerContent); // 创建信息窗口对象
    var pt = new BMap.Point(obj.lng, obj.lat);
    var myIcon = new BMap.Icon("/static/images/map_danger.png", new BMap.Size(22, 22));
    var marker = new BMap.Marker(pt, {icon: myIcon}); // 创建标注
    map.addOverlay(marker);
    marker.addEventListener("click", function () {
        querySingleDanger(obj.id, dangerWindow, marker);
    });
    //将marker和eventWindow缓存起来
    cacheMap.put(obj.id, marker);
    infoWindowMap.put(obj.id, dangerWindow);
}


/**
 * 查询单条危险信息，在原有的基础上新增详情
 * @param id  危险编号
 * @param dangerWindow  原本弹窗对象
 * @param marker  危险点
 */
function querySingleDanger(id, dangerWindow, marker) {
    $.ajax({
        type: "GET",
        url: "/rest/web/danger/selectDangerInfoList ",
        data: {
            dangerID: id
        },
        dataType: "json",
        success: function (data) {
            var dangerList = data.data;
            var reportDangerHtml = "<div data-spy='scroll' data-target='#myScrollspy' data-offset='0' class='skuiwu-scroll'>";
            $.each(dangerList, function (i, n) {
                reportDangerHtml = reportDangerHtml + "<div class='footer'>" +
                    "<div class='danger-details'>" +
                    "<div class='user-message'><img src='" + n.userIcon + "'><div><h1>" + n.nickName + "</h1><h5>" + n.department + "</h5></div></div>" +
                    "<div class='details '><h6>时间：</h6><p>" + n.createTime + "</p></div>" +
                    "<div class='details danger-content'><h6>内容：</h6><p>" + n.content + "</p></div>" +
                    "<div class='details-images'><h6>图片：</h6><ul class='images'>";
                if (n.pic != null) {
                    var imgArr = n.pic.split(",");
                    for (var j = 0; j < imgArr.length; j++) {
                        reportDangerHtml = reportDangerHtml + "<li><img src='" + imgArr[j] + "' class='imgBtn'></li>";
                    }
                }
                reportDangerHtml = reportDangerHtml + "</ul></div></div></div>";

            });
            reportDangerHtml = reportDangerHtml + "</div>";

            //获取原本的弹窗内容
            var content = dangerWindow.getContent();

            //获取data的数据拼接一个字符串
            var dangerInfo = $(content).find(".danger-info").eq(0);

            var dangerContent = "<div class='skuiwu-danger danger' >" + dangerInfo.prop('outerHTML') + reportDangerHtml + "<div>";
            /**点击照片放大效果**/
            $(".imgBtn").click(function () {
                if ($(".imgBtn").css("width") == "80px") {
                    $(this).css("width", "200px");
                } else {
                    $(this).css("width", "80px");
                }

            });
            //把原本的弹窗内容和新增的内容进行拼接
            dangerWindow.setContent(dangerContent);

            marker.openInfoWindow(dangerWindow);
            $('.images').viewer();
        }
    })
}

/**搜索框的模糊查询**/
$("#blurBtn").click(function () {
    queryDanger();
});
queryDanger();


