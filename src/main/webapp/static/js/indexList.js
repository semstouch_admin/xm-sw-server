/**
 * Created by Administrator on 2217/5/14.
 */
var map = new BMap.Map("map");  // 创建Map实例
var point = new BMap.Point(118.10388605, 24.48923061);// 初始化地图,用城市名设置地图中心点
map.centerAndZoom(point, 15);//
map.enableScrollWheelZoom();//地图可以缩放


//二级菜单中的li的active转换
$(".twoMenu ul li").click(function () {
    $(".twoMenu ul li  ").removeClass("active");
    $(this).addClass("active");
});
/************************************************************设施方法*************************************/
/**查询全部设施**/
function queryDevice() {
    map.clearOverlays();
    querySense();
    queryBuild();
    queryCover();
    queryHan();


}

queryDevice();
/**查询全部传感**/
function querySense() {
    $.ajax({
        type: "GET",
        url: "/rest/web/index/selectSenseList",
        dataType: "json",
        success: function (data) {
            var senseList = data.msg;
            $.each(senseList, function (i, o) {
                setSensePoint(o);
            });
        }
    });
}

/**
 * 绘制传感点
 * @param obj
 */
function setSensePoint(obj) {
    var senseContent = "";
    senseContent = senseContent + "<div class='senseContent' id='senseDemo'>" +
        "<div><h6>类别：</h6><p>" + obj.type + "</p></div>" +
        "<div><h6>名称：</h6><p>" + obj.name + "</p></div>" +
        "<div><h6>编号：</h6><p>" + obj.sno + "</p></div>" +
        "<div><h6>年份：</h6><p>" + obj.year + "</p></div>" +
        "<div><h6>规格：</h6><p>" + obj.spec + "</p></div>" +
        "<div><h6>材质：</h6><p>" + obj.quality + "</p></div>" +
        "<div><h6>寿命：</h6><p>" + obj.age + "</p></div>" +
        "<div><h6>责任：</h6><p>" + obj.personLiable + "</p></div>" +
        "<div><h6>部门：</h6><p>" + obj.department + "</p></div>" +
        "<div class='remarks' ><h6>备注：</h6><p>" + obj.remarks + "</p></div>" +
        "</div>" +
        "</div>";

    var senseWindow = new BMap.InfoWindow(senseContent); // 创建信息窗口对象
    //创建传感小图标
    var pt = new BMap.Point(obj.lng, obj.lat);
    var myIcon = new BMap.Icon("/static/images/map_sensor.png", new BMap.Size(22, 22));
    var marker = new BMap.Marker(pt, {icon: myIcon}); // 创建标注
    map.addOverlay(marker);
    marker.addEventListener("click", function () {
        this.openInfoWindow(senseWindow);
        //图片加载完毕重绘infowindow
        document.getElementById('senseDemo').onload = function () {
            senseWindow.redraw(); //防止在网速较慢，图片未加载时，生成的信息框高度比图片的总高度小，导致图片部分被隐藏
        }
    });
}

/**查询全部建筑**/
function queryBuild() {
    $.ajax({
        type: "GET",
        url: "/rest/web/index/selectArchitectureList",
        dataType: "json",
        success: function (data) {
            var buildList = data.data;
            $.each(buildList, function (i, o) {
                setBuildPoint(o);
            });
        }
    });
}

function setBuildPoint(obj) {
    var buildContent = "";
    buildContent = buildContent + "<div class='senseContent' id='buildDemo'>" +
        "<div><h6>类别：</h6><p>" + obj.type + "</p></div>" +
        "<div><h6>名称：</h6><p>" + obj.name + "</p></div>" +
        "<div><h6>编号：</h6><p>" + obj.sno + "</p></div>" +
        "<div><h6>年份：</h6><p>" + obj.year + "</p></div>" +
        "<div><h6>规格：</h6><p>" + obj.spec + "</p></div>" +
        "<div><h6>寿命：</h6><p>" + obj.age + "</p></div>" +
        "<div><h6>责任：</h6><p>" + obj.personLiable + "</p></div>" +
        "<div><h6>部门：</h6><p>" + obj.department + "</p></div>" +
        "<div class='remarks'><h6>备注：</h6><p>" + obj.remarks + "</p></div>" +
        "</div>";

    var buildWindow = new BMap.InfoWindow(buildContent); // 创建信息窗口对象
    //创建建筑小图标
    var pt = new BMap.Point(obj.lng, obj.lat);
    var myIcon = new BMap.Icon("/static/images/map_building.png", new BMap.Size(22, 22));
    var marker = new BMap.Marker(pt, {icon: myIcon}); // 创建标注
    map.addOverlay(marker);
    marker.addEventListener("click", function () {
        this.openInfoWindow(buildWindow);
        //图片加载完毕重绘infowindow
        document.getElementById('buildDemo').onload = function () {
            buildWindow.redraw(); //防止在网速较慢，图片未加载时，生成的信息框高度比图片的总高度小，导致图片部分被隐藏
        }
    });
}

/**查询全部井盖**/
function queryCover() {
    $.ajax({
        type: "GET",
        url: "/rest/web/index/selectManholeList ",
        dataType: "json",
        success: function (data) {
            var coverList = data.data;
            $.each(coverList, function (i, o) {
                setCoverPoint(o);
            });
        }
    });
}

/**
 * 绘制井盖的点
 * @param obj
 */
function setCoverPoint(obj) {
    var coverContent = "";
    coverContent = coverContent + "<div class='senseContent' id='coverDemo'>" +
        "<div><h6>类别：</h6><p>" + obj.type + "</p></div>" +
        "<div><h6>名称：</h6><p>" + obj.name + "</p></div>" +
        "<div><h6>编号：</h6><p>" + obj.sno + "</p></div>" +
        "<div><h6>年份：</h6><p>" + obj.year + "</p></div>" +
        "<div><h6>规格：</h6><p>" + obj.spec + "</p></div>" +
        "<div><h6>井深：</h6><p>" + obj.depth + "</p></div>" +
        "<div><h6>防护：</h6><p>" + obj.protect + "</p></div>" +
        "<div><h6>爬梯：</h6><p>" + obj.ladder + "</p></div>" +
        "<div><h6>寿命：</h6><p>" + obj.age + "</p></div>" +
        "<div><h6>材质：</h6><p>" + obj.quality + "</p></div>" +
        "<div><h6>形状：</h6><p>" + obj.shape + "</p></div>" +
        "<div><h6>责任：</h6><p>" + obj.personLiable + "</p></div>" +
        "<div><h6>部门：</h6><p>" + obj.department + "</p></div>" +
        "<div class='remarks'><h6>备注：</h6><p>" + obj.remarks + "</p></div>" +
        "</div>";
    var coverWindow = new BMap.InfoWindow(coverContent); // 创建信息窗口对象
    //创建井盖小图标
    var pt = new BMap.Point(obj.lng, obj.lat);
    var myIcon = new BMap.Icon("/static/images/map_covers.png", new BMap.Size(22, 22));
    var marker = new BMap.Marker(pt, {icon: myIcon}); // 创建标注
    map.addOverlay(marker);
    marker.addEventListener("click", function () {
        this.openInfoWindow(coverWindow);
        //图片加载完毕重绘infowindow
        document.getElementById('coverDemo').onload = function () {
            coverWindow.redraw(); //防止在网速较慢，图片未加载时，生成的信息框高度比图片的总高度小，导致图片部分被隐藏
        }
    });
}

/**创建涵匣的全部查询 **/
function queryHan() {
    $.ajax({
        type: "GET",
        url: "/rest/web/information/selectGatesList",
        dataType: "json",
        success: function (data) {
            var hanList = data.data;
            $.each(hanList, function (i, o) {
                setHanPoint(o);
            });
        }
    });
}

/**
 * 绘制涵匣点
 * @param obj
 */
function setHanPoint(obj) {
    var hanContent = "";
    hanContent = hanContent + "<div class='senseContent' id='hanDemo'>" +
        "<div><h6>类别：</h6><p>" + obj.type + "</p></div>" +
        "<div><h6>名称：</h6><p>" + obj.name + "</p></div>" +
        "<div><h6>编号：</h6><p>" + obj.sno + "</p></div>" +
        "<div><h6>年份：</h6><p>" + obj.year + "</p></div>" +
        "<div><h6>规格：</h6><p>" + obj.spec + "</p></div>" +
        "<div><h6>寿命：</h6><p>" + obj.age + "</p></div>" +
        "<div><h6>责任：</h6><p>" + obj.personLiable + "</p></div>" +
        "<div><h6>部门：</h6><p>" + obj.department + "</p></div>" +
        "<div class='remarks'><h6>备注：</h6><p>" + obj.remarks + "</p></div>" +
        "</div>";
    var hanWindow = new BMap.InfoWindow(hanContent); // 创建信息窗口对象
    //创建涵匣小图标
    var pt = new BMap.Point(obj.lng, obj.lat);
    var myIcon = new BMap.Icon("/static/images/map_hanza.png", new BMap.Size(22, 22));
    var marker = new BMap.Marker(pt, {icon: myIcon}); // 创建标注
    map.addOverlay(marker);
    marker.addEventListener("click", function () {
        this.openInfoWindow(hanWindow);
        //图片加载完毕重绘infowindow
        document.getElementById('hanDemo').onload = function () {
            hanWindow.redraw(); //防止在网速较慢，图片未加载时，生成的信息框高度比图片的总高度小，导致图片部分被隐藏
        }
    });
}

/*********************************************************管网方法*******************/
/**创建管网的全部查询 **/
function queryPipe() {
    $.ajax({
        type: "GET",
        url: "/rest/web/information/selectPipeList",
        dataType: "json",
        success: function (data) {
            map.clearOverlays();
            var pipeList = data.data;
            $.each(pipeList, function (i, o) {
                setPipePoint(o);
            });
        }
    });
}

/**
 * 绘制管网点
 * @param obj
 */
function setPipePoint(obj) {
    var pipeContent = "";
    pipeContent = pipeContent + "<div class='senseContent'>" +
        "<div><h6>类别：</h6><p>" + obj.type + "</p></div>" +
        "<div><h6>名称：</h6><p>" + obj.name + "</p></div>" +
        "<div><h6>编号：</h6><p>" + obj.sno + "</p></div>" +
        "<div><h6>年份：</h6><p>" + obj.year + "</p></div>" +
        "<div><h6>规格：</h6><p>" + obj.spec + "</p></div>" +
        "<div><h6>埋深：</h6><p>" + obj.depth + "</p></div>" +
        "<div><h6>高程：</h6><p>" + obj.altitude + "</p></div>" +
        "<div><h6>壁厚：</h6><p>" + obj.thickness + "</p></div>" +
        "<div><h6>寿命：</h6><p>" + obj.age + "</p></div>" +
        "<div><h6>材质：</h6><p>" + obj.quality + "</p></div>" +
        "<div><h6>责任：</h6><p>" + obj.personLiable + "</p></div>" +
        "<div><h6>部门：</h6><p>" + obj.department + "</p></div>" +
        "<div class='remarks'><h6>备注：</h6><p>" + obj.remarks + "</p></div>" +
        "</div>";
    var pipeWindow = new BMap.InfoWindow(pipeContent);
    var startPt = new BMap.Point(obj.startLng, obj.startLat);
    var endPt = new BMap.Point(obj.endLng, obj.endLat);
    var polyline = new BMap.Polyline([
        startPt,
        endPt
    ], {strokeColor: "blue", strokeWeight: 2});
    map.addOverlay(polyline);
    var myIcon = new BMap.Icon("/static/images/map_pipe.png", new BMap.Size(22, 22));
    var marker = new BMap.Marker(endPt, {icon: myIcon}); // 创建标注
    map.centerAndZoom(endPt, 15);
    map.addOverlay(marker);
    marker.addEventListener("click", function () {
        this.openInfoWindow(pipeWindow);
    });
}

/*********************************************************危险方法*******************/
/**查询全部危险**/
function queryDanger() {
    var status = 0;
    $.ajax({
        type: "GET",
        url: "/rest/web/index/selectDangerList ",
        dataType: "json",
        data: {
            status: status
        },
        success: function (data) {
            map.clearOverlays();
            var dangerList = data.data;
            $.each(dangerList, function (i, o) {
                setDangerPoint(o);
            });
        }
    });
}

/**
 * 绘制点危险
 * @param obj
 */
function setDangerPoint(obj) {
    var dangerContent = "";
    dangerContent = dangerContent + "<div class='second-danger' id=''dangerDemo'>" +
        "<div><h6>危险主题：</h6><p>" + obj.title + "</p></div>" +
        "<div><h6>危险地点：</h6><p>" + obj.address + "</p></div>" +
        "<div><h6>更新时间：</h6><p>" + obj.createTime + "</p></div>" +
        "<div class='remarks'><h6>最新进度：</h6><p>" + obj.content + "</p></div>" +
        "</div>";
    var opts = {
        width: 280,     // 信息窗口宽度
        height: 130,     // 信息窗口高度
    }
    var dangerWindow = new BMap.InfoWindow(dangerContent, opts); // 创建信息窗口对象
    //创建危险小图标
    var pt = new BMap.Point(obj.lng, obj.lat);
    var myIcon = new BMap.Icon("/static/images/map_danger.png", new BMap.Size(22, 22));
    var marker = new BMap.Marker(pt, {icon: myIcon}); // 创建标注
    map.addOverlay(marker);
    marker.addEventListener("click", function () {
        this.openInfoWindow(dangerWindow);
        //图片加载完毕重绘infowindow
        document.getElementById('dangerDemo').onload = function () {
            dangerWindow.redraw(); //防止在网速较慢，图片未加载时，生成的信息框高度比图片的总高度小，导致图片部分被隐藏
        }
    });
}

/*********************************************************人员方法*******************/
/**查询全部人员**/
function queryPeople() {
    $.ajax({
        type: "GET",
        url: "/rest/web/locationLog/selectAll ",
        dataType: "json",
        data: {
            createTime: new Date(new Date()).Format("yyyy-MM-dd"),
        },
        success: function (data) {
            map.clearOverlays();
            var peopleList = data.data;
            $.each(peopleList, function (i, o) {
                if(o.createTime != null){
                    setPeoplePoint(o);
                }

            });
        }
    });
}

/**
 * 绘制点人员
 * @param obj
 */
function setPeoplePoint(obj) {
    var peopleContent = "";
    var status = (obj.createTime == null ? "离线" : "在线");
    peopleContent = peopleContent + "<div class='peopleContent  senseContent ' id='peopleDemo'>" +
        "<div><h5>" + obj.nickName + "&nbsp; </h5><p>" + obj.department + " &nbsp; &nbsp;  状态：<span>" + status + "</span></p></div>" +
        "<div><h6>当前位置：</h6><p>" + obj.address + "</p></div>" +
        "<div><h6>最近在线：</h6><p>" + obj.createTime + "</p></div>" +
        "<div><h6>手机号码：</h6><p>" + obj.phone + "</p></div>" +
        "<div><h6>坐标：</h6><p class='coordinate'>" + obj.lng + "," + obj.lat + "</p></div>" +
        "</div>";
    var opts = {
        width: 280,     // 信息窗口宽度
        height: 120,     // 信息窗口高度
    }
    var peopleWindow = new BMap.InfoWindow(peopleContent, opts); // 创建信息窗口对象
    //创建人员小图标
    var pt = new BMap.Point(obj.lng, obj.lat);
    var myIcon = new BMap.Icon("/static/images/map_people.png", new BMap.Size(22, 22));
    var marker = new BMap.Marker(pt, {icon: myIcon}); // 创建标注
    map.addOverlay(marker);
    marker.addEventListener("click", function () {
        this.openInfoWindow(peopleWindow);
        //图片加载完毕重绘infowindow
        document.getElementById('peopleDemo').onload = function () {
            peopleWindow.redraw(); //防止在网速较慢，图片未加载时，生成的信息框高度比图片的总高度小，导致图片部分被隐藏
        }
    });
}


/*********************************************************事件方法*******************/
/**查询全部事件**/
function queryEvent() {
    var status =0;
    $.ajax({
        type: "GET",
        url: "/rest/web/index/selectEventInfoList",
        dataType: "json",
        data: {
            status: status
        },
        success: function (data) {
            map.clearOverlays();
            var eventList = data.data;
            $.each(eventList, function (i, o) {
                setEventPoint(o);
            });
        }
    });
}

/**
 * 绘制点事件
 * @param obj
 */
function setEventPoint(obj) {
    var eventContent = "";
    eventContent = eventContent + "<div class='second-danger' id='eventDemo'>" +
        "<div><h6>上报人：</h6><p>" + obj.nickName + "</p></div>" +
        "<div><h6>上报时间：</h6><p>" + obj.createTime + "</p></div>" +
        "<div><h6>事件主题：</h6><p>" + obj.title + "</p></div>" +
        "<div><h6>事件地点：</h6><p>" + obj.address + "</p></div>" +
        "</div>";
    var opts = {
        width: 280,     // 信息窗口宽度
        height: 90,     // 信息窗口高度
    }
    var eventWindow = new BMap.InfoWindow(eventContent, opts); // 创建信息窗口对象

    //创建事件小图标
    var pt = new BMap.Point(obj.lng, obj.lat);
    var myIcon = new BMap.Icon("/static/images/map_event.png", new BMap.Size(22, 22));
    var marker = new BMap.Marker(pt, {icon: myIcon}); // 创建标注
    map.addOverlay(marker);
    marker.addEventListener("click", function () {
        this.openInfoWindow(eventWindow);
        //图片加载完毕重绘infowindow
        document.getElementById('eventDemo').onload = function () {
            eventWindow.redraw(); //防止在网速较慢，图片未加载时，生成的信息框高度比图片的总高度小，导致图片部分被隐藏
        }
    });
}
