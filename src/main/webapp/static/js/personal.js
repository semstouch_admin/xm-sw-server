/**
 * Created by Administrator on 2017/5/17.
 */
/***查询用户个人信息的方法**/
function queryPersonal() {
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "/rest/web/sysuser/selectPersonal",
        success: function (data) {
            var personalList = data.data;
            //个人信息部分
            var personalHtml = "";
            personalHtml += "<div><input name='id' type='hidden' value='" + personalList.id + "'/></div><div class='message-form'><h4>个人信息</h4><div><div><label><h5>姓名：</h5></label> <p>" + personalList.nickName + "</p> </div> <div> <label><h5>帐号：</h5></label> <p>" + personalList.loginName + "</p> </div> <div> <label><h5>手机号：</h5></label> <p>" + personalList.phone + "</p> </div> " +
                "<div><label><h5>所属部门：</h5></label> <p>" + personalList.department + "</p> </div></div></div>" +
                " <div class='password-form'><h4>修改密码</h4><ul class='xform-ul'></li><li> <div class='form-label'><label>原密码：</label></div> <div class='form-input'> <input type='password' id='oldpassword' name='pwd' value=''></div></li> <li> <div class='form-label'><label>新密码：</label></div> <div class='form-input'> <input type='password' id='password1' name='pwd1' placeholder='6-18位数字和字母组合' /><div id='ts'></div></div> </li> " +
                "<li> <div class='form-label'><label>确认密码：</label></div> <div class='form-input'> <input type='password' id='password2' name='pwd2' placeholder='与新密码相同' /> </div> </li>" +
                "<li class='text-center'> <div class='btn-submit editBtn' data-id='"
                + personalList.id + "'> <div id='btn'>确认</div> </div> </li> <li class='text-center'> <div id='tip4'></div> </li> </ul></div>";
            $(".shuiwu-personal").html(personalHtml);
            //编辑密码部分
            $(".editBtn").click(function () {
                editPersonal($(this).attr("data-id"));
            })
        }
    })
}

function editPersonal(id) {
    if ($("#password1").val() != $("#password2").val()) {
        alert("您输入的新密码与确认密码确认不一致");
        return;
    }
    if ($("#password1").val() ==""&& $("#password2").val()==""&&$("#oldpassword").val()=="") {
        alert("密码不为空");
        return;
    }
    $.ajax({
        type: "POST",
        dataType: "json",
        url: '/rest/web/sysuser/updateSysuser ',
        data: {
            id: id,
            oldPwd: $("#oldpassword").val(),
            pwd: $("#password1").val()
        },
        success: function (data) {
            if(data.success){
                alert("密码修改成功");
                window.location.href = "rest/web/toLoginOut";
            }
            else
            alert(data.msg);
        }
    });
}
queryPersonal();


