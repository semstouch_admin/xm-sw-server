/**
 * Created by Administrator on 2017/5/10.
 */
var map = new BMap.Map("map");  // 创建Map实例
var point = new BMap.Point(118.10388605, 24.48923061);// 初始化地图,用城市名设置地图中心点
map.centerAndZoom(point, 15);//
map.enableScrollWheelZoom();//地图可以缩放

//声明八个对象用来缓存marker和Window，使用id来查询和添加缓存
var senseMap = null;
var senseWindowMap = null;
var buildMap = null;
var buildWindowMap = null;
var coverMap = null;
var coverWindowMap = null;
var hanMap = null;
var hanWindowMap = null;
/**
 * 自定义map
 * @constructor
 */
function Map() {

    var mapObj = {};

    this.put = function (key, value) {
        mapObj[key] = value;
    };

    this.remove = function (key) {
        if (mapObj.hasOwnProperty(key)) {
            delete mapObj[key];
        }
    };

    this.get = function (key) {
        if (mapObj.hasOwnProperty(key)) {
            return mapObj[key];
        }
        return null;
    };

    this.getKeys = function () {
        var keys = [];
        for (var k in mapObj) {
            keys.push(k);
        }
        return keys;
    };

    // 遍历map
    this.each = function (fn) {
        for (var key in mapObj) {
            fn(key, mapObj[key]);
        }
    };

    this.toString = function () {
        var str = "{";
        for (var k in mapObj) {
            str += "\"" + k + "\" : \"" + mapObj[k] + "\",";
        }
        str = str.substring(0, str.length - 1);
        str += "}";
        return str;
    }

}

/**查询地图上全部传感**/
function querySense() {
    $.ajax({
        type: "GET",
        url: "/rest/web/index/selectSenseList",
        dataType: "json",
        data:{
             name:$("#name").val()
        },
        success: function (data) {
            //每次查询后都把marker和senseWindow缓存的数据清空掉
            senseMap = new Map();
            senseWindowMap = new Map();

            var senseList = data.msg;
            $.each(senseList, function (i, o) {
                setSensePoint(o);
            });
            var reportSenseHtml = "";
            $.each(senseList, function (i, n) {
                if (i == 0) {
                    getSingleSense(n.id);
                    reportSenseHtml = reportSenseHtml + "<li data-id='" + n.id + "'> <p>" + n.sno + "</p><p>" + n.name + "</p></li>";
                }
                else {
                    reportSenseHtml = reportSenseHtml + "<li data-id='" + n.id + "'> <p>" + n.sno + "</p><p>" + n.name + "</p></li>";
                }
            });
            $("#reportSense").html(reportSenseHtml);
            $("#reportSense>li").click(function () {
                getSingleSense($(this).attr("data-id"));
            });
            //求出总数的方法
            var data = data.msg;
            var dealWith = "";
            dealWith += "<p>总数：<span>" + data.length + "</span></p>"
            $("#senseTotal").html(dealWith);
        }
    });
}
/**
 * 绘制传感点
 * @param obj
 */
function setSensePoint(obj) {
    var senseContent = "";
    senseContent = senseContent + "<div class='facilities' id='senseDemo'>" +
        "<div class='message'><h5>" + obj.name + "&nbsp; </h5><p> &nbsp; &nbsp; 状态：<span>使用中</span></p></div>" +
        "<div><h6>坐标：</h6><p class='coordinate'>" + obj.lng + "," + obj.lat + "</p></div>" +
        "<div><h6>地址：</h6><p>" + obj.address + "</p></div>" +
        "<div class='classify'>" +
        "<div><h6>类别：</h6><p>" + obj.type + "</p></div><div><h6>规格：</h6><p>" + obj.spec + "</p></div>" +
        "</div>" +
        "<div class='classify'>" +
        "<div><h6>编号：</h6><p>" + obj.sno + "</p></div><div><h6>材质：</h6><p>" + obj.quality + "</p></div>" +
        "</div>" +
        "<div class='classify'>" +
        "<div><h6>录入：</h6><p>" + obj.createTime + "</p></div><div><h6>寿命：</h6><p>" + obj.age + "</p></div>" +
        "</div>" +
        "<div class='classify'>" +
        "<div><h6>年份：</h6><p>" + obj.year + "</p></div><div><h6>责任：</h6><p>" + obj.personLiable + "</p></div>" +
        "</div>" +
        "<div class='classify'>" +
        "<div><h6>部门：</h6><p>" + obj.department + "</p></div>" +
        "</div>" +
        "<div class='classify remarks'><h6>备注：</h6><p>" + obj.remarks + "</p></div>" +
        "</div>" +
        "</div>";

    var senseWindow = new BMap.InfoWindow(senseContent); // 创建信息窗口对象
    //创建传感小图标
    var pt = new BMap.Point(obj.lng, obj.lat);
    var myIcon = new BMap.Icon("/static/images/map_sensor.png", new BMap.Size(22, 22));
    var marker = new BMap.Marker(pt, {icon: myIcon}); // 创建标注
    map.addOverlay(marker);
    marker.addEventListener("click", function () {
        this.openInfoWindow(senseWindow);
        //图片加载完毕重绘infowindow
        document.getElementById('senseDemo').onload = function () {
            senseWindow.redraw(); //防止在网速较慢，图片未加载时，生成的信息框高度比图片的总高度小，导致图片部分被隐藏
        }
    });
    //将marker和senseWindow缓存起来
    senseMap.put(obj.id, marker);
    senseWindowMap.put(obj.id, senseWindow);
}
/**
 * 根据id把缓存的marker和senseWindow拼装后调用map的openInfoWindow方法弹窗展示
 * @param id
 */
function getSingleSense(id) {
    var infoWindow = senseWindowMap.get(id);
    var marker = senseMap.get(id);
    marker.openInfoWindow(infoWindow);
}

querySense();


/**查询全部地图上建筑**/
function queryBuild() {
    $.ajax({
        type: "GET",
        url: "/rest/web/index/selectArchitectureList",
        dataType: "json",
        data:{
            name: $("#name").val()
        },
        success: function (data) {
            //每次查询后都把marker和buildWindow缓存的数据清空掉
            buildMap = new Map();
            buildWindowMap = new Map();
            var buildList = data.data;
            $.each(buildList, function (i, o) {
                setBuildPoint(o);
            });
            var reportBuildHtml = "";
            $.each(buildList, function (i, n) {
                if (i == 0) {
                    getSingleBuild(n.id);
                    reportBuildHtml = reportBuildHtml + "<li data-id='" + n.id + "'> <p>" + n.sno + "</p><p>" + n.name + "</p></li>";
                }
                else {
                    reportBuildHtml = reportBuildHtml + "<li data-id='" + n.id + "'> <p>" + n.sno + "</p><p>" + n.name + "</p></li>";
                }
            });
            $("#reportBuild").html(reportBuildHtml);
            $("#reportBuild>li").click(function () {
                getSingleBuild($(this).attr("data-id"));
            });
            //求出总数的方法
            var data = data.data;
            var dealWith = "";
            dealWith += "<p>总数：<span>" + data.length + "</span></p>"
            $("#buildTotal").html(dealWith);
        }
    });
}
function setBuildPoint(obj) {
    var buildContent = "";
    buildContent = buildContent + "<div class='facilities' id='buildDemo'>" +
        "<div  class='message'><h5>" + obj.name + "&nbsp; </h5><p> &nbsp; &nbsp; 状态：<span>使用中</span></p></div>" +
        "<div><h6>坐标：</h6><p class='coordinate'>" + obj.lng + "," + obj.lat + "</p></div>" +
        "<div><h6>地址：</h6><p>" + obj.address + "</p></div>" +
        "<div class='classify'>" +
        "<div><h6>类别：</h6><p>" + obj.type + "</p></div><div><h6>规格：</h6><p>" + obj.spec + "</p></div>" +
        "</div>" +
        "<div class='classify'>" +
        "<div><h6>编号：</h6><p>" + obj.sno + "</p></div><div><h6>寿命：</h6><p>" + obj.age + "</p></div>" +
        "</div>" +
        "<div class='classify'>" +
        "<div><h6>录入：</h6><p>" + obj.createTime + "</p></div><div><h6>责任：</h6><p>" + obj.personLiable + "</p></div>" +
        "</div>" +
        "<div class='classify'>" +
        "<div><h6>年份：</h6><p>" + obj.year + "</p></div><div><h6>部门：</h6><p>" + obj.department + "</p></div>" +
        "</div>" +
        "<div class='classify remarks'><h6>备注：</h6><p>" + obj.remarks + "</p></div>" +
        "</div>" +
        "</div>";
    var buildWindow = new BMap.InfoWindow(buildContent); // 创建信息窗口对象
    //创建建筑小图标
    var pt = new BMap.Point(obj.lng, obj.lat);
    var myIcon = new BMap.Icon("/static/images/map_building.png", new BMap.Size(22, 22));
    var marker = new BMap.Marker(pt, {icon: myIcon}); // 创建标注
    map.addOverlay(marker);
    marker.addEventListener("click", function () {
        this.openInfoWindow(buildWindow);
        //图片加载完毕重绘infowindow
        document.getElementById('buildDemo').onload = function () {
            buildWindow.redraw(); //防止在网速较慢，图片未加载时，生成的信息框高度比图片的总高度小，导致图片部分被隐藏
        }
    });
    //将marker和buildWindow缓存起来
    buildMap.put(obj.id, marker);
    buildWindowMap.put(obj.id, buildWindow);
}
/**
 * 根据id把缓存的marker和buildWindow拼装后调用map的openInfoWindow方法弹窗展示
 * @param id
 */
function getSingleBuild(id) {
    var infoWindow = buildWindowMap.get(id);
    var marker = buildMap.get(id);
    marker.openInfoWindow(infoWindow);
}
queryBuild();


/**创建涵匣的全部查询 **/
function queryHan() {
    $.ajax({
        type: "GET",
        url: "/rest/web/information/selectGatesList",
        dataType: "json",
        data:{
            name: $("#name").val()
        },
        success: function (data) {
            //每次查询后都把marker和hanWindow缓存的数据清空掉
            hanMap = new Map();
            hanWindowMap = new Map();
            var hanList = data.data;
            $.each(hanList, function (i, o) {
                setHanPoint(o);
            });
            var reportHanHtml = "";
            $.each(hanList, function (i, n) {
                if (i == 0) {
                    getSingleHan(n.id);
                    reportHanHtml = reportHanHtml + "<li data-id='" + n.id + "'> <p>" + n.sno + "</p><p>" + n.name + "</p></li>";
                }
                else {
                    reportHanHtml = reportHanHtml + "<li data-id='" + n.id + "'> <p>" + n.sno + "</p><p>" + n.name + "</p></li>";
                }
            });
            $("#reportHan").html(reportHanHtml);
            $("#reportHan>li").click(function () {
                getSingleHan($(this).attr("data-id"));
            });
            //求出总数的方法
            var data = data.data;
            var dealWith = "";
            dealWith += "<p>总数：<span>" + data.length + "</span></p>"
            $("#boxTotal").html(dealWith);
        }
    });
}
/**
 * 绘制涵匣点
 * @param obj
 */
function setHanPoint(obj) {
    var hanContent = "";
    hanContent = hanContent + "<div class='facilities' id='senseDemo'>" +
        "<div class='message'><h5>" + obj.name + "&nbsp; </h5><p> &nbsp; &nbsp; 状态：<span>使用中</span></p></div>" +
        "<div><h6>坐标：</h6><p class='coordinate'>" + obj.lng + "," + obj.lat + "</p></div>" +
        "<div><h6>地址：</h6><p>" + obj.address+ "</p></div>" +
        "<div class='classify'>" +
        "<div><h6>类别：</h6><p>" + obj.type + "</p></div><div><h6>规格：</h6><p>" + obj.spec + "</p></div>" +
        "</div>" +
        "<div class='classify'>" +
        "<div><h6>编号：</h6><p>" + obj.sno + "</p></div><div><h6>寿命：</h6><p>" + obj.age + "</p></div>" +
        "</div>" +
        "<div class='classify'>" +
        "<div><h6>录入：</h6><p>" + obj.createTime + "</p></div><div><h6>责任：</h6><p>" + obj.personLiable + "</p></div>" +
        "</div>" +
        "<div class='classify'>" +
        "<div><h6>年份：</h6><p>" + obj.year + "</p></div><div><h6>部门：</h6><p>" + obj.department + "</p></div>" +
        "</div>" +
        "<div class='classify remarks'><h6>备注：</h6><p>" + obj.remarks + "</p></div>" +
        "</div>" +
        "</div>";
    var hanWindow = new BMap.InfoWindow(hanContent); // 创建信息窗口对象
    //创建涵匣小图标
    var pt = new BMap.Point(obj.lng, obj.lat);
    var myIcon = new BMap.Icon("/static/images/map_hanza.png", new BMap.Size(22, 22));
    var marker = new BMap.Marker(pt, {icon: myIcon}); // 创建标注
    map.addOverlay(marker);
    marker.addEventListener("click", function () {
        this.openInfoWindow(hanWindow);
        //图片加载完毕重绘infowindow
        document.getElementById('hanDemo').onload = function () {
            hanWindow.redraw(); //防止在网速较慢，图片未加载时，生成的信息框高度比图片的总高度小，导致图片部分被隐藏
        }
    });
    //将marker和hanWindow缓存起来
    hanMap.put(obj.id, marker);
    hanWindowMap.put(obj.id, hanWindow);
}
/**
 * 根据id把缓存的marker和hanWindow拼装后调用map的openInfoWindow方法弹窗展示
 * @param id
 */
function getSingleHan(id) {
    var infoWindow = hanWindowMap.get(id);
    var marker = hanMap.get(id);
    marker.openInfoWindow(infoWindow);
}
queryHan();

/**查询地图上全部井盖**/
function queryCover() {
    $.ajax({
        type: "GET",
        url: "/rest/web/index/selectManholeList ",
        dataType: "json",
        data:{
                    name: $("#name").val()
                },
        success: function (data) {
            //每次查询后都把marker和coverWindow缓存的数据清空掉
            coverMap = new Map();
            coverWindowMap = new Map();
            var coverList = data.data;
            $.each(coverList, function (i, o) {
                setCoverPoint(o);
            });
            var reportCoverHtml = "";
            $.each(coverList, function (i, n) {
                if (i == 0) {
                    getSingleCover(n.id);
                    reportCoverHtml = reportCoverHtml + "<li data-id='" + n.id + "'> <p>" + n.sno + "</p><p>" + n.name + "</p></li>";
                }
                else {
                    reportCoverHtml = reportCoverHtml + "<li data-id='" + n.id + "'> <p>" + n.sno + "</p><p>" + n.name + "</p></li>";
                }
            });
            $("#reportCover").html(reportCoverHtml);
            $("#reportCover>li").click(function () {
                getSingleCover($(this).attr("data-id"));
            });
            //求出总数的方法
            var data = data.data;
            var dealWith = "";
            dealWith += "<p>总数：<span>" + data.length + "</span></p>";
            $("#coverTotal").html(dealWith);
        }
    });
}
/**
 * 绘制井盖的点
 * @param obj
 */
function setCoverPoint(obj) {
    var coverContent = "";
    coverContent = coverContent + "<div class='facilities' id='senseDemo'>" +
        "<div class='message'><h5>" + obj.name + "&nbsp; </h5><p> &nbsp; &nbsp; 状态：<span>使用中</span></p></div>" +
        "<div><h6>坐标：</h6><p class='coordinate'>" + obj.lng + "," + obj.lat + "</p></div>" +
        "<div><h6>地址：</h6><p>" + obj.address + "</p></div>" +
        "<div class='classify'>" +
        "<div><h6>类别：</h6><p>" + obj.type + "</p></div><div><h6>井深：</h6><p>" + obj.depth + "</p></div>" +
        "</div>" +
        "<div class='classify'>" +
        "<div><h6>编号：</h6><p>" + obj.sno + "</p></div><div><h6>规格：</h6><p>" + obj.spec + "</p></div>" +
        "</div>" +
        "<div class='classify'>" +
        "<div><h6>录入：</h6><p>" + obj.createTime + "</p></div><div><h6>材质：</h6><p>" + obj.quality + "</p></div>" +
        "</div>" +
        "<div class='classify'>" +
        "<div><h6>年份：</h6><p>" + obj.year + "</p></div><div><h6>寿命：</h6><p>" + obj.age + "</p></div>" +
        "</div>" +
        "<div class='classify'>" +
        "<div><h6>爬梯：</h6><p>" + obj.ladder + "</p></div><div><h6>责任：</h6><p>" + obj.personLiable + "</p></div>" +
        "</div>" +
        "<div class='classify'>" +
        "<div><h6>形状：</h6><p>" + obj.shape + "</p></div><div><h6>部门：</h6><p>" + obj.department + "</p></div>" +
        "</div>" +
        "<div class='classify'><h6>防护：</h6><p>" + obj.protect + "</p></div>" +
        "<div class='classify remarks'><h6>备注：</h6><p>" + obj.remarks + "</p></div>" +
        "</div>" +
        "</div>";
    var coverWindow = new BMap.InfoWindow(coverContent); // 创建信息窗口对象
    //创建井盖小图标
    var pt = new BMap.Point(obj.lng, obj.lat);
    var myIcon = new BMap.Icon("/static/images/map_covers.png", new BMap.Size(22, 22));
    var marker = new BMap.Marker(pt, {icon: myIcon}); // 创建标注
    map.addOverlay(marker);
    marker.addEventListener("click", function () {
        this.openInfoWindow(coverWindow);
        //图片加载完毕重绘infowindow
        document.getElementById('coverDemo').onload = function () {
            coverWindow.redraw(); //防止在网速较慢，图片未加载时，生成的信息框高度比图片的总高度小，导致图片部分被隐藏
        }
    });
    //将marker和coverWindow缓存起来
    coverMap.put(obj.id, marker);
    coverWindowMap.put(obj.id, coverWindow);
}
/**
 * 根据id把缓存的marker和coverWindow拼装后调用map的openInfoWindow方法弹窗展示
 * @param id
 */
function getSingleCover(id) {
    var infoWindow = coverWindowMap.get(id);
    var marker = coverMap.get(id);
    marker.openInfoWindow(infoWindow);
}
queryCover();
/*********************************************************************搜索框的模糊查询******************************************/
/**搜索框中传感的模糊查询**/
$("#blurBtn").click(function () {
    queryHan();
    querySense();
    queryBuild();
    queryCover();
});

