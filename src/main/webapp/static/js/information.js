/**
 * Created by Administrator on 2217/5/11.
 */
/**在添加设备中的五种类型的转换**/
var deviceType = "";  //全局变量存绘制的设备类型
$(".skuiwu-menu a").click(function () {
    deviceType = $(this).attr("data-type");
    //除了官网其它的全部是绘制点的类型
    if (deviceType == "p") {
        drawingManager.open();
        drawingManager.setDrawingMode(BMAP_DRAWING_POLYLINE);
    } else {
        drawingManager.open();
        drawingManager.setDrawingMode(BMAP_DRAWING_MARKER);
    }
});

var map = new BMap.Map("map");  // 创建Map实例
var point = new BMap.Point(118.10388605, 24.48923061);// 初始化地图,用城市名设置地图中心点
map.centerAndZoom(point, 15);//
map.enableScrollWheelZoom();//地图可以缩放
var overlays = [];
var overlaycomplete = function (e) {
    overlays.push(e.overlay);
    drawingManager.close();
};

//实例化鼠标绘制工具
var styleOptions = {
    strokeColor: "blue",    //边线颜色。
    fillColor: "#0081cc",      //填充颜色。当参数为空时，圆形将没有填充效果。
    strokeWeight: 2,       //边线的宽度，以像素为单位。
    //strokeOpacity: 0.8,	   //边线透明度，取值范围0 - 1。
    fillOpacity: 0.6,      //填充的透明度，取值范围0 - 1。
    strokeStyle: 'solid' //边线的样式，solid或dashed。
}
//实例化鼠标绘制工具
var drawingManager = new BMapLib.DrawingManager(map, {
    isOpen: false, //是否开启绘制模式
    enableDrawingTool: false, //是否显示工具栏
    drawingToolOptions: {
        anchor: BMAP_ANCHOR_TOP_RIGHT, //位置
        offset: new BMap.Size(5, 5), //偏离值
        drawingTypes: [
            BMAP_DRAWING_MARKER,
            BMAP_DRAWING_POLYLINE
        ]
    },
    circleOptions: styleOptions, //圆的样式
    polylineOptions: styleOptions, //线的样式
});

//添加鼠标绘制工具监听事件，用于获取绘制结果
drawingManager.addEventListener('overlaycomplete', overlaycomplete);
//绘制点成功后事件
drawingManager.addEventListener("markercomplete", function (e, overlay) {
    //接下来给绘制点添加自定义的窗口，并且将经度和纬度填充到响应的表单里面
    if (deviceType == 's') {
        addSenseForm(overlay);
    }
    if (deviceType == 'b') {
        addBuildForm(overlay);
    }
    if (deviceType == 'c') {
        addCoverForm(overlay);
    }
    if (deviceType == 'h') {
        addHanForm(overlay);
    }
});

drawingManager.addEventListener("polylinecomplete", function (e, overlay) {
    console.log(overlay.getPath());
    var pointArr = overlay.getPath();
    var length = pointArr.length;
    //接下来给绘制点添加自定义的窗口，并且将经度和维度填充到响应的表单里面
    var sContent = "<div class='edit-pipe'><form>" +
        "<div class='modal-header'>添加管网</h4> </div>" +
        "<div class='modal-body'>" +
        "<div class='input-group'><label><span>起点:</span></label><input class='form-control' type='text' name='startAddress' /></div>" +
        "<div class='classify-input input-group'><label><span>经度:</span></label><input class='form-control' type='text' name='startLng'  value='" + pointArr[0].lng + "' ><label><span>纬度:</span></label><input class='form-control' type='text' name='startLat' value='" + pointArr[0].lat + "'/></div>" +
        "<div class='input-group'><label><span>终点:</span></label><input class='form-control' type='text' name='endAddress' /></div>" +
        "<div class='classify-input input-group'><label><span>经度:</span></label><input class='form-control' type='text' name='endLng'  value='" + pointArr[length - 1].lng + "' ><label><span>纬度:</span></label><input class='form-control' type='text' name='endLat'value='" + pointArr[length - 1].lat + "'/></div>" +
        "<div class='three-input input-group'><label><span>编号:</span></label><input class='form-control' type='text' name='sno' ><label><span>埋深:</span></label><input class='form-control' type='text' name='depth'/><label><span>规格:</span></label><input class='form-control' type='text' name='spec'/></div>" +
        "<div class='three-input input-group'><label><span>类别:</span></label><input class='form-control' type='text' name='type'><label><span>高程:</span></label><input class='form-control' type='text' name='altitude'/><label><span>寿命:</span></label><input class='form-control' type='text' name='age'/> </div>" +
        "<div class='three-input input-group'><label><span>名称:</span></label><input class='form-control' type='text' name='name'><label><span>壁厚:</span></label><input class='form-control' type='text' name='thickness'/> <label><span>责任:</span></label><input class='form-control' type='text' name='personLiable'/></div>" +
        "<div class='three-input input-group'><label><span>年份:</span></label><input class='form-control' type='text'  name='year' ><label><span>材质:</span></label><input class='form-control' type='text' name='quality'/> <label><span>部门:</span></label><input class='form-control' type='text' name='department'/></div>" +
        "<div class='remark-input input-group'><label><span>备注:</span></label><textarea class='form-control' type='text' name='remarks'/></textarea></div>" +
        "</div>" +
        " <div class='modal-footer'><a href='javascript:;' class='pipeBtn  btn ensure-btn' >确定</a><a  class='btn btn-default cancel-modal' data-dismiss='modal'>取消</a>" +
        "</div>" +
        "</form></div>";
    var point = new BMap.Point(pointArr[length - 1].lng, pointArr[length - 1].lat);
    map.centerAndZoom(point, 15);
    var myIcon = new BMap.Icon("/static/images/map_pipe.png", new BMap.Size(22, 22));
    var marker = new BMap.Marker(point, {icon: myIcon}); // 创建标注
    map.addOverlay(marker);
    var infoWindow = new BMap.InfoWindow(sContent);
    marker.openInfoWindow(infoWindow);
    marker.addEventListener("click", function () {
        this.openInfoWindow(infoWindow);
    });

    //弹出窗口被关闭掉触发删除新建点
    infoWindow.addEventListener("close", function () {
        map.removeOverlay(overlay);
    });
    //点击取消掉触发删除新建点
    $(".cancel-modal").click(function () {
        map.removeOverlay(overlay);
    });

    /**创建添加管网 **/
        //绑定事件必须的在最后否则无效
    $(".pipeBtn").click(function () {
        //获取最近的form表单的全部字段（记得给每个input等加name字段，否则获取不到表单的值）
        var param = $(this).closest("form").serialize();
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "/rest/web/information/insertPipe ",
            data: param,
            success: function () {
                window.location.href = "rest/web/information/toInformationList ";
            }
        })
    });
});


/************************************************************传感的方法************************************************************************/
/**创建传感的全部查询 **/
function querySense() {
    $.ajax({
        type: "GET",
        url: "/rest/web/information/selectSenseList",
        dataType: "json",
        success: function (data) {
            var senseList = data.data;
            $.each(senseList, function (i, o) {
                setSensePoint(o);
            });
            //删除传感
            $(".deleteBtn").click(function () {
                delSense($(this).attr("data-id"));
            });
            //编辑传感
            $(".editBtn").click(function () {
                editSense($(this).attr("data-id"));
            })
        }
    });
}
/**
 * 添加传感器点的表单
 * @param overlay
 */
function addSenseForm(overlay) {
    var sContent = "<div class='edit-sense'><form>" +
        "<div class='modal-header'>新增传感</h4> </div>" +
        "<div class='modal-body'>" +
        "<div class='input-group'><label'><span>地址:</span></label><input class='form-control' type='text' name='address'/></div>" +
        "<div class='classify-input input-group'><label><span>经度:</span></label><input class='form-control' type='text' value='" + overlay.point.lng + "' name='lng'/><label><span>纬度:</span>" +
        "</label><input class='form-control' type='text' value='" + overlay.point.lat + "'  name='lat' /></div>" +
        "<div class='classify-input input-group'><label><span>编号:</span></label><input class='form-control' type='text' name='sno'/><label><span>材质:</span></label><input class='form-control' type='text' name='quality' /></div>" +
        "<div class='classify-input input-group'><label><span>类别:</span></label><input class='form-control' type='text' name='type'/><label><span>寿命:</span></label><input class='form-control' type='text' name='age' /></div>" +
        "<div class='classify-input input-group'><label><span>名称:</span></label><input class='form-control' type='text' name='name'/><label><span>责任:</span></label><input class='form-control' type='text' name='personLiable'/></div>" +
        "<div class='classify-input input-group'><label><span>年份:</span></label><input class='form-control' type='text'  name='year' /><label><span>部门:</span></label><input class='form-control' type='text' name='department'/></div>" +
        "<div class='classify-input input-group'><label><span>规格:</span></label><input class='form-control' type='text'  name='spec' /></div>" +
        "<div class='remark-input input-group'><label><span>备注:</span></label><textarea class='form-control' type='text'  name='remarks' ></textarea></div>" +
        "</div>" +
        " <div class='modal-footer'><a href='javascript:;' class='senseBtn  btn ensure-btn' >确定</a><a  class='btn btn-default cancel-modal' data-dismiss='modal'>取消</a>" +
        "</div>" +
        "</form></div>";
    var infoWindow = new BMap.InfoWindow(sContent);
    overlay.openInfoWindow(infoWindow);
    overlay.addEventListener("click", function () {
        this.openInfoWindow(infoWindow);
    });

    //弹出窗口被关闭掉触发删除新建点
    infoWindow.addEventListener("close", function () {
        map.removeOverlay(overlay);
    });
    //点击取消掉触发删除新建点
    $(".cancel-modal").click(function () {
        map.removeOverlay(overlay);
    });

    //绑定事件必须的在最后否则无效
    $(".senseBtn").click(function () {
        //获取最近的form表单的全部字段（记得给每个input等加name字段，否则获取不到表单的值）
        var param = $(this).closest("form").serialize();
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "/rest/web/information/insertSense",
            data: param,
            success: function () {
                window.location.href = "rest/web/information/toInformationList ";
            }
        })
    });
}
/**
 * 绘制传感点
 * @param obj
 */
function setSensePoint(obj) {
    var senseContent = "";
    senseContent = senseContent + "<div class='edit-sense' id='senseDemo'><form id='edit-form-" + obj.id + "'>" +
        "<div class='modal-header'>编辑传感</h4> </div>" +
        "<div class='modal-body'>" +
        "<div class='input-group'><input name='id' type='hidden' value='" + obj.id + "'/></div>" +
        "<div class='input-group'><label><span>地址:</span></label><input class='form-control' type='text' name='address'  value='" + obj.address + "' /></div>" +
        "<div class='classify-input input-group'><label><span>经度:</span></label><input class='form-control' type='text' value='" + obj.lng + "' name='lng'/><label><span>纬度:</span>" +
        "</label><input class='form-control' type='text' value='" + obj.lat + "'  name='lat'/></div>" +
        "<div class='classify-input input-group'><label><span>编号:</span></label><input class='form-control' type='text' name='sno' value='" + obj.sno + "'><label><span>材质:</span></label><input class='form-control' type='text' name='quality' value='" + obj.quality + "'></div>" +
        "<div class='classify-input input-group'><label><span>类别:</span></label><input class='form-control' type='text' name='type' value='" + obj.type + "'><label><span>寿命:</span></label><input class='form-control' type='text' name='age' value='" + obj.age + "'></div>" +
        "<div class='classify-input input-group'><label><span>名称:</span></label><input class='form-control' type='text' name='name' value='" + obj.name + "'><label><span>责任:</span></label><input class='form-control' type='text' name='personLiable' value='" + obj.personLiable + "'></div>" +
        "<div class='classify-input input-group'><label><span>年份:</span></label><input class='form-control' type='text'  name='year' value='" + obj.year + "'><label><span>部门:</span></label><input class='form-control' type='text' name='department' value='" + obj.department + "'></div>" +
        "<div class='classify-input input-group'><label><span>规格:</span></label><input class='form-control' type='text' name='spec' value='" + obj.spec + "'></div>" +
        "<div class='remark-input input-group'><label><span>备注:</span></label><textarea class='form-control' type='text' name='remarks'  value='" + obj.remarks + "'></textarea></div>" +
        "</div>" +
        " <div class='modal-footer'><a href='javascript: editSense(" + obj.id + ");' class='editBtn  btn ensure-btn'  data-id='"
        + obj.id + "'>确定</a><a  href='javascript:confirmSense(" + obj.id + ");' class='deleteBtn  btn btn-default' data-dismiss='modal' data-id='"
        + obj.id + "'>删除</a>" +
        "</div></form></div>";

    var senseWindow = new BMap.InfoWindow(senseContent); // 创建信息窗口对象
    //创建传感小图标
    var pt = new BMap.Point(obj.lng, obj.lat);
    var myIcon = new BMap.Icon("/static/images/map_sensor.png", new BMap.Size(22, 22));
    var marker = new BMap.Marker(pt, {icon: myIcon}); // 创建标注
    map.addOverlay(marker);
    marker.addEventListener("click", function () {
        this.openInfoWindow(senseWindow);
        //图片加载完毕重绘infowindow
        document.getElementById('senseDemo').onload = function () {
            senseWindow.redraw(); //防止在网速较慢，图片未加载时，生成的信息框高度比图片的总高度小，导致图片部分被隐藏
        }
    });

}
/**编辑传感**/
function editSense(id) {
    var param = $("#edit-form-" + id).serialize();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/rest/web/information/updateSense",
        data: param,
        success: function (data) {
            window.location.href = "rest/web/information/toInformationList ";
        }
    });
}
/**删除涵匣提示框**/
function confirmSense(id) {
    if ($("#myConfirm").length > 0) {
        $("#myConfirm").remove();
    }
    var html='<div class="modal fade" id="myConfirm" tabindex="-1" role="dialog" aria-labelledby="prompt">'+
            '<div class="modal-dialog" role="document"> <div class="modal-content" style="width:320px;">'+
            '<div class="modal-header" > <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span>'+
            '</button> <h4 class="modal-title" id="prompt" style="font-weight: bold;font-size: 14px;">提示信息</h4> </div> <div class="modal-body"> <div> <input id="dataId" type="hidden" value=""/> </div>'+
            '<p>您确认要删除吗？</p> </div> <div class="modal-footer"> <button type="button" class="confirmOk btn ensure-btn">确定</button> '+
            '<button type="button" class="btn btn-default" data-dismiss="modal">取消</button> </div> </div> </div> </div>';
    $("body").append(html);

    $("#myConfirm").modal("show");
    $(".confirmOk").on("click", function() {
        delSense(id);
    });
}

/**删除传感**/
function delSense(id) {
    var param = "id=" + id;
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/rest/web/information/deleteSense",
        data: param,
        success: function () {
            window.location.href = "rest/web/information/toInformationList ";
        }
    });
}
querySense();

/************************************************************建筑的方法************************************************************************/
/**创建建筑的全部查询 **/
function queryBuild() {
    $.ajax({
        type: "GET",
        url: "/rest/web/information/selectArchitectureList",
        dataType: "json",
        success: function (data) {
            var buildList = data.data;
            $.each(buildList, function (i, o) {
                setBuildPoint(o);
            });
            //删除建筑
            $(".deleteBtn").click(function () {
                delBuild($(this).attr("data-id"));
            });
            //编辑建筑
            $(".editBtn").click(function () {
                editBuild($(this).attr("data-id"));
            })
        }
    });
}

/**
 * 添加建筑点的表单
 * @param overlay
 */
function addBuildForm(overlay) {
    var sContent = "<div class='edit-build'><form>" +
        "<div class='modal-header'>新增建筑</h4> </div>" +
        "<div class='modal-body'>" +
        "<div class='input-group'><label><span>地址:</span></label><input class='form-control' type='text'  name='address' /></div>" +
        "<div class='classify-input input-group'><label><span>经度:</span></label><input class='form-control' type='text' value='" + overlay.point.lng + "' name='lng'/><label><span>纬度:</span>" +
        "</label><input class='form-control' type='text' value='" + overlay.point.lat + "'  name='lat'/></div>" +
        "<div class='classify-input input-group'><label><span>编号:</span></label><input class='form-control' type='text' name='sno'/><label><span>规格:</span></label><input class='form-control' type='text' name='spec'/></div>" +
        "<div class='classify-input input-group'><label><span>类别:</span></label><input class='form-control' type='text' name='type'/><label><span>寿命:</span></label><input class='form-control' type='text' name='age' /></div>" +
        "<div class='classify-input input-group'><label><span>名称:</span></label><input class='form-control' type='text' name='name'/><label><span>责任:</span></label><input class='form-control' type='text' name='personLiable'/></div>" +
        "<div class='classify-input input-group'><label><span>年份:</span></label><input class='form-control' type='text'  name='year' /><label><span>部门:</span></label><input class='form-control' type='text' name='department'/></div>" +
        "<div class='remark-input input-group'><label><span>备注:</span></label><textarea class='form-control' type='text'  name='remarks' ></textarea></div>" +
        "</div>" +
        " <div class='modal-footer'><a href='javascript:;' class='buildBtn  btn ensure-btn' >确定</a><a  class='btn btn-default cancel-modal' data-dismiss='modal'>取消</a>" +
        "</div>" +
        "</form></div>";
    var infoWindow = new BMap.InfoWindow(sContent);
    overlay.openInfoWindow(infoWindow);
    overlay.addEventListener("click", function () {
        this.openInfoWindow(infoWindow);
    });

    //弹出窗口被关闭掉触发删除新建点
    infoWindow.addEventListener("close", function () {
        map.removeOverlay(overlay);
    });
    //点击取消掉触发删除新建点
    $(".cancel-modal").click(function () {
        map.removeOverlay(overlay);
    });

    //绑定事件必须的在最后否则无效
    $(".buildBtn").click(function () {
        //获取最近的form表单的全部字段（记得给每个input等加name字段，否则获取不到表单的值）
        var param = $(this).closest("form").serialize();
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "/rest/web/information/insertArchitecture ",
            data: param,
            success: function () {
                window.location.href = "rest/web/information/toInformationList ";
            }
        })
    });
}

/**绘制建筑的点 **/
function setBuildPoint(obj) {
    var buildContent = "";
    buildContent = buildContent + "<div class='edit-build' id='buildDemo' ><form id='edit-form-" + obj.id + "'>" +
        "<div class='modal-header'>编辑建筑</h4> </div>" +
        "<div class='modal-body'>" +
        "<div class='input-group'><input name='id' type='hidden' value='" + obj.id + "'/></div>" +
        "<div class='input-group'><label><span>地址:</span></label><input class='form-control' type='text' name='address'  value='" + obj.address + "' /></div>" +
        "<div class='classify-input input-group'><label><span>经度:</span></label><input class='form-control' type='text' value='" + obj.lng + "' name='lng'/><label><span>纬度:</span>" +
        "</label><input class='form-control' type='text' value='" + obj.lat + "'  name='lat'/></div>" +
        "<div class='classify-input input-group'><label><span>编号:</span></label><input class='form-control' type='text' name='sno' value='" + obj.sno + "'><label><span>规格:</span></label><input class='form-control' type='text' name='quality' value='" + obj.spec + "'></div>" +
        "<div class='classify-input input-group'><label><span>类别:</span></label><input class='form-control' type='text' name='type' value='" + obj.type + "'><label><span>寿命:</span></label><input class='form-control' type='text' name='age' value='" + obj.age + "'></div>" +
        "<div class='classify-input input-group'><label><span>名称:</span></label><input class='form-control' type='text' name='name' value='" + obj.name + "'><label><span>责任:</span></label><input class='form-control' type='text' name='personLiable' value='" + obj.personLiable + "'></div>" +
        "<div class='classify-input input-group'><label><span>年份:</span></label><input class='form-control' type='text'  name='year' value='" + obj.year + "'><label><span>部门:</span></label><input class='form-control' type='text' name='department' value='" + obj.department + "'></div>" +
        "<div class='remark-input input-group'><label><span>备注:</span></label><textarea class='form-control' type='text' name='remarks'  value='" + obj.remarks + "'></textarea></div>" +
        "</div>" +
        " <div class='modal-footer'><a href='javascript: editBuild(" + obj.id + ");' class='editBtn  btn ensure-btn'  data-id='"
        + obj.id + "'>确定</a><a  href='javascript:confirmBuild(" + obj.id + ");' class='deleteBtn  btn btn-default' data-dismiss='modal' data-id='"
        + obj.id + "'>删除</a>" +
        "</div>" +
        " </form>" +
        "</div>";
    var buildWindow = new BMap.InfoWindow(buildContent); // 创建信息窗口对象
    //创建传感小图标
    var pt = new BMap.Point(obj.lng, obj.lat);
    var myIcon = new BMap.Icon("/static/images/map_building.png", new BMap.Size(22, 22));
    var marker = new BMap.Marker(pt, {icon: myIcon}); // 创建标注
    map.addOverlay(marker);
    marker.addEventListener("click", function () {
        this.openInfoWindow(buildWindow);
        //图片加载完毕重绘infowindow
        document.getElementById('buildDemo').onload = function () {
            buildWindow.redraw(); //防止在网速较慢，图片未加载时，生成的信息框高度比图片的总高度小，导致图片部分被隐藏
        }
    });
}
/**编辑建筑**/
function editBuild(id) {
    var param = $("#edit-form-" + id).serialize();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/rest/web/information/updateArchitecture",
        data: param,
        success: function (data) {
            window.location.href = "rest/web/information/toInformationList ";
        }
    });
}
/**删除涵匣提示框**/
function confirmBuild(id) {
    if ($("#myConfirm").length > 0) {
        $("#myConfirm").remove();
    }
    var html='<div class="modal fade" id="myConfirm" tabindex="-1" role="dialog" aria-labelledby="prompt">'+
            '<div class="modal-dialog" role="document"> <div class="modal-content" style="width:320px;">'+
            '<div class="modal-header" > <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span>'+
            '</button> <h4 class="modal-title" id="prompt" style="font-weight: bold;font-size: 14px;">提示信息</h4> </div> <div class="modal-body"> <div> <input id="dataId" type="hidden" value=""/> </div>'+
            '<p>您确认要删除吗？</p> </div> <div class="modal-footer"> <button type="button" class="confirmOk btn ensure-btn">确定</button> '+
            '<button type="button" class="btn btn-default" data-dismiss="modal">取消</button> </div> </div> </div> </div>';
    $("body").append(html);

    $("#myConfirm").modal("show");
    $(".confirmOk").on("click", function() {
        delBuild(id);
    });
}

/**删除建筑**/
function delBuild(id) {
    var param = "id=" + id;
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/rest/web/information/deleteArchitecture",
        data: param,
        success: function () {
            window.location.href = "rest/web/information/toInformationList ";
        }
    });
}
queryBuild();
/**搜索框中建筑的模糊查询**/
$("#blurBtn").click(function () {
    var name = $("#name").val();
    $.ajax({
        type: "GET",
        url: "/rest/web/information/selectArchitectureList",
        dataType: "json",
        data: {name: name},
        success: function (data) {
            var buildList = data.data;

        }
    });
});

/************************************************************井盖的方法************************************************************************/
/**创建井盖的全部查询 **/
function queryCover() {
    $.ajax({
        type: "GET",
        url: "/rest/web/information/selectManholeList",
        dataType: "json",
        success: function (data) {
            var coverList = data.data;
            $.each(coverList, function (i, o) {
                setCoverPoint(o);
            });
            ////删除井盖
            //$(".deleteBtn").click(function () {
            //    delCover($(this).attr("data-id"));
            //});
            //编辑井盖
            $(".editBtn").click(function () {
                editCover($(this).attr("data-id"));
            })
        }
    });
}
/**
 * 添加井盖点的表单
 * @param overlay
 */
function addCoverForm(overlay) {
    var sContent = "<div class='edit-cover'><form>" +
        "<div class='modal-header'>新增井盖</h4> </div>" +
        "<div class='modal-body'>" +
        "<div class='input-group'><label><span>地址:</span></label><input class='form-control' type='text' name='address'/></div>" +
        "<div class='classify-input input-group'><label><span>经度:</span></label><input class='form-control' type='text' value='" + overlay.point.lng + "' name='lng'/><label><span>纬度:</span>" +
        "</label><input class='form-control' type='text' value='" + overlay.point.lat + "'  name='lat'/></div>" +
        "<div class='classify-input input-group'><label><span>编号:</span></label><input class='form-control' type='text' name='sno'/><label><span>爬梯:</span></label><input class='form-control' type='text' name='ladder'/></div>" +
        "<div class='classify-input input-group'><label><span>类别:</span></label><input class='form-control' type='text' name='type'/><label><span>寿命:</span></label><input class='form-control' type='text' name='age' /></div>" +
        "<div class='classify-input input-group'><label><span>名称:</span></label><input class='form-control' type='text' name='name'/><label><span>材质:</span></label><input class='form-control' type='text' name='quality'/></div>" +
        "<div class='classify-input input-group'><label><span>年份:</span></label><input class='form-control' type='text'  name='year'/><label><span>形状:</span></label><input class='form-control' type='text' name='shape'/></div>" +
        "<div class='classify-input input-group'><label><span>规格:</span></label><input class='form-control' type='text' name='spec'/><label><span>责任:</span></label><input class='form-control' type='text' name='personLiable'/></div>" +
        "<div class='classify-input input-group'><label><span>井深:</span></label><input class='form-control' type='text'  name='depth'/><label><span>部门:</span></label><input class='form-control' type='text' name='department'/></div>" +
        "<div class='classify-input input-group'><label><span>防护:</span></label><input class='form-control' type='text'  name='protect'/></div>" +
        "<div class='remark-input input-group'><label><span>备注:</span></label><textarea class='form-control' type='text'  name='remarks'/></textarea></div>" +
        "</div>" +
        " <div class='modal-footer'><a href='javascript:;' class='coverBtn  btn ensure-btn' >确定</a><a  class='btn btn-default cancel-modal' data-dismiss='modal'>取消</a>" +
        "</div>" +
        "</form></div>";
    var infoWindow = new BMap.InfoWindow(sContent);
    overlay.openInfoWindow(infoWindow);
    overlay.addEventListener("click", function () {
        this.openInfoWindow(infoWindow);
    });

    //弹出窗口被关闭掉触发删除新建点
    infoWindow.addEventListener("close", function () {
        map.removeOverlay(overlay);
    });
    //点击取消掉触发删除新建点
    $(".cancel-modal").click(function () {
        map.removeOverlay(overlay);
    });
    //绑定事件必须的在最后否则无效
    $(".coverBtn").click(function () {
        //获取最近的form表单的全部字段（记得给每个input等加name字段，否则获取不到表单的值）
        var param = $(this).closest("form").serialize();
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "/rest/web/information/insertManhole",
            data: param,
            success: function () {
                window.location.href = "rest/web/information/toInformationList ";
            }
        })
    });
}
/**
 * 绘制井盖的点
 * @param obj
 */
function setCoverPoint(obj) {
    var coverContent = "";
    coverContent = coverContent + "<div class='edit-cover' id='coverDemo ' ><form id='edit-form-" + obj.id + "'>" +
        "<div class='modal-header'>编辑井盖</h4> </div>" +
        "<div class='modal-body'>" +
        "<div class='input-group'><input name='id' type='hidden' value='" + obj.id + "'/></div>" +
        "<div class='input-group'><label><span>地址:</span></label><input class='form-control' type='text' name='address'  value='" + obj.address + "' /></div>" +
        "<div class='classify-input input-group'><label><span>经度:</span></label><input class='form-control' type='text' value='" + obj.lng + "' name='lng'/><label><span>纬度:</span>" +
        "</label><input class='form-control' type='text' value='" + obj.lat + "'  name='lat'/></div>" +
        "<div class='classify-input input-group'><label><span>编号:</span></label><input class='form-control' type='text' name='sno' value='" + obj.sno + "'><label><span>爬梯:</span></label><input class='form-control' type='text' name='ladder' value='" + obj.ladder + "'></div>" +
        "<div class='classify-input input-group'><label><span>类别:</span></label><input class='form-control' type='text' name='type' value='" + obj.type + "'><label><span>寿命:</span></label><input class='form-control' type='text' name='age' value='" + obj.age + "'></div>" +
        "<div class='classify-input input-group'><label><span>名称:</span></label><input class='form-control' type='text' name='name' value='" + obj.name + "'><label><span>材质:</span></label><input class='form-control' type='text' name='quality' value='" + obj.quality + "'></div>" +
        "<div class='classify-input input-group'><label><span>年份:</span></label><input class='form-control' type='text'  name='year' value='" + obj.year + "'><label><span>形状:</span></label><input class='form-control' type='text'  name='shape' value='" + obj.shape + "'></div>" +
        "<div class='classify-input input-group'><label><span>规格:</span></label><input class='form-control' type='text' name='spec' value='" + obj.spec + "'><label><span>责任:</span></label><input class='form-control' type='text' name='personLiable' value='" + obj.personLiable + "'></div>" +
        "<div class='classify-input input-group'><label><span>井深:</span></label><input class='form-control' type='text' name='depth' value='" + obj.depth + "'><label><span>部门:</span></label><input class='form-control' type='text' name='department' value='" + obj.department + "'></div>" +
        "<div class='classify-input input-group'><label><span>防护:</span></label><input class='form-control' type='text' name='protect' value='" + obj.protect + "'></div>" +
        "<div class='remark-input input-group'><label><span>备注:</span></label><textarea class='form-control' type='text' name='remarks'  value='" + obj.remarks + "'></textarea></div>" +
        "</div>" +
        " <div class='modal-footer'><a href='javascript: editCover(" + obj.id + ");' class='editBtn  btn ensure-btn'  data-id='"
        + obj.id + "'>确定</a><a  href='javascript:confirmCover(" + obj.id + ");' class='deleteBtn  btn btn-default' data-dismiss='modal' data-id='"
        + obj.id + "'>删除</a>" +
        "</div>" +
        " </form>" +
        "</div>";
    var coverWindow = new BMap.InfoWindow(coverContent); // 创建信息窗口对象
    //创建井盖小图标
    var pt = new BMap.Point(obj.lng, obj.lat);
    var myIcon = new BMap.Icon("/static/images/map_covers.png", new BMap.Size(22, 22));
    var marker = new BMap.Marker(pt, {icon: myIcon}); // 创建标注
    map.addOverlay(marker);
    marker.addEventListener("click", function () {
        this.openInfoWindow(coverWindow);
        //图片加载完毕重绘infowindow
        document.getElementById('coverDemo ').onload = function () {
            coverWindow.redraw(); //防止在网速较慢，图片未加载时，生成的信息框高度比图片的总高度小，导致图片部分被隐藏
        }
    });
}
/**编辑井盖**/
function editCover(id) {
    var param = $("#edit-form-" + id).serialize();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/rest/web/information/updateManhole",
        data: param,
        success: function (data) {
            window.location.href = "rest/web/information/toInformationList ";
        }
    });
}
/**删除涵匣提示框**/
function confirmCover(id) {
    if ($("#myConfirm").length > 0) {
        $("#myConfirm").remove();
    }
    var html='<div class="modal fade" id="myConfirm" tabindex="-1" role="dialog" aria-labelledby="prompt">'+
            '<div class="modal-dialog" role="document"> <div class="modal-content" style="width:320px;">'+
            '<div class="modal-header" > <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span>'+
            '</button> <h4 class="modal-title" id="prompt" style="font-weight: bold;font-size: 14px;">提示信息</h4> </div> <div class="modal-body"> <div> <input id="dataId" type="hidden" value=""/> </div>'+
            '<p>您确认要删除吗？</p> </div> <div class="modal-footer"> <button type="button" class="confirmOk btn ensure-btn">确定</button> '+
            '<button type="button" class="btn btn-default" data-dismiss="modal">取消</button> </div> </div> </div> </div>';
    $("body").append(html);

    $("#myConfirm").modal("show");
    $(".confirmOk").on("click", function() {
        delCover(id);
    });
}

/**删除井盖**/
function delCover(id) {
    var param = "id=" + id;
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/rest/web/information/deleteManhole",
        data: param,
        success: function () {
            window.location.href = "rest/web/information/toInformationList ";
        }
    });
}
queryCover();
/************************************************************涵匣的方法************************************************************************/
/**创建涵匣的全部查询 **/
function queryHan() {
    $.ajax({
        type: "GET",
        url: "/rest/web/information/selectGatesList",
        dataType: "json",
        success: function (data) {
            var hanList = data.data;
            $.each(hanList, function (i, o) {
                setHanPoint(o);
            });

            //编辑涵匣
            $(".editBtn").click(function () {
                editHan($(this).attr("data-id"));
            })
        }
    });
}

/**
 * 添加涵匣点的表单
 * @param overlay
 */
function addHanForm(overlay) {
    var sContent = "<div class='edit-han'><form>" +
        "<div class='modal-header'>新增涵闸</h4> </div>" +
        "<div class='modal-body'>" +
        "<div class='input-group'><label'><span>地址:</span></label><input class='form-control' type='text' name='address'/></div>" +
        "<div class='classify-input input-group'><label><span>经度:</span></label><input class='form-control' type='text' value='" + overlay.point.lng + "' name='lng'/><label><span>纬度:</span>" +
        "</label><input class='form-control' type='text' value='" + overlay.point.lat + "'  name='lat'/></div>" +
        "<div class='classify-input input-group'><label><span>编号:</span></label><input class='form-control' type='text' name='sno'/><label><span>规格:</span></label><input class='form-control' type='text' name='spec' /></div>" +
        "<div class='classify-input input-group'><label><span>类别:</span></label><input class='form-control' type='text' name='type'/><label><span>寿命:</span></label><input class='form-control' type='text' name='age'  /></div>" +
        "<div class='classify-input input-group'><label><span>名称:</span></label><input class='form-control' type='text' name='name'/><label><span>责任:</span></label><input class='form-control' type='text' name='personLiable'/></div>" +
        "<div class='classify-input input-group'><label><span>年份:</span></label><input class='form-control' type='text'  name='year' /><label><span>部门:</span></label><input class='form-control' type='text' name='department'/></div>" +
        "<div class='remark-input input-group'><label><span>备注:</span></label><textarea class='form-control' type='text'  name='remarks' ></textarea></div>" +
        "</div>" +
        " <div class='modal-footer'><a href='javascript:;' class='hanBtn  btn ensure-btn' >确定</a><a  class='btn btn-default cancel-modal' data-dismiss='modal'>取消</a>" +
        "</div>" +
        "</form></div>";
    var infoWindow = new BMap.InfoWindow(sContent);
    overlay.openInfoWindow(infoWindow);
    overlay.addEventListener("click", function () {
        this.openInfoWindow(infoWindow);
    });
    $(".cancel-modal").click(function () {
        map.clearOverlays();
    });
    //绑定事件必须的在最后否则无效
    $(".hanBtn").click(function () {
        //获取最近的form表单的全部字段（记得给每个input等加name字段，否则获取不到表单的值）
        var param = $(this).closest("form").serialize();
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "/rest/web/information/insertGates",
            data: param,
            success: function () {
                window.location.href = "rest/web/information/toInformationList ";
            }
        })
    });
}

/**
 * 绘制涵闸点
 * @param obj
 */
function setHanPoint(obj) {
    var hanContent = "";
    hanContent = hanContent + "<div class='edit-han' id='hanDemo'><form id='edit-form-" + obj.id + "'>" +
        "<div class='modal-header'>编辑涵闸</h4> </div>" +
        "<div class='modal-body'>" +
        "<div class='input-group'><input name='id' type='hidden' value='" + obj.id + "'/></div>" +
        "<div class='input-group'><label><span>地址:</span></label><input class='form-control' type='text' name='address'  value='" + obj.address + "' /></div>" +
        "<div class='classify-input input-group'><label><span>经度:</span></label><input class='form-control' type='text' value='" + obj.lng + "' name='lng'/><label><span>纬度:</span>" +
        "</label><input class='form-control' type='text' value='" + obj.lat + "'  name='lat'/></div>" +
        "<div class='classify-input input-group'><label><span>编号:</span></label><input class='form-control' type='text' name='sno' value='" + obj.sno + "'><label><span>规格:</span></label><input class='form-control' type='text' name='spec' value='" + obj.spec + "'></div>" +
        "<div class='classify-input input-group'><label><span>类别:</span></label><input class='form-control' type='text' name='type' value='" + obj.type + "'><label><span>寿命:</span></label><input class='form-control' type='text' name='age' value='" + obj.age + "'></div>" +
        "<div class='classify-input input-group'><label><span>名称:</span></label><input class='form-control' type='text' name='name' value='" + obj.name + "'><label><span>责任:</span></label><input class='form-control' type='text' name='personLiable' value='" + obj.personLiable + "'></div>" +
        "<div class='classify-input input-group'><label><span>年份:</span></label><input class='form-control' type='text'  name='year' value='" + obj.year + "'><label><span>部门:</span></label><input class='form-control' type='text' name='department' value='" + obj.department + "'></div>" +
        "<div class='remark-input input-group'><label><span>备注:</span></label><textarea class='form-control' type='text' name='remarks'  value='" + obj.remarks + "'></textarea></div>" +
        "</div>" +
        " <div class='modal-footer'><a href='javascript: editHan(" + obj.id + ");' class='editBtn  btn ensure-btn'  data-id='"
        + obj.id + "'>确定</a><a href='javascript:confirmHan(" + obj.id + ");' class='deleteBtn  btn btn-default'  data-toggle='modal' data-target='#delete-userModal' data-id='"
        + obj.id + "'>删除</a>" +
        "</div>" +
        "</form></div>";//href='javascript:delHan(" + obj.id + ");'
    var hanWindow = new BMap.InfoWindow(hanContent); // 创建信息窗口对象
    //创建传感小图标
    var pt = new BMap.Point(obj.lng, obj.lat);
    var myIcon = new BMap.Icon("/static/images/map_hanza.png", new BMap.Size(22, 22));
    var marker = new BMap.Marker(pt, {icon: myIcon}); // 创建标注
    map.addOverlay(marker);
    marker.addEventListener("click", function () {
        this.openInfoWindow(hanWindow);
        //图片加载完毕重绘infowindow
        document.getElementById('hanDemo').onload = function () {
            hanWindow.redraw(); //防止在网速较慢，图片未加载时，生成的信息框高度比图片的总高度小，导致图片部分被隐藏
        }
    });
}
/**编辑涵匣**/
function editHan(id) {
    var param = $("#edit-form-" + id).serialize();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/rest/web/information/updateGates",
        data: param,
        success: function (data) {
            window.location.href = "rest/web/information/toInformationList ";
        }
    });
}
/**删除涵匣提示框**/
function confirmHan(id) {
    if ($("#myConfirm").length > 0) {
        $("#myConfirm").remove();
    }
    var html='<div class="modal fade" id="myConfirm" tabindex="-1" role="dialog" aria-labelledby="prompt">'+
        '<div class="modal-dialog" role="document"> <div class="modal-content" style="width:320px;">'+
        '<div class="modal-header" > <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span>'+
        '</button> <h4 class="modal-title" id="prompt" style="font-weight: bold;font-size: 14px;">提示信息</h4> </div> <div class="modal-body"> <div> <input id="dataId" type="hidden" value=""/> </div>'+
        '<p>您确认要删除吗？</p> </div> <div class="modal-footer"> <button type="button" class="confirmOk btn ensure-btn">确定</button> '+
        '<button type="button" class="btn btn-default" data-dismiss="modal">取消</button> </div> </div> </div> </div>';
    $("body").append(html);

    $("#myConfirm").modal("show");
    $(".confirmOk").on("click", function() {
        delHan(id);
    });
}

/**删除涵匣**/
function delHan(id) {
    var param = "id=" + id;
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/rest/web/information/deleteGates",
        data: param,
        success: function () {
            window.location.href = "rest/web/information/toInformationList ";
        }
    });
}
queryHan();
/************************************************************管网的方法************************************************************************/
/**创建管网的全部查询 **/
function queryPipe() {
    $.ajax({
        type: "GET",
        url: "/rest/web/information/selectPipeList",
        dataType: "json",
        success: function (data) {
            var pipeList = data.data;
            $.each(pipeList, function (i, o) {
                setPipePoint(o);
            });
            //删除管网
            $(".deleteBtn").click(function () {
                delPipe($(this).attr("data-id"));
            });
            //编辑管网
            $(".editBtn").click(function () {
                editPipe($(this).attr("data-id"));
            })
        }
    });
}

/**
 * 绘制管网点
 * @param obj
 */
function setPipePoint(obj) {
    var pipeContent = "";
    pipeContent = pipeContent + "<div class='edit-pipe' id='pipeDemo'><form id='edit-form-" + obj.id + "'>" +
        "<div class='modal-header'>编辑管网</h4> </div>" +
        "<div class='modal-body'>" +
        "<div class='input-group'><input name='id' type='hidden' value='" + obj.id + "'/></div>" +
        "<div class='input-group'><label><span>起点:</span></label><input class='form-control' type='text' name='startAddress'  value='" + obj.startAddress + "' /></div>" +
        "<div class='classify-input input-group'><label><span>经度:</span></label><input class='form-control' type='text' name='startLng'  value='" + obj.startLng + "'/><label><span>纬度:</span></label><input class='form-control' type='text' name='startLat ' value='" + obj.startLat + "'></div>" +
        "<div class='input-group'><label><span>终点:</span></label><input class='form-control' type='text' name='endAddress'  value='" + obj.endAddress + "' /></div>" +
        "<div class='classify-input input-group'><label><span>经度:</span></label><input class='form-control' type='text' name='endLng'  value='" + obj.endLng + "'/><label><span>纬度:</span></label><input class='form-control' type='text' name='endLat'value='" + obj.endLat + "'></div>" +
        "<div class='three-input input-group'><label><span>编号:</span></label><input class='form-control' type='text' name='sno' value='" + obj.sno + "'><label><span>埋深:</span></label><input class='form-control' type='text' name='depth' value='" + obj.depth + "'><label><span>规格:</span></label><input class='form-control' type='text' name='spec' value='" + obj.spec + "'></div>" +
        "<div class='three-input input-group'><label><span>类别:</span></label><input class='form-control' type='text' name='type' value='" + obj.type + "'><label><span>高程:</span></label><input class='form-control' type='text'  name='altitude' value='" + obj.altitude + "'><label><span>寿命:</span></label><input class='form-control' type='text' name='age' value='" + obj.age + "'></div>" +
        "<div class='three-input input-group'><label><span>名称:</span></label><input class='form-control' type='text' name='name' value='" + obj.name + "'><label><span>壁厚:</span></label><input class='form-control' type='text' name='thickness' value='" + obj.thickness + "'><label><span>责任:</span></label><input class='form-control' type='text' name='personLiable' value='" + obj.personLiable + "'></div>" +
        "<div class='three-input input-group'><label><span>年份:</span></label><input class='form-control' type='text'  name='year' value='" + obj.year + "'><label><span>材质:</span></label><input class='form-control' type='text' name='quality' value='" + obj.quality + "'><label><span>部门:</span></label><input class='form-control' type='text' name='department' value='" + obj.department + "'></div>" +
        "<div class='remark-input input-group'><label><span>备注:</span></label><textarea class='form-control' type='text' name='remarks'  value='" + obj.remarks + "'></textarea></div>" +
        "</div>" +
        " <div class='modal-footer'><a href='javascript: editPipe(" + obj.id + ");' class='editBtn  btn ensure-btn'  data-id='"
        + obj.id + "'>确定</a><a  href='javascript:confirmPipe(" + obj.id + ");' class='deleteBtn  btn btn-default' data-dismiss='modal' data-id='"
        + obj.id + "'>删除</a>" +
        "</div>" +
        "</form></div>";
    //创建传感小图标
    var pipeWindow = new BMap.InfoWindow(pipeContent);
    var startPt = new BMap.Point(obj.startLng, obj.startLat);
    var endPt = new BMap.Point(obj.endLng, obj.endLat);
    //没有进来endPt

    var polyline = new BMap.Polyline([
        startPt,
        endPt
    ], {strokeColor: "blue", strokeWeight: 2, strokeOpacity: 0.5});
    map.addOverlay(polyline);
    var myIcon = new BMap.Icon("/static/images/map_pipe.png", new BMap.Size(22, 22));
    var marker = new BMap.Marker(endPt, {icon: myIcon}); // 创建标注
    map.centerAndZoom(endPt, 15);
    map.addOverlay(marker);
    marker.addEventListener("click", function () {
        this.openInfoWindow(pipeWindow);
    });

};

/**编辑管网**/
function editPipe(id) {
    var param = $("#edit-form-" + id).serialize();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/rest/web/information/updatePipe",
        data: param,
        success: function (data) {
            window.location.href = "rest/web/information/toInformationList ";
        }
    });
}
/**删除涵匣提示框**/
function confirmPipe(id) {
    if ($("#myConfirm").length > 0) {
        $("#myConfirm").remove();
    }
    var html='<div class="modal fade" id="myConfirm" tabindex="-1" role="dialog" aria-labelledby="prompt">'+
        '<div class="modal-dialog" role="document"> <div class="modal-content" style="width:320px;">'+
        '<div class="modal-header" > <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span>'+
        '</button> <h4 class="modal-title" id="prompt" style="font-weight: bold;font-size: 14px;">提示信息</h4> </div> <div class="modal-body"> <div> <input id="dataId" type="hidden" value=""/> </div>'+
        '<p>您确认要删除吗？</p> </div> <div class="modal-footer"> <button type="button" class="confirmOk btn ensure-btn">确定</button> '+
        '<button type="button" class="btn btn-default" data-dismiss="modal">取消</button> </div> </div> </div> </div>';
    $("body").append(html);

    $("#myConfirm").modal("show");
    $(".confirmOk").on("click", function() {
        delPipe(id);
    });
}

/**删除管网**/
function delPipe(id) {
    var param = "id=" + id;
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/rest/web/information/deletePipe",
        data: param,
        success: function () {
            window.location.href = "rest/web/information/toInformationList ";
        }
    });
}
queryPipe();

