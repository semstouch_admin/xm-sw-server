/**
 * Created by Administrator on 2017/5/9.
 */
$(function () {
    /**
     * @decription:登录方法
     * @author：xiaoxiaoxia
     */
    $("#login").click(function () {
        var params = "loginName=" + $("#loginName").val()
            + "&pwd=" + $("#pwd").val();
        $.ajax({
            url: "rest/web/login",
            type: "POST",
            dataType: "json",
            data: params,
            success: function (data) {
                var types = data.success;
                if (!types) {
                    alert(data.msg);
                }
                else {
                    window.location.href = "rest/web/index/toIndexList";
                }
            },
        });
    });
});
