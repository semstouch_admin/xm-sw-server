/**
 * Created by Administrator on 2017/5/8.
 */

//自定义时间格式化函数
Date.prototype.Format = function (fmt) { //author: meizz
    var o = {
        "M+": this.getMonth() + 1, //月份
        "d+": this.getDate(), //日
        "h+": this.getHours(), //小时
        "m+": this.getMinutes(), //分
        "s+": this.getSeconds(), //秒
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度
        "S": this.getMilliseconds() //毫秒
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}


//导航中的li的active转换
$(".nav ul li ").click(function() {
    $(".nav ul  li ").removeClass("active");
    $(this).addClass("active");
});

//搜索框与左边的转换
$(".left-icon").click(function() {
    if ($(".left-icon").css("margin-left") == "342px") {
        $(".left-icon span").attr("class","iconfont icon-you");
        $(".left-icon").css("margin-left", "0px");
        $(".search-input").hide();
        $(".people-bs-example").hide();
    } else {
        $(".left-icon span").attr("class","iconfont icon-zuo");
        $(".left-icon").css("margin-left", "342px");
        $(".search-input").show();
        $(".people-bs-example").show();
    }
});
//人员管理页面的信息点击日期出现
$("#message .trajectory .btn").click(function() {
    $("#message  .trajectory .btn ").removeClass("active");
    $(this).addClass("active");
});
//人员管理页面的轨迹点击日期出现
$("#path .trajectory .btn").click(function() {
    $("#path .trajectory .btn ").removeClass("active");
    $(this).addClass("active");
});
//信息管理页面的多层状态框套
$("#btn2").click(function() {
    $("#show2").modal("show");
});
