/**
 * Created by Administrator on 2017/5/11.
 */

var map = new BMap.Map("map");  // 创建Map实例
var point = new BMap.Point(118.10388605, 24.48923061);// 初始化地图,用城市名设置地图中心点
map.centerAndZoom(point, 15);//
map.enableScrollWheelZoom();//地图可以缩放
map.addEventListener("tilesloaded",function(){
    $('.images').viewer();
});

//声明两个对象用来缓存marker和eventWindow，使用id来查询和添加缓存
var cacheMap = null;
var infoWindowMap = null;

/**
 * 自定义map
 * @constructor
 */
function Map() {

    var mapObj = {};

    this.put = function (key, value) {
        mapObj[key] = value;
    };

    this.remove = function (key) {
        if (mapObj.hasOwnProperty(key)) {
            delete mapObj[key];
        }
    };

    this.get = function (key) {
        if (mapObj.hasOwnProperty(key)) {
            return mapObj[key];
        }
        return null;
    };

    this.getKeys = function () {
        var keys = [];
        for (var k in mapObj) {
            keys.push(k);
        }
        return keys;
    };

    // 遍历map
    this.each = function (fn) {
        for (var key in mapObj) {
            fn(key, mapObj[key]);
        }
    };

    this.toString = function () {
        var str = "{";
        for (var k in mapObj) {
            str += "\"" + k + "\" : \"" + mapObj[k] + "\",";
        }
        str = str.substring(0, str.length - 1);
        str += "}";
        return str;
    }

}


/**查询全部在百度地图上事件**/
function queryEvent() {
    $.ajax({
        type: "GET",
        url: "/rest/web/eventInfo/selectEventInfoList",
        dataType: "json",
        success: function (data) {
            //每次查询后都把marker和eventWindow缓存的数据清空掉
            cacheMap = new Map();
            infoWindowMap = new Map();
            var eventList = data.data;
            $.each(eventList, function (i, o) {
                setEventPoint(o);
            });
            //查询列表
            var reportEventHtml = "";
            var statusTotal = 0;//声明未处理的总数
            $.each(eventList, function (i, n) {
                //查询未处理的次数
                if (n.status == "0") {
                    statusTotal = statusTotal + 1;
                }
                var status = (n.status == "0" ? "未处理" : "已完成");
                if (i == 0) {
                    getSingleEvent(n.id);
                    reportEventHtml = reportEventHtml + "<li data-id='" + n.id + "'> <p>" + n.createTime + "</p> <p>" + status + "</p> <p>" + n.title + "</p></li>";
                }
                else {
                    reportEventHtml = reportEventHtml + "<li data-id='" + n.id + "'> <p>" + n.createTime + "</p> <p>" + status + "</p><p>" + n.title + "</p></li>";
                }
            });
            $("#reportEvent").html(reportEventHtml);
            $("#reportEvent>li").click(function () {
                getSingleEvent($(this).attr("data-id"));
            });
            //求出总数的方法
            var data = data.data;
            var dealWith = "";
            dealWith += "<div><p class='statusTotal'>未处理：<span>" + statusTotal + "</span></p><p>/总数：<span>" + data.length + "</span></p></div>";
            $(".dealWith-div").html(dealWith);
        }
    });
}
/**
 * 绘制点事件
 * @param obj
 */
function setEventPoint(obj) {
    var eventContent = "";
    eventContent = eventContent + "<div class='event ' id='senseDemo'>" +
        "<div class='header'><h3>事件上报&nbsp;&nbsp;&nbsp;" + obj.id + "</h3></div>" +
        "<div class='content'>" +
        "<div class='user-message'><img src='" + obj.userIcon + "'><div><h1>" + obj.nickName + "</h1><h5>" + obj.department + "</h5></div></div>" +
        "<div class='details'><h6>主题：</h6><p>" + obj.title + "</p></div>" +
        "<div class='details details-content'><h6>内容：</h6><p>" + obj.content + "</p></div>" +
        "<div class='details '><h6>时间：</h6><p>" + obj.createTime + "</p></div>" +
        "<div class='details details-address'><h6>地址：</h6><p>" + obj.address + "</p></div>" +
        "<div class='classify'>" +
        "<div><h6>坐标：</h6><p class='coordinate'>" + obj.lng + "," + obj.lat + "</p></div></div>" +
        "<div class='details-images'><h6>图片：</h6>" +
        "<ul class='images'>";
    if (obj.pic != null) {
        var imgArr = obj.pic.split(",");
        for (var j = 0; j < imgArr.length; j++) {
            eventContent = eventContent + "<li><img src='" + imgArr[j] + "' class='imgBtn'></li>";
        }
    }
    eventContent = eventContent + "</ul></div></div></div></div></div>";
    var eventWindow = new BMap.InfoWindow(eventContent); // 创建信息窗口对象

    //创建事件小图标
    var pt = new BMap.Point(obj.lng, obj.lat);
    var myIcon = new BMap.Icon("/static/images/map_event.png", new BMap.Size(22, 22));
    var marker = new BMap.Marker(pt, {icon: myIcon}); // 创建标注
    map.addOverlay(marker);
    marker.addEventListener("click", function () {
        this.openInfoWindow(eventWindow);
        $('.images').viewer();
    });
    //将marker和eventWindow缓存起来
    cacheMap.put(obj.id, marker);
    infoWindowMap.put(obj.id, eventWindow);
}
/**
 * 根据id把缓存的marker和eventWindow拼装后调用map的openInfoWindow方法弹窗展示
 * @param id
 */

function getSingleEvent(id) {
    var infoWindow = infoWindowMap.get(id);
    var marker = cacheMap.get(id);
    marker.openInfoWindow(infoWindow);
    $('.images').viewer();
}
queryEvent();
/**搜索框的模糊查询**/
$("#blurBtn").click(function () {
    var title = $("#title").val();
    $.ajax({
        type: "GET",
        url: "/rest/web/eventInfo/selectEventInfoList",
        dataType: "json",
        data: {title: title},
        success: function (data) {
            var eventList = data.data;
            var reportEventHtml = "";
            $.each(eventList, function (i, n) {
                //对未处理与已处理进行判断
                var status = (n.status == "0" ? "未处理" : "已完成");
                if (i == 0) {
                    getSingleEvent(n.id);
                    reportEventHtml = reportEventHtml + "<li data-id='" + n.id + "'> <p>" + n.createTime + "</p> <p>" + status + "</p> <p>" + n.title + "</p></li>";
                }
                else {
                    reportEventHtml = reportEventHtml + "<li data-id='" + n.id + "'> <p>" + n.createTime + "</p> <p>" + status + "</p><p>" + n.title + "</p></li>";
                }
            });
            $("#reportEvent").html(reportEventHtml);
            $("#reportEvent>li").click(function () {
                getSingleEvent($(this).attr("data-id"));
            });
        }
    });
});


